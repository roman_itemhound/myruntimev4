/*
 ######  ####### #       #       ######     #     #####  #######    ######  #######  #####  #######       #    ######  ### 
 #     # #     # #       #       #     #   # #   #     # #          #     # #       #     #    #         # #   #     #  #  
 #     # #     # #       #       #     #  #   #  #       #          #     # #       #          #        #   #  #     #  #  
 ######  #     # #       #       ######  #     #  #####  #####      ######  #####    #####     #       #     # ######   #  
 #   #   #     # #       #       #     # #######       # #          #   #   #             #    #       ####### #        #  
 #    #  #     # #       #       #     # #     # #     # #          #    #  #       #     #    #       #     # #        #  
 #     # ####### ####### ####### ######  #     #  #####  #######    #     # #######  #####     #       #     # #       ### 
 Rollbase API Functions
*/

var request = require('request');

var rollbase = {
  loginUrl  : 'http://www.rb.rollbase.ph/rest/api/login',
  logoutUrl : 'http://www.rb.rollbase.ph/rest/api/logout',
  queryUrl  : 'http://www.rb.rollbase.ph/rest/api/selectQuery',
  createUrl : 'http://www.rb.rollbase.ph/rest/api/create2',
  updateUrl : 'http://www.rb.rollbase.ph/rest/api/update2',
  getRelationshipsUrl: 'http://www.rb.rollbase.ph/rest/api/getRelationships',
  username  : 'IntermediateServer',
  password  : 'a8solutions'      
};

exports.loginToRollbase = function(url, callback){
  var data = {form: {loginName:rollbase.username, password:rollbase.password, output:"json"}};
  request.post(url, data, function(err, response, body){
    if(err){
      callback(err);      
    }
    else{
      if(JSON.parse(body).sessionId){
        callback(null, JSON.parse(body).sessionId);  
      }
      else{
        callback(JSON.parse(body).message);
      }
    }
  });
};


exports.logoutFromRollbase = function(sessionId, callback){
  request(rollbase.logoutUrl + "?output=json&sessionId=" + sessionId, function(err, response, body){
    if(err){
      console.log(err);
      callback(err, null);
    }
    else{
      if(JSON.parse(body).status === "ok"){
        callback(null, "OK");
      }
      else{
        callback(JSON.parse(body).message, null);
      }
    }
  });
};


exports.queryRollbase = function(sessionId, query, callback){
  var data = {form: {sessionId: sessionId, output: 'json', startRow: 0, maxRows: '999999', query: query}};
  request.post(rollbase.queryUrl, data, function(err, response, body){
    if(err){
      callback(err, null);
    }
    else{
      callback(null, body);
    }      
  });
};


exports.getRelationships = function(sessionId, data, callback){
  var getUrl = rollbase.getRelationshipsUrl + '?sessionId=' + sessionId +'&output=json&id='+ data.id + '&relName=' + data.relName;  
  request.get(getUrl, function(err, response, body){
    if(err){
      callback(err, null);
    }
    else{
      callback(null, body);
    }
  });  
};


exports.createObjectRollbase = function(data, callback){
  request.post(rollbase.createUrl, data, function(err, response, body){
    if(err){
      callback(err, null);
    }
    else{
      callback(null, body);
    }    
  });
};


exports.updateObjectRollbase = function(sessionId, recordId, integrationName, fieldToUpdate, fieldValue, callback){  
  var data = {form: {sessionId: sessionId, objName: integrationName, id:recordId , output: 'json', useIds: true }};
  data.form[fieldToUpdate] = fieldValue;

  request.post(rollbase.updateUrl, data, function(err, response, body){
    if(err){
      console.log(err);
      callback(null);
    }
    else{
      callback(body);
    }
  });  
};

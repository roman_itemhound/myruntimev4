'use strict';

var Model = require('../models');
var Race = require('./Race.js');
var async = require('async');

var BibFactory = function(model){
  var _model = model;

/*
  var _save = function(callback){
    _model.save(callback);
  };
*/

  var _deactivate = function(callback){
    _model.activated = false;
    _model.userId = "";
    _model.save(callback);
  };  

  var Bib = {
    Model: _model,
    Deactivate: _deactivate 
  };

  return Bib;  
};

exports.fetchUpcomingBibsByUserId = function(params, callback){
  var _upcomingBibs = [];

  var _findUpcomingBib = function(bib, cb){    
    Race.fetchRace({find:{'raceId':bib.raceId}, select:'published raceName raceDate racePhoto raceId raceNick'}, function(err, race){
      if(err){        
        cb(err);
      } 
      else{
        if(!race.Model.published){
          _upcomingBibs.push({'bibDetails': new BibFactory(bib), 'raceDetails':race });
        }        
        cb(null);
      }
    });
  };
  
  Model.Bib.fetchBibsByUserId(params, function(err, bibs){
    if(err){
      console.log(err);
      callback(err);
    } 
    else{     
      async.each(bibs, _findUpcomingBib, function(err){
        if(err){
          console.log(err);
          callback(err);
        } 
        else{          
          callback(null, _upcomingBibs);
        }
      });      
    }
  });
};

exports.fetchBib = function(params, callback){  
  Model.Bib.fetchBib(params, function(err, bib){
    if (err){
      callback(err);
    } 
    else {
      if (!bib){
        callback("Bib Does not exist");
      } 
      else {
        callback(null, new BibFactory(bib));
      }
    }
  });
};

exports.createBib = function(params, callback){  
  Model.Bib.createBib(params, function(err, bib){
    if (err) {
      callback(err); return; 
    }
    else{
      callback(null, new BibFactory(bib));
    }
  });
};

exports.fetchBibs = function(params, callback){
  Model.Bib.fetchBibs(params, function(err, bibs){
    if (err){
      callback(err);
    } 
    else {
      callback(null, bibs.map(function(bib){return new BibFactory(bib);}));
    }
  });
};

exports.countBibs = function(params, callback){
  Model.Bib.count({
    find: params
  }, callback);
};

exports.getBibs = function(params, callback){
  Model.Bib.find({
    find : params.find
  }, callback);
};

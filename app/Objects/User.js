'use strict';

var Model = require('../models');
var crypto = require('crypto');
var graph = require('fbgraph');
var Admin = require('./Admin.js');
var Result = require('./Result.js');
var async = require('async');
var _ = require('underscore');
var nodemailer = require("nodemailer");
var appConfiguration = require('../../config/env').config();
var htmlTemplate = require('../mailTemplates').activateTemplateHtml;
var createUserTemplate = require('../mailTemplates').createLocalUserTemplate;
var rollbaseApi = require('../rollbaseAPI');

function _computeHash(dateCreated, PW){
  var preSalt = '_#115!_';
  //var postSalt = dateCreated.getFullYear()+dateCreated.getMonth()+dateCreated.getDate()+dateCreated.getSeconds();
  //Works over different timezone
  var postSalt = dateCreated.getUTCFullYear()+dateCreated.getUTCMonth()+dateCreated.getUTCDate()+dateCreated.getUTCSeconds();

    
  var saltedKey =  preSalt + PW+ postSalt;
  var shasum = crypto.createHash('sha1');
  shasum.update(saltedKey);
  var hash = shasum.digest('hex');  
  for (var i = 0; i < 127999; i++)
  {
    shasum = crypto.createHash('sha1');
    shasum.update(preSalt+hash+postSalt);
    hash = shasum.digest('hex');
  }
  return hash;
}

function UserFactory(model){
  
  var _model = model;

  var __createRollbaseObject = function(params){
    var raceId_PSEBullRun = 154;
    var lookupIHEventId = 12193032;

    console.log(params);
    
    if(params.raceId === raceId_PSEBullRun){

      //create claimStub Object on rollbase
      rollbaseApi.loginToRollbase('http://www.rb.rollbase.ph/rest/api/login', function(err, sessionId){
        if(err){
          console.log("Error logging in");
          console.log(err);
        }
        else{
          //build eventDealObject Data
          var eventDealObjectData = {form: {sessionId: sessionId, output: 'json', objName: 'event_deal', bib_number: params.bibnumber, activation_code: params.activationCode, R11181186: lookupIHEventId }};

          rollbaseApi.createObjectRollbase(eventDealObjectData, function(err, newClaimStubResult){
            if(err){
              console.log("Error creating rollbase object");
              console.log(err);
            }
            else{
              console.log("Success creating rollbase object");
              console.log(newClaimStubResult);
            }
          });
        }
      });
    }

    else{
      //don't do anything
    }
  }; 

  var _activateBib = function(params, callback){

    if ((!params.bibnumber)||(!params.activationCode)) { callback("Please fill in activation fields."); return; }

    var raceId;
    var userId = _model._id;
    
    Model.Bib.ValidateBib({
      "bibnumber":params.bibnumber,
      "activationCode": params.activationCode,
      "userId" : userId
    }, function(err, result){
      if (err){
        callback(err);
      }
      else{
        raceId = result.raceId;

        var rollbaseParams = {
          raceId: result.raceId,
          bibnumber: result.bibnumber,
          activationCode: result.activationCode
        };

        __createRollbaseObject(rollbaseParams);

        if(_model.races.indexOf(result.raceId) === -1){
          _model.races.push(result.raceId);  
        }        
        _save(function(err){
          if(err){
            callback(err);
          } 
          else{            
            //activate result if found;
            params.raceId = raceId;
            Result.fetchResult(params, function(err, result){
              if(err) {
                callback(err);
              }
              else{
                if(result){                  
                  result.ActivateRunner({userId:userId}, function(err, result){
                    if(err){
                      callback(err);
                    } 
                    else{
                      callback(null, result);
                    }  
                  });
                }
                else{
                  console.log("No result for bib yet.");
                  callback(null,null);
                }
              }
            });
          }
        });
      }
    });
  };

  var _watchRace = function(callback){
    callback();
  };

  var _save = function(callback){
    _model.save(callback);
  };

  var _refreshFbAccessToken = function(param, callback){
    if ((!_model.faceBookId)||(!_model.facebookAccessToken)){
      callback("Invalid. User has no FB info");
    } 
    else{      
      _model.facebookAccessToken = param;
      _model.save(callback);
    }
  };

  var _findFbUserFriends = function(callback){
    var fbId = _model.faceBookId;
    var fbToken = _model.facebookAccessToken;
    graph.setAccessToken(fbToken);

    graph.get(fbId + '/friends', function(error, friends){
      if(!error){
        async.eachSeries(friends, function(friend, callback){
          Model.User.fetchUserByFB(friend.id, function(err, foundFriend){
            if(!err && (foundFriend !== null)){
              if(foundFriend.friends.indexOf(_model._id) === -1){
                foundFriend.friends.push(_model._id); 
              }
              foundFriend.save(function(err){
                if(err){
                  callback(err);
                }
                else{
                  if(_model.friends.indexOf(foundFriend._id) === -1){
                    _model.friends.push(foundFriend._id);
                  }
                  callback(null);
                }                  
              });             
            }
          });
        }, function(err){
          if(err){
            console.log(err);
          }
          _model.save(callback);
        });
      }
      else{
        console.log(error);
        _model.save(callback);
      }
    });
  };

  var _checkBadges = function(param, callback){
    console.log(param);
    callback();
  };  

  var _submitWeight = function(params, callback){
    _model.weight = params.weight;
    _model.save(callback);
  };

  var _activateUserAccount = function(params, callback){
    _model.activationToken = undefined;
    _model.activated = params.activated;
    _model.save(callback);
  };

  var _updateUserDetails = function(params, callback){
    _model.firstName = params.fbProfile.first_name;
    _model.lastName = params.fbProfile.last_name;
    _model.gender = params.fbProfile.gender === 'female' ? 'F' : 'M';
    _model.activated = true;       
    _model.facebookAccessToken = params.accessToken;

     if ((!_model.faceBookId)||(!_model.facebookAccessToken)){
      callback("Invalid. User has no FB info");
    } 
    else{      
      _model.save(callback);
    }
  };

  var _updateLocalUserDetails = function(params, callback){
    _model.email = params.email;
    _model.firstName = params.firstName;
    _model.lastName = params.lastName;
    _model.gender = params.gender;
    _model.save(callback);
  };

  var _updateFBUserDetails = function(params, callback){
    if(params.createLocalUser){
      async.waterfall([
        //generate random pw
        function(done){
          crypto.randomBytes(8, function(err, buffer){
            var token = parseInt(buffer.toString('hex'),16).toString().substring(0,10);
            done(err, token);
          });
        },
        function(token, done){
          var dateCreated = new Date();
          var hash = _computeHash(dateCreated, token);                   
          var newUserParams = {};          
          newUserParams.lastLogin = dateCreated;
          newUserParams.hash = hash;
          newUserParams.email = params.email;
          newUserParams.firstName = params.firstName;
          done(null, token, newUserParams);          
        },
        function(token, user, done) {
          var compiledHtml = _.template(createUserTemplate, {'name': user.firstName, 'username': user.email, 'password':token});

          var smtpTransport = nodemailer.createTransport("SMTP",{
            service: appConfiguration.SMTP.service,
            auth: {
              user: appConfiguration.SMTP.user,
              pass: appConfiguration.SMTP.pass
            }
          });

          var mailOptions = {
            to: user.email,
            from: 'Team Itemhound <info@itemhound.com>',
            subject: 'Your new MyRunTime Account!',
            html: compiledHtml          
          };

          smtpTransport.sendMail(mailOptions, function(err) {
            done(err, user);
          });
        }
      ], function(err, result){ 
        _model.email = params.email;
        _model.lastLogin = result.lastLogin;
        _model.hash = result.hash;
        _model.firstName = params.firstName;
        _model.lastName = params.lastName;
        _model.facebookAccessToken = params.facebook.access_token;    
        _model.faceBookId = params.facebook.id;
        _model.fbLink = params.facebook.link;
        _model.activated = true;
        _model.save(callback);       
      });
    }
    else{
      _model.email = params.email;
      _model.firstName = params.firstName;
      _model.lastName = params.lastName;
      _model.facebookAccessToken = params.facebook.access_token;    
      _model.faceBookId = params.facebook.id;
      _model.fbLink = params.facebook.link;
      _model.activated = true;
      _model.save(callback);
    }
  };
  
  var User = {
    ActivateBib : _activateBib,
    WatchRace   : _watchRace,
    Model       : _model,
    RefreshFbAccessToken : _refreshFbAccessToken,
    FindFbUserFriends : _findFbUserFriends,
    UserClass   : _model.userClass,
    CheckBadges : _checkBadges,
    userId      : _model._id,
    SubmitWeight: _submitWeight,
    ActivateUserAccount: _activateUserAccount,
    UpdateUserDetails: _updateUserDetails,
    UpdateLocalUserDetails: _updateLocalUserDetails,
    UpdateFBUserDetails: _updateFBUserDetails,
    isAdmin     : false
  };

  if (_model.userClass === 100){
    return User;
  } 
  else {
    return Admin.AdminFactory(User);
  }
}

exports.UserFactory = UserFactory;

var _fetchUserByFB = function(param, callback){
  Model.User.fetchUserByFB(param, function(err, user){
    if (err){
      callback(err);
    }
    else{
      if(user){        
        var User = new UserFactory(user);
        callback(null, User);
      }
      else{
        callback('No matching User', null);
      }      
    }
  });
};

var _createUserFromFB = function(params, callback){
  var newFBUserParams = {};
  newFBUserParams.firstName = params.fbProfile.first_name;
  newFBUserParams.lastName = params.fbProfile.last_name;
  newFBUserParams.gender = params.fbProfile.gender === 'female' ? 'F' : 'M';
  newFBUserParams.fbLink = params.fbProfile.link;
  newFBUserParams.faceBookId = params.fbProfile.id;
  newFBUserParams.email = params.fbProfile.email;
  newFBUserParams.facebookAccessToken = params.accessToken;
  newFBUserParams.activated = true;
  newFBUserParams.userClass = 100;

  Model.User.createUser(newFBUserParams, function(err, newUser){
    if(err){
      callback(err, null);
    }
    else{
      callback(null, newUser);
    } 
  });
};

var _createNewLocalUser = function(params, callback){
  Model.User.fetchUserDetails({"email":params.email}, function(err, result){
    if(err){
      console.log(err);
      callback(err);
    }
    else{
      if(result){
        callback("Email address already taken by user");
      }
      else{
        async.waterfall([
          //generate random activation token
          function(done){
            crypto.randomBytes(20, function(err, buffer) {
              var token = buffer.toString('hex');
              done(err, token);
            });
          },
          function(token, done){
            var dateCreated = new Date();
            var hash = _computeHash(dateCreated, params.password);            
            var newUserParams = {};
            newUserParams.firstName = params.firstName;
            newUserParams.lastName = params.lastName;
            newUserParams.gender = params.gender;
            newUserParams.email = params.email;
            newUserParams.lastLogin = dateCreated;
            newUserParams.hash = hash;
            newUserParams.userClass = 100; 
            newUserParams.activationToken = token;
            Model.User.createUser(newUserParams, function(err, newUser){
              done(err, token, newUser);
            });
          },
          function(token, user, done) {
            var activateLink = appConfiguration.localRegisterEndpoint + '/auth/activate/' + token;       
            var compiledHtml = _.template(htmlTemplate, {'name': user.firstName, 'activateLink':activateLink});

            var smtpTransport = nodemailer.createTransport("SMTP",{
              service: appConfiguration.SMTP.service,
              auth: {
                user: appConfiguration.SMTP.user,
                pass: appConfiguration.SMTP.pass
              }
            });

            var mailOptions = {
              to: user.email,
              from: 'Team Itemhound <info@itemhound.com>',
              subject: 'MyRunTime Account Activation',
              html: compiledHtml          
            };

            smtpTransport.sendMail(mailOptions, function(err) {
              done(err, user);
            });
          }
        ], function(err, result){        
          callback(err, result);    
        });
      }
    }
  });  
};

var _createLocalUserAndFBByAPI = function(params, callback){
  async.waterfall([
    //generate random pw
    function(done){
      crypto.randomBytes(8, function(err, buffer){
        var token = parseInt(buffer.toString('hex'),16).toString().substring(0,10);
        done(err, token);
      });
    },
    function(token, done){
      var dateCreated = new Date();
      var hash = _computeHash(dateCreated, token);            
      var newUserParams = {};
      newUserParams.firstName = params.firstName;
      newUserParams.lastName = params.lastName;
      newUserParams.gender = params.gender;
      newUserParams.email = params.email;
      newUserParams.lastLogin = dateCreated;
      newUserParams.hash = hash;
      newUserParams.userClass = 100; 
      newUserParams.fbLink = params.fbLink;
      newUserParams.facebookAccessToken = params.facebook.access_token;
      newUserParams.faceBookId = params.facebook.id; 
      newUserParams.activated = true;
      Model.User.createUser(newUserParams, function(err, newUser){
        done(err, token, newUser);
      });
    },
    function(token, user, done) {
      var compiledHtml = _.template(createUserTemplate, {'name': user.firstName, 'username': user.email, 'password':token});

      var smtpTransport = nodemailer.createTransport("SMTP",{
        service: appConfiguration.SMTP.service,
        auth: {
          user: appConfiguration.SMTP.user,
          pass: appConfiguration.SMTP.pass
        }
      });

      var mailOptions = {
        to: user.email,
        from: 'Team Itemhound <info@itemhound.com>',
        subject: 'Your new MyRunTime Account!',
        html: compiledHtml          
      };

      smtpTransport.sendMail(mailOptions, function(err) {
        done(err, user);
      });
    }
  ], function(err, result){        
    callback(err, result);    
  });
};


exports.validateUser = function(params, callback){
  var email = params.email;
  var password = params.password;
  var errors = [];
  Model.User.getUser({"email":email}, function(err, user){
    if (err) { errors.push(err); callback(errors); }
    if (user === null) { errors.push('No matching User'); callback(errors); }
    else{
      var suspectHash = _computeHash(user.lastLogin, password);
      if (suspectHash === user.hash){
        delete user.hash;
        callback([], new UserFactory(user));
      } else{
        callback(["Login and password do not match"], null);
      }  
    }
  });
};

exports.fetchUser = function(params,callback){
  Model.User.fetchUser({"_id":params}, function(err, result){
    if (err || !result){
      callback(err);
    } 
    else {
      var user = new UserFactory(result);
      callback(null, user);
    }
  });
};

exports.fetchUserByFb = function(params, callback){
  Model.User.fetchUserDetails({"faceBookId":params}, function(err, result){
    if(err || !result){
      callback(err);
    }
    else{
      var user = new UserFactory(result);      
      callback(null, user);
    }
  });
};

exports.fetchUserByEmail = function(params, callback){
  Model.User.fetchUserDetails({"email":params}, function(err, result){
    if(err || !result){
      callback(err);
    }
    else{
      var user = new UserFactory(result);
      callback(null, user);
    }
  });
};

exports.fetchUserByActivationToken = function(params, callback){
  Model.User.fetchUserDetails({"activationToken":params}, function(err, result){
    if(err || !result){
      callback(err);
    }
    else{
      var user = new UserFactory(result);
      callback(null, user);
    }
  });
};

exports.findOrCreateFBUser = function(params, callback){
  _fetchUserByFB(params.fbProfile.id, function(err, user){
    if (err){
      //user not found, create new user!
      _createUserFromFB(params, function(err, newUser){
        if(err) {
          callback(err, null);
        }
        else{
          var User = new UserFactory(newUser);
          User.FindFbUserFriends(function(err){
            if(err){
              callback(err);
            }
            else{
              callback(null, User);    
            }
          });
        } 
      }); 
    } 
    else{      
      //user found. refresh access token and user details and update runner friends
      user.UpdateUserDetails(params, function(err){
        if(err){
          callback(err);
        }
        else{
          user.FindFbUserFriends(function(err){
            if(err) {
              callback(err);
            }
            else{
              callback(null, user);
            } 
          });
        }
      });
    }
  });
};

exports.createNewLocalUser = function(params, callback){
  _createNewLocalUser(params, function(err, result){
    callback(err, result);
  });
};

exports.createOrUpdateNewLocalUserByAPI = function(params, callback){
  Model.User.fetchUserDetails({"email":params.email}, function(err, result){
    if(err){
      console.log(err);
      callback(err);
    }
    else{
      if(result){        
        //callback("Email address already taken by user");
        var _thisUser = new UserFactory(result);
        _thisUser.UpdateLocalUserDetails(params, function(err, result){
          if(err){
            callback(err);
          }
          else{           
            callback(null, result);
          }
        });
      }
      else{
        async.waterfall([
          //generate random pw
          function(done){
            crypto.randomBytes(8, function(err, buffer){
              var token = parseInt(buffer.toString('hex'),16).toString().substring(0,10);
              done(err, token);
            });
          },
          function(token, done){
            var dateCreated = new Date();
            var hash = _computeHash(dateCreated, token);            
            var newUserParams = {};
            newUserParams.firstName = params.firstName;
            newUserParams.lastName = params.lastName;
            newUserParams.gender = params.gender;
            newUserParams.email = params.email;
            newUserParams.lastLogin = dateCreated;
            newUserParams.hash = hash;
            newUserParams.userClass = 100; 
            newUserParams.activationToken = undefined;
            newUserParams.activated = true;
            Model.User.createUser(newUserParams, function(err, newUser){
              done(err, token, newUser);
            });
          },
          function(token, user, done) {
            var compiledHtml = _.template(createUserTemplate, {'name': user.firstName, 'username': user.email, 'password':token});

            var smtpTransport = nodemailer.createTransport("SMTP",{
              service: appConfiguration.SMTP.service,
              auth: {
                user: appConfiguration.SMTP.user,
                pass: appConfiguration.SMTP.pass
              }
            });

            var mailOptions = {
              to: user.email,
              from: 'Team Itemhound <info@itemhound.com>',
              subject: 'Your new MyRunTime Account!',
              html: compiledHtml          
            };

            smtpTransport.sendMail(mailOptions, function(err) {
              done(err, user);
            });
          }
        ], function(err, result){        
          callback(err, result);    
        });
      }
    }
  });  
};

exports.findOrCreateFBUserByAPI = function(params, callback){
  _fetchUserByFB(params.facebook.id, function(err, user){    
    if (err){      
      //check if user with email exists
      Model.User.fetchUserDetails({"email":params.email}, function(err, result){
        if(err){
          console.log(err);
          callback(err);
        }
        else{
          if(result){
            //user with email found. update this user
            var _thisUser = new UserFactory(result);
            _thisUser.UpdateFBUserDetails(params, function(err, result){
              if(err){
                callback(err);
              }
              else{
                callback(null, result);
              }
            });
          }
          else{
            //no email found for user. create local and facebook
            _createLocalUserAndFBByAPI(params, function(err, result){
              if(err){
                callback(err);
              }
              else{
                callback(null, result);
              }
            });
          }
        }
      });
    } 
    else{      
      //fb user found.
      //check if user with email exists
      Model.User.fetchUserDetails({"email":params.email}, function(err, result){
        if(err){
          callback(err);
        }
        else{
          if(result){
            params.createLocalUser = false;
            user.UpdateFBUserDetails(params, function(err){
              if(err){
                callback(err);
              }
              else{
                callback(null, user);
              }
            });
          }
          else{
            params.createLocalUser = true;
            user.UpdateFBUserDetails(params, function(err){
              if(err){
                callback(err);
              }
              else{
                callback(null, user);
              }
            });           
          }
        }
      });
    }
  });
};

exports.getAllUsers = function(callback){

  var query = Model.User.User.find({});
  query.select('email firstName lastName dateJoined');
  query = query.setOptions({lean : true});
  query.exec(callback);

};

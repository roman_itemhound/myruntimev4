'use strict';

var Model = require('../models');
//var _ = require('lodash');

exports.fetchRaceLocations = function(callback){
  Model.RaceLocation.fetchRaceLocation(callback);
};

exports.updateRaceLocations = function(params, callback){
  Model.RaceLocation.fetchRaceLocation(function(err, raceLocation){
    if(err){
      callback(err);
    }
    else{
      console.log(params);
      callback(null, raceLocation);
    }
  });
};
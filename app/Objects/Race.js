'use strict';

var Model = require('../models');
var Result = require('./Result.js');
var Bib = require('./Bib.js');
var async = require('async');
var moment = require('moment');

var RaceFactory = function(model){
  var _model = model;
  var _waves = {};

  var _save = function(callback){
    _model.save(callback);
  };  
  
  var _checkPermission = function(params){
    if (!params.user){
      return false;
    } 
    var user = params.user;
    if (!user.isAdmin){
      return false;
    }
    if (user.UserClass !== 0){
      if (user.userId !== _model.owner){
        return false;
      }
    }
    return true;
  };

  var _createBibs = function(params, callback){
    if (!_checkPermission(params)) { callback("User has not enough permissions"); return; } 

    var bibs = params.bibs;
    var _createBibLambda = function(bib, cb){
      Bib.createBib({
        "bibnumber":bib,
        "raceId"   : _model.raceId
      }, cb);
    };

    async.map(bibs, _createBibLambda, function(err, bibs){
      if (err){
        callback(err);
      } 
      else{
        callback(null, bibs);
      } 
    });
  };

  var _addResults = function(params, callback){

    if (!_checkPermission(params)) {callback("User has not enough permissions"); return;}
    var user = params.user;
    var raceId = _model.raceId;
    var waveStart = _waves[params.waveId].waveStart;
    var runners = params.runners;
    var waveId = params.waveId;

    var _createResultLambda = function(result, cb){
      var obj = {
        raceId : raceId,
        waveId : waveId,
        result : result,
        waveStart : waveStart
      };
      Result.CreateResult(obj, cb);      
    };

    Result.ClearResults({
      "raceId" : raceId,
      "waveId" : waveId
    }, function(err){
      if (err){
        callback(err);
      }
      else{
        async.map(runners, _createResultLambda, function(err){
          if (err){
            callback(err);
          } 
          else {
            _rebuildWaveOrder({"waveId":waveId,"user":user}, function(err){
              if (err){
                callback(err);
              }
              else{
                callback(null);
              }
            });
          }
        });
      }
    });
  };

  var _addResult = function(params, callback){
    if (!_checkPermission(params)) {callback("User has not enough permissions"); return;}
    params.raceId = _model.raceId;
    params.waveStart = _waves[params.waveId].waveStart;
    Result.CreateResult(params, callback);
    _rebuildWaveOrder(params, callback);
  };

  var _batchAddResult = function(params, callback){
    if (!_checkPermission(params)) {callback("User has not enough permissions"); return;}
    var count = 0;
    var cleanInserts = 0;
    var total = params.result.length;
    var globalerr = null;
    params.result.forEach(function(doc){
      doc.raceId = _model.raceId;
      doc.waveStart = _waves[doc.waveId].waveStart;
      Result.CreateResult(doc, function(err){
        count++;        
        if (!err){
          cleanInserts++;
        } 
        else {
          if (!globalerr){
            globalerr = [];
          } 
          globalerr[count] = err;
        }

        if (count === total){
          _rebuildWaveOrder(function(err){
            if (err) {
              if (!globalerr){
                globalerr = [];
              }
              globalerr["RebuildOrder"] = err;
            }
            callback(globalerr);
          });
        }
      });
    });
  };

  var _setWaveStart = function(params, callback){
    if (!_checkPermission(params)) {callback("User has not enough permissions"); return;}

    _model.waves.forEach(function(wave){      
      if (wave.waveId === parseInt(params.waveId,10)){
        wave.waveStart = params.waveStart;
      }
    });
    _save(callback);
  }; 

  var _updateWaveDetails = function(params, callback){
    if (!_checkPermission(params)) {callback("User has not enough permissions"); return;}
    _model.waves = _model.waves.map(function(wave){
      if(wave.waveId === parseInt(params.waveDetails.waveId,10)){
        wave = params.waveDetails;
      }            
      return wave;
    });
    _save(callback);    
  };

  var _updateRacePhoto = function(params, callback){
    if (!_checkPermission(params)) {callback("User has not enough permissions"); return;}
    _model.racePhoto = params.fileName;
    _save(callback);
  };

  var _togglePublished = function(params, callback){
    if (!_checkPermission(params)) {callback("User has not enough permissions"); return;}
    
    _model.published = !_model.published;
    _save(callback);
  };

  var _toggleUpcoming = function(params, callback){
    if (!_checkPermission(params)) {callback("User has not enough permissions"); return;}
    _model.upcoming = !_model.upcoming;
    _save(callback);
  };

  var _toggleHideRank = function(params, callback){
    if (!_checkPermission(params)) {callback("User has not enough permissions"); return;}
    _model.hideRank = !_model.hideRank;
    _save(callback);
  };

  var _updateRaceAddress = function(params, callback){
    if (!_checkPermission(params)) {callback("User has not enough permissions"); return;}
    _model.raceAddress = params.raceAddress;
    _save(callback);
  };

  var _updateRaceDate = function(params, callback){
    if (!_checkPermission(params)) {callback("User has not enough permissions"); return;}
    _model.raceDate = params.raceDate;
    _save(callback);
  };

  var _getRunnersInCategory = function(params, callback){
    params.populate = "userId";
    params.populateFields = 'firstName lastName _id gender faceBookId fbLink friends';

    if(params.getByChipTime){
      Result.fetchResultsByChipTime({
        "raceId" : _model.raceId,
        "waveId" : params.waveId,
        "fetchAll" : params.fetchAll,
        "pageNum"   : params.pageNum,
        "params"  : params
      }, callback);
    }
    else if(params.getByBib){
      Result.fetchResultsByBib({
        "raceId" : _model.raceId,
        "waveId" : params.waveId,
        "fetchAll" : params.fetchAll,
        "params"  : params
      }, callback); 
    }
    else{
      Result.fetchResults({
        "raceId" : _model.raceId,
        "waveId" : params.waveId,
        "fetchAll" : params.fetchAll,
        "pageNum"   : params.pageNum,
        "params"  : params
      }, callback);
    }

  };

  var _getActivatedBibs = function(params, callback){
    if (!_checkPermission(params)) {callback("User has not enough permissions"); return;}
    Bib.getBibs({
      find: {"raceId" : _model.raceId}
    }, callback);
  };

  var _rebuildWaveOrder = function(params, callback){
    if (!_checkPermission(params)) {callback("User has not enough permissions"); return;}    
    console.log("rebuildingWaveOrder");

    var totalWaves = _model.waves.length;
    var ctr = 0;
    var completeCtr = function(waveId, text){
      ctr++;
      for (var i = 0; i < _model.waves.length; i++){
        if (model.waves[i].waveId === parseInt(waveId,10)){
          model.waves[i].runnersText = text;
          break;
        }
      }
      if (ctr === totalWaves){
        _save(callback);
      }
    };

    _model.waves.map(function(wave){
      Result.fetchResults({
      "raceId" : _model.raceId,
      "waveId" : wave.waveId,        
      "fetchAll" : true
      }, function(err, runners){  
        if(err){
          console.log(err);
          callback(err);
        }
        else{
          var _runners = runners.map(function(r){
            return {
              b : r.Model.bibnumber,
              o : r.Model.gunTime,
              c : r.Model.chipTime,
              g : r.Model.gender
            };
          });
          var str = JSON.stringify(_runners);        
          completeCtr(wave.waveId, str);
        }        
      });    
    });
  };

  var Race = {
    Model                 : _model, 
    RebuildWaveOrder      : _rebuildWaveOrder,
    GetRunnersInCategory  : _getRunnersInCategory,
    AddResult             : _addResult,
    AddResults            : _addResults,
    TogglePublished       : _togglePublished,
    ToggleUpcoming        : _toggleUpcoming,
    ToggleHideRank        : _toggleHideRank,
    UpdateRaceAddress     : _updateRaceAddress,
    UpdateRaceDate        : _updateRaceDate,
    SetWaveStart          : _setWaveStart,
    CreateBibs            : _createBibs,
    GetActivatedBibs      : _getActivatedBibs,
    BatchAddResult        : _batchAddResult,
    UpdateRacePhoto       : _updateRacePhoto,
    UpdateWaveDetails     : _updateWaveDetails
    
  };

  if (_model.waves){
    for (var i = 0; i < _model.waves.length; i++){
      _waves[_model.waves[i].waveId] = _model.waves[i];
    }
    Race.Waves = _waves;
  }
  return Race;  
};

exports.fetchFilteredRaces = function(params, callback){
  var findParams = {
    sort: {raceDate: -1},
    select : "raceId raceName raceNick raceDate racePhoto raceDescription raceAddress published"    
  };

  var findRaceName;
  var findRaceDate;
  var findRaceLocation;
  var visibleOrPublished = { $or: [ {upcoming: true}, {published: true} ] };
  var startDateString;
  var endDateString;
  var startDate;
  var endDate;

  //filter race name
  if(params.raceName){   
    if(params.raceName === ""){
      findRaceName = {'raceName': new RegExp('.*')};
    }
    else{
      findRaceName = { 'raceName': new RegExp(params.raceName, "i") };
    }
  }
  else{
    findRaceName = {'raceName': new RegExp('.*')};
  }

  //filter location
  if(params.raceLocation){
    if(params.raceLocation === ""){
      findRaceLocation = { 'raceAddress': new RegExp(".*") };
    }
    else{
      findRaceLocation = { 'raceAddress': new RegExp(params.raceLocation, "i") };  
    }
    
  }
  else{
    findRaceLocation = { 'raceAddress': new RegExp(".*") };
  }

  //filter date  
  if(params.month && params.year){
    //complete month and year
    startDateString = params.month + '/1/' + params.year;
    endDateString = moment(startDateString).add(1, 'months');
    startDate = new Date(startDateString);
    endDate = new Date(endDateString);
    findRaceDate = { raceDate: {"$gte": startDate, "$lt": endDate} };
    findParams.find = {$and: [findRaceLocation, findRaceName, findRaceDate, visibleOrPublished ]};
  }
  else{
    //month only
    if(params.month){
      if(params.month !== ""){

        //up to 2019!

        var startDateString2012 = params.month + '/1/2012';
        var endDateString2012 = moment(startDateString2012).add(1, 'months');
        var startDate2012 = new Date(startDateString2012);
        var endDate2012 = new Date(endDateString2012);

        var startDateString2013 = params.month + '/1/2013';
        var endDateString2013 = moment(startDateString2013).add(1, 'months');
        var startDate2013 = new Date(startDateString2013);
        var endDate2013 = new Date(endDateString2013);

        var startDateString2014 = params.month + '/1/2014';
        var endDateString2014 = moment(startDateString2014).add(1, 'months');
        var startDate2014 = new Date(startDateString2014);
        var endDate2014 = new Date(endDateString2014);

        var startDateString2015 = params.month + '/1/2015';
        var endDateString2015 = moment(startDateString2015).add(1, 'months');
        var startDate2015 = new Date(startDateString2015);
        var endDate2015 = new Date(endDateString2015);

        var startDateString2016 = params.month + '/1/2016';
        var endDateString2016 = moment(startDateString2016).add(1, 'months');
        var startDate2016 = new Date(startDateString2016);
        var endDate2016 = new Date(endDateString2016);

        var startDateString2017 = params.month + '/1/2017';
        var endDateString2017 = moment(startDateString2017).add(1, 'months');
        var startDate2017 = new Date(startDateString2017);
        var endDate2017 = new Date(endDateString2017);

        var startDateString2018 = params.month + '/1/2018';
        var endDateString2018 = moment(startDateString2018).add(1, 'months');
        var startDate2018 = new Date(startDateString2018);
        var endDate2018 = new Date(endDateString2018);

        var startDateString2019 = params.month + '/1/2019';
        var endDateString2019 = moment(startDateString2019).add(1, 'months');
        var startDate2019 = new Date(startDateString2019);
        var endDate2019 = new Date(endDateString2019);
        
        findRaceDate = { $or: [ 
          {raceDate: {$gte: startDate2012, $lt:endDate2012} },
          {raceDate: {$gte: startDate2013, $lt:endDate2013} },
          {raceDate: {$gte: startDate2014, $lt:endDate2014} },
          {raceDate: {$gte: startDate2015, $lt:endDate2015} },
          {raceDate: {$gte: startDate2016, $lt:endDate2016} },
          {raceDate: {$gte: startDate2017, $lt:endDate2017} },
          {raceDate: {$gte: startDate2018, $lt:endDate2018} },
          {raceDate: {$gte: startDate2019, $lt:endDate2019} }          
        ]};

        findParams.find = {$and: [findRaceLocation, findRaceName, findRaceDate, visibleOrPublished ]};

      }
      else{
        findParams.find = {$and: [findRaceLocation, findRaceName, visibleOrPublished ]};
      }
    }
    //year only
    else if(params.year){
      if(params.year !== ""){
        startDateString = '1/1/' + params.year;
        endDateString = moment(startDateString).add(1, 'years');
        startDate = new Date(startDateString);
        endDate = new Date(endDateString);
        findRaceDate = { raceDate: {"$gte": startDate, "$lt": endDate} };
        findParams.find = {$and: [findRaceLocation, findRaceName, findRaceDate, visibleOrPublished ]};
      }
      else{
        findParams.find = {$and: [findRaceLocation, findRaceName, visibleOrPublished ]};        
      }
    }

    //no date filter 
    else{
      findParams.find = {$and: [findRaceLocation, findRaceName, visibleOrPublished]};
    }
  }

  findParams.sort = {raceDate: -1};

  Model.Race.fetchRaces(findParams, function(err, races){
    if (err){
      callback(err);
    } 
    else{
      callback(null, races.map(function(race){return new RaceFactory(race);}));
    } 
  });

};

exports.fetchRaceStub = function(params, callback){
  callback(null, new RaceFactory(params));
};

exports.fetchRace = function(params, callback){
  if (params.raceId) {
    params.raceId = parseInt(params.raceId, 10);
  }
  Model.Race.fetchRace({find:params.find, select:params.select}, function(err, race){
    if (err){
      callback(err);
    }
    else {
      if (!race){
        callback("Race Does not exist");
      } 
      else {
        callback(null, new RaceFactory(race));
      }
    }
  });
};

exports.CreateRace = function(params, callback){  
  Model.Race.createRace(params, function(err, race){
    if (err){
      callback(err); return; 
    }
    callback(null, new RaceFactory(race) );
  });
};

exports.UpdateRace = function(params, callback){
  params.find = {'raceId': params.raceId};
  Model.Race.fetchRace({find:params.find}, function(err, race){
    if(err){
      callback(err);
    }
    else{
      if (params.waves){
        for (var i = 0; i < params.waves.length; i++ ){
          params.waves[i].waveId = (i+1);
        }
        race.waves = params.waves;
        race.save(callback);
      }
    }
  });
};

exports.fetchRaces = function(params, callback){
  
  Model.Race.fetchRaces(params, function(err, races){
    if (err){
      callback(err);
    } 
    else{
      callback(null, races.map(function(race){return new RaceFactory(race);}));
    } 
  });
};

exports.deleteRace = function(params, callback){
  Model.Race.deleteRace(params, callback);
};

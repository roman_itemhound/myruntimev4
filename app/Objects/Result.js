'use strict';

var Model = require('../models');
var Bib = require('./Bib.js');
var User = require('./User.js');

var ResultFactory = function(model){
  var _model = model;
  var wave = {};

  if (_model.raceId.waves){
    for (var i = 0; i < _model.raceId.waves.length; i++){
      if (_model.raceId.waves[i].waveId === _model.waveId){
        wave = _model.raceId.waves[i];
        
        break;
      }
    }
  }

  if (_model.userId){
    _model.firstName = _model.userId.firstName;
    _model.lastName = _model.userId.lastName;
  }

  var _activateRunner = function(params, callback){
    _model.userId = params.userId;
    _model.activated = true;
    _model.save(callback);
  };

  var _deactivateRunner = function(callback){
    _model.userId = null;
    _model.activated = false;
    _model.firstName = "";
    _model.lastName = "";
    _model.save(callback);
  };

/*
  var _save = function(callback){
    _model.save(callback);
  };
*/
  var Result = {
    Model       : _model,
    ActivateRunner  : _activateRunner,
    DeactivateRunner : _deactivateRunner
  };

  Result.Wave = wave;
  
  return Result;
};


exports.fetchResult = function(params, callback){
  Model.Result.fetchResult({"bibnumber":params.bibnumber, "raceId":parseInt(params.raceId, 10)}, function(err, result){
    if (err || !result){
      callback(err);
    }
    else {      
      var res = new ResultFactory(result);
      callback(null, res);
    }
  });
};


exports.fetchResultById = function(params, callback){
  Model.Result.fetchResult({"userId":params.userId}, function(err, result){
    if (err){
      callback(err);
    }
    else {
      callback(null, new ResultFactory(result));
    }
  });
};


exports.fetchResultByRaceAndId = function(params, callback){
  Model.Result.fetchResult(params, function(err, result){
    if(err){
      callback(err);
    }
    else{
      if(result){
        callback(null, new ResultFactory(result));  
      }
      else{
        callback(null, null);
      }      
    }
  });
};


exports.fetchResultsById = function(params, callback){  
  var queryParams = {
    find: {"userId": params.userId},
    sort: "-raceId",
    populate: "raceId",
    populateFields: 'raceName raceNick raceDate racePhoto waves.waveId waves.waveName waves.waveDistance'
  };

  Model.Result.find(queryParams, function(err, results){
    if(err){
      callback(err);
    } 
    else{
      callback(null, results.map(function(result){ return new ResultFactory(result); }));
    }
  });
};


exports.fetchResults = function(params, callback){ 
  var queryparams = {
    find: {"raceId":params.raceId, "waveId" : parseInt(params.waveId, 10)},
    sort : "gunTime"    
  };

  if (!params.fetchAll){
    queryparams.skip = params.pageNum;
  }

  if (params.populate){
    queryparams.populate = params.populate;
    queryparams.populateFields = params.populateFields;
  }

  Model.Result.find(queryparams, function(err, results){    
    if (err){
      callback(err);
    } 
    else{
      callback(null, results.map(function(result){
        return new ResultFactory(result);
      }));
    }
  });
};

exports.fetchResultsByChipTime = function(params, callback){
  var queryparams = {
    find: {"raceId":params.raceId, "waveId" : parseInt(params.waveId, 10)},
    sort : "chipTime"    
  };

  if (!params.fetchAll){
    queryparams.skip = params.pageNum;
  }

  if (params.populate){
    queryparams.populate = params.populate;
    queryparams.populateFields = params.populateFields;
  }

  Model.Result.find(queryparams, function(err, results){    
    if (err){
      callback(err);
    } 
    else{
      callback(null, results.map(function(result){
        return new ResultFactory(result);
      }));
    }
  });
};

exports.fetchResultsByBib = function(params, callback){
  var queryparams = {
    find: {"raceId":params.raceId, "waveId" : parseInt(params.waveId, 10)}    
  };

  if (!params.fetchAll){
    queryparams.skip = params.pageNum;
  }

  if (params.populate){
    queryparams.populate = params.populate;
    queryparams.populateFields = params.populateFields;
  }

  Model.Result.find(queryparams, function(err, results){    
    if (err){
      callback(err);
    } 
    else{
      var sortedResult = results.sort(function(a, b){
        var _a = parseInt(a.bibnumber.replace(/\D/g,''), 10);
        var _b = parseInt(b.bibnumber.replace(/\D/g,''), 10);
        return _a - _b;
      });

      callback(null, sortedResult.map(function(result){
        return new ResultFactory(result);
      }));
    }
  });
};

exports.ClearResults = function(params, callback){
  Model.Result.removeResults(params, callback);
};


exports.CreateResult = function(params, callback){
  var waveStart = params.waveStart;
  var saveModel = function(params,cb){
    params.waveStart = waveStart;
    Model.Result.createResult(params, function(err, result){
      if (err){
        cb(err);
      } 
      else if (!result){
        callback("No result found"); 
      }
      else{ 
        cb(null, new ResultFactory(result));
      }
    }); 
  };

  Bib.fetchBib({
    "bibnumber" : params.result.bibnumber,
    "raceId"    : params.result.raceId
  }, function(err, bib){
    if (err) { callback(err); return; }
    if((bib)&&(bib.Model.activated)){
      params.result.userId = bib.Model.userId;    
      params.result.activated = true;
      User.fetchUser({"_id":bib.Model.userId}, function(err, user){
        if ((!err)&&(user)){
          params.result.firstName = user.Model.firstName;
          params.result.lastName = user.Model.lastName;          
        }
        saveModel(params.result,callback);
      });
    }
    else{
      saveModel(params.result,callback);
    }
  });
};

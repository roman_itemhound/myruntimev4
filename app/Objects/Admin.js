'use strict';

var Model = require('../models');
var Race = require('./Race.js');
var Result = require('./Result.js');
var Bib = require('./Bib.js');
var crypto = require('crypto');

var computeGunTimes = function(params){
  var wave = params.wave;  
  var WaveStart = new Date(wave.waveStart);
  var GMTOffset = WaveStart.getTimezoneOffset();          
  var PHOffset = 8*60*60*1000; //GMT +8
  
  var WaveStartLocal = new Date(WaveStart.getTime() + (GMTOffset * 60000) + PHOffset);

  var SplitIDs = [];
  if (wave.splits.length !== 0){
    var splits = wave.splits;
    splits.sort(function(a,b){
      return (a.splitDistance - b.splitDistance);
    });
    for(var ctr = 0; ctr < splits.length; ctr++) {
      SplitIDs.push(splits[ctr].splitName);
    }
  }

  var runners = params.runners.map(function(data){    
    var StartTime = data.startTime;
    var EndTime = data.endTime;
    //var GunTime = (EndTime.getTime() - WaveStart.getTime()) / 1000;
    var GunTime = (EndTime.getTime() - WaveStartLocal.getTime()) / 1000;
    var ChipTime = (EndTime.getTime() - StartTime.getTime()) / 1000;
    data.startTime = StartTime;
    data.endTime = EndTime;
    data.gunTime = GunTime;
    data.chipTime = ChipTime;
    return data;
    
  }); 
  return runners;
};

/*
  Creates administrators  
*/
function AdminFactory (User) {
  //Check if valid administrator
  //var that = this;
  
  var _logAdminAction = function(params, callback){
    console.log(params);    
    callback();
  };
  
  
  var _createRace = function(params, callback){    
    params.owner = this.Model.userId;
    Race.CreateRace(params, callback);
  };

  var _updateRace = function(params, callback){
    Race.UpdateRace(params,callback);
  };

  var _addResult = function(params, callback){
    //Check that administrator has enough privileges to add runner to race
    _logAdminAction({"action":"Added Result","params":params},callback);    
  };

/* TODO?
  var _editResult = function(params, callback){    
    //Check that administrator has enough privileges to edit  race results
    _logAdminAction({"action":"Edited Result","params":params},callback);
  };

  var _editRace = function(params, callback){    
    //Check that administrator has enough privileges to edit  race
    _logAdminAction({"action":"Edited Race","params":params},callback);
  };
*/

  var _updateWaveDetails = function(params, callback){
    Race.fetchRace({"find": {"raceId" : params.raceId}, "lean":false}, function(err, race){
      if(err){ callback(err); return; }
      else{        
        race.UpdateWaveDetails(params, function(err, result){
          if(err) { callback(err); return; }
          else{
            callback(err, result);
          }
        });
      }
    });
    //_logAdminAction({"action":"Updated WaveDetails","params":params},null);
  };

  var _updateWaveResult = function(params, callback){
    var that = this;
    params.fetchAll = true; 
    Result.fetchResults(params, function(err, results){
      if(err){
        console.log(err);        
      } 
      else
      {
        Race.fetchRace({"find": {"raceId" : params.raceId}, "lean":true}, function(err, race){          
          if(err){ 
            callback(err);
            return;
          }
          else{
            var wave = race.Model.waves.filter(function(wave){
              return (wave.waveId === params.waveId);
            });
            if (wave.length === 0) { 
              callback("wave doesnt exist"); 
              return; 
            }
            else{
              var runners = results.map(function(result){
                return result.Model;
              });
              runners = computeGunTimes({
                "runners" : runners,
                "wave":wave[0]
              });                                                        
              callback(null, "OK");              
              that.AddResults({"raceId":params.raceId, runners:runners, "waveId":params.waveId}, function(err, result){
                if (err){
                  callback(err);
                }
                else{
                  callback(null,result);
                } 
              });
            }
          }          
        });
      }      
    });  
    callback(null, "OK");
  };

  var _deleteRace = function(params, callback){
    if (this.Model.userClass !== 0){
      params.owner = this.userId;
    }
    Race.deleteRace(params, callback);
    //_logAdminAction({"action":"Deleted Race","params":params},null);
  };

  var _getActivatedBibs = function(params, callback){
    //var that = this;
    var findParams = {};

    findParams.find = {'raceId' : params.raceId, 'activated':true};
    //findParams.params = {'activated':true};
    Bib.getBibs(findParams, function(err, result){
      if(err){ 
        callback(err); 
        return;
      }
      callback(err, result);
    });    
  };

  var _addResults = function(params, callback){
    var that = this;
    Race.fetchRace({"find": {"raceId" : params.raceId}, "lean":false}, function(err, race){
      if (err) { callback(err); return; }
      params.user = that;
      race.AddResults(params, callback);
    }); 

    //Check that administrator has enough privileges to add runner to race

    //_logAdminAction({"action":"Added Results","params":params},null);
  };

  var _getBibs = function(params, callback){
    //var that = this;
    
    var findParams = {};
    if(params.raceId){
      findParams.find = {'raceId' : params.raceId};    
    }
    if(params.activated){
     findParams.find = {'activated' : params.activated};     
    }
    
    Bib.getBibs(findParams, function(err, result){
      if(err){ 
        callback(err); 
        return;
      }
      callback(err, result);
    });
  };

  var _rebuildWaveOrder = function(params, callback){
    var that = this;
    Race.fetchRace({"find": {"raceId" : params.raceId}, "lean":false}, function(err, race){
      if (err) { callback(err); return; }
      params.user = that;
      race.RebuildWaveOrder(params, callback); 
    }); 
  };

  var _updateActivatedBibs = function(params, callback){
    //var that = this;
    Result.fetchResult(params, function(err, result){
      if(err){
        callback(err);
        return;
      }
      else{
        if(result){
          result.ActivateRunner({userId: params.userId}, function(err, result){
            if(err){
              callback(err);
            }
            else{
              callback(null, result);
            }
          });
        }
      }
    });
  };

  var _togglePublished = function(params, callback){
    var that = this;
    Race.fetchRace({"find": {"raceId" : params.raceId}, "lean":false}, function(err, race){
      if (err) { 
        callback(err); return; 
      }
      params.user = that;
      race.TogglePublished(params, callback); 
    }); 
  };

  var _toggleUpcoming = function(params, callback){
    var that = this;
    Race.fetchRace({"find": {"raceId" : params.raceId}, "lean":false}, function(err, race){
      if (err) { callback(err); return; }
      params.user = that;
      race.ToggleUpcoming(params, callback); 
    }); 
  };

  var _toggleHideRank = function(params, callback){
    var that = this;
    Race.fetchRace({"find": {"raceId" : params.raceId}, "lean":false}, function(err, race){
      if (err) { callback(err); return; }
      params.user = that;
      race.ToggleHideRank(params, callback); 
    }); 
  };

  var _updateRaceAddress = function(params, callback){
    var that = this;
    Race.fetchRace({"find": {"raceId" : params.raceId}, "lean":false}, function(err, race){
      if (err) { callback(err); return; }
      params.user = that;
      race.UpdateRaceAddress(params, callback); 
    }); 
  };

  var _updateRaceDate = function(params, callback){
    var that = this;
    Race.fetchRace({"find": {"raceId" : params.raceId}, "lean":false}, function(err, race){
      if (err) { callback(err); return; }
      params.user = that;
      race.UpdateRaceDate(params, callback); 
    }); 
  };

  var _getBibsCount = function(params, callback){ 
    Bib.countBibs(params, function(err, count){
      if (err){
        callback(err);
      }
      else{
        callback(null, count);
      }
    });
  };

  var _createBibs = function(params, callback){
    var that = this;
    Race.fetchRace({"find": {"raceId" : params.raceId}, "lean":true}, function(err, race){
      if (err) { callback(err); return; }
      params.user = that;
      race.CreateBibs(params, callback);
    });
  };

  var _getRaces = function(callback){
    if (this.Model.userClass === 0){
      Race.fetchRaces({"find": {}, "select" : 'raceId _id raceName raceNick raceDate racePhoto published upcoming raceDescription'}, callback);
    }
    else{
      Race.fetchRaces({
        "find" : { "owner":this.userId },
        "select" : 'raceId _id raceName raceNick raceDate racePhoto published upcoming raceDescription'
      }, callback);  
    }    
  };

  var _setWaveStart = function(params, callback){
    var that = this;
    Race.fetchRace({"find": {"raceId" : params.raceId}}, function(err, race){
      if (err){
        callback(err);
      }
      else if (!race) {
        callback("race does not exist");
      }
      else {
        race.SetWaveStart({
          "waveId" : params.waveId,
          "waveStart" : params.waveStart,
          "user"  : that
        }, callback);
      }
    });
  };

  var _getRace = function(params, callback){
    if (this.Model.userClass === 0){
      Race.fetchRace({"find": {"raceId" : params.raceId}, "lean":true}, callback);
    }
    else{
      Race.fetchRace({
        "find": {
          "owner":this.userId,
          "raceId" : params.raceId
        }, 
        "lean" : true
      }, callback);  
    }    
  };

  var _uploadRacePhoto = function(params, callback){
    var that = this;    
    if(this.Model.userClass === 0){
      Race.fetchRace({"find": {"raceId" : params.raceId}, "lean":false}, function(err, race){
        params.user = that;
        race.UpdateRacePhoto(params, function(){
          callback();  
        });        
      });      
    }
  };

  var _encryptPass = function(dateCreated, PW){
    var preSalt = '_#115!_';  
    var postSalt = dateCreated.getUTCFullYear()+dateCreated.getUTCMonth()+dateCreated.getUTCDate()+dateCreated.getUTCSeconds();
    
    var saltedKey =  preSalt + PW+ postSalt;
    var shasum = crypto.createHash('sha1');
    shasum.update(saltedKey);
    var hash = shasum.digest('hex');  
    for (var i = 0; i < 127999; i++)
    {
      shasum = crypto.createHash('sha1');
      shasum.update(preSalt+hash+postSalt);
      hash = shasum.digest('hex');
    }
    return hash;
  };

  var _createUser = function(params, callback){
    var dateCreated = new Date();

    var hash = _encryptPass(dateCreated, params.password);
    
    params.hash = hash;
    params.lastLogin = dateCreated;
    params.userClass = 0;
    params.gender = 'male';

    Model.User.createUser(params, function(err, newAdminUser){
      if(err){
        console.log(err);
        callback(err);
      }
      else{
        callback(null, newAdminUser);
      }
    });

    //_logAdminAction({"action":"Create Result","params":params},null);
  };

  if (User.Model.userClass === 0){
    User.createUser = _createUser;
  }
  User.isAdmin = true;  
  User.SetWaveStart = _setWaveStart;
  User.CreateRace = _createRace;
  User.UpdateRace = _updateRace;
  User.AddResult  = _addResult;
  User.GetRaces = _getRaces;
  User.DeleteRace = _deleteRace;  
  User.GetBibs = _getBibs;
  User.UpdateWaveDetails = _updateWaveDetails;
  User.UpdateWaveResult = _updateWaveResult;
  User.TogglePublished = _togglePublished;
  User.ToggleUpcoming = _toggleUpcoming;
  User.ToggleHideRank = _toggleHideRank;
  User.UpdateRaceAddress = _updateRaceAddress;
  User.UpdateRaceDate = _updateRaceDate;
  User.CreateBibs = _createBibs;
  User.GetActivatedBibs = _getActivatedBibs;  
  User.GetBibsCount = _getBibsCount;  
  User.AddResults = _addResults;
  User.GetRace = _getRace;
  User.RebuildWaveOrder = _rebuildWaveOrder;
  User.UpdateActivatedBibs = _updateActivatedBibs;
  User.UploadRacePhoto = _uploadRacePhoto;
  return User;
}

exports.AdminFactory = AdminFactory;

'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var RaceLocationSchema = new Schema({
  locations : [ String ]
});

var RaceLocation = mongoose.model('RaceLocation', RaceLocationSchema);

exports.fetchRaceLocation = function(callback){
  RaceLocation.findOne({}).exec(callback);
};

exports.RaceLocation = RaceLocation;

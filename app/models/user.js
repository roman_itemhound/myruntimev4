'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  Utilities = require('../utilities');
  
var ThirdPartySchema = new Schema({
  site          : String,
  siteId        : String,
  siteToken     : String
});

var UserSchema = new Schema({
  userName        : { type: String, required: false },
  email           : { type: String, required: false, set: Utilities.toLower, unique: true},
  firstName       : { type: String, required: true, default: "" , get: Utilities.toProper, set: Utilities.toProper },
  lastName        : { type: String, required: true, default: "" , get: Utilities.toProper, set: Utilities.toProper },
  hash            : { type: String, required: false },
  userClass       : {type: Number, default: 100, required: true},
  lastLogin       : { type : Date },
  birthdate       : { type : Date },
  faceBookId      : { type: String  , default: "" },
  facebookAccessToken     : { type: String  , default: "" },
  thirdParties    : [ThirdPartySchema],
  gender          : { type: String, default: "", required: true },
  fbLink          : { type: String, required: false },
  races           : [Number],
  friends         : [String],
  weight          : {type: Number, default: 0},
  taggedPhotos    : {type: [String], default : []},
  badgesEarned    : {type: [String], default : []},
  activated       : {type: Boolean, default: false},
  activationToken : {type: String},
  dateJoined      : {type: Date}
});

UserSchema.index({email : 1});

var User = mongoose.model('UserModel', UserSchema);

exports.User = User;

exports.fetchUserByFB = function (faceBookId, callback) {
  var query = User.findOne({"faceBookId":faceBookId});
  query.select('email firstName lastName userClass gender races friends taggedPhotos badgesEarned facebookAccessToken faceBookId dateJoined');
  query.exec(callback);
};

exports.fetchUserDetails = function(params, callback){
  var query = User.findOne(params);
  query.select('email firstName lastName gender faceBookId userClass activated dateJoined');
  query.exec(callback);
};

exports.fetchUser = function(params,callback){
  var query = User.findOne({"_id":params._id});
  query.select('email firstName lastName userClass gender races friends taggedPhotos badgesEarned _id faceBookId weight activated dateJoined');
  query.exec(callback);
};

exports.getUser = function(params, callback){
  var query = User.findOne(params);
  query.exec(callback); 
};

exports.createUser = function(params, callback){
  var user = new User(params);
  user.dateJoined = new Date();
  user.save(callback);
};

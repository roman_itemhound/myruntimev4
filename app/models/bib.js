'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  Utilities = require('../utilities');
  
var BibSchema = new Schema({
  bibnumber: String,
  raceId: Number,
  activationCode: String,
  activated: { type: Boolean, default: false},
  userId: { type: String, default: ''}
});

BibSchema.index({bibnumber: 1, activationCode: 1}, {unique: true});
BibSchema.index({bibnumber: 1, raceId: 1}, {unique: true});
BibSchema.index({userId: 1});

var Bib = mongoose.model('BibModel', BibSchema);

var buildRandomActivationCode = function(){
  var actCode = "";
  for (var i =0; i <6; i++){
    actCode += String.fromCharCode(65+parseInt(Math.random()*26, 10));
  } 
  return actCode;
};

var checkBibExists = function(bibinfo, callback){
  Bib.findOne({"activationCode":bibinfo.activationCode, "bibnumber": bibinfo.bibnumber}).exec(function(err, card){
    if (err) {
      callback(err);
    }
    else if (card != null){
      callback("ID exists");
    } 
    else {
      callback(null, bibinfo.activationCode);
    }
  });
};

var assignActivationCode = function(bibnumber, callback){
  var checkIfBibExists = function(err, id){
    if (err === null){
      callback(null, id);
    } 
    else{
      checkBibExists({"bibnumber":bibnumber, "activationCode": buildRandomActivationCode()}, checkIfBibExists);
    }
  };
  checkBibExists({"bibnumber":bibnumber, "activationCode": buildRandomActivationCode()}, checkIfBibExists);
};

BibSchema.pre('save', function(next){
  if (this.isNew){
    var that = this;
    assignActivationCode(that.bibnumber, function(err, activationCode){
      if (err){
        next(err);
      } 
      else {
        that.activationCode = activationCode;
        next();
      }
    });  
  }else{
    next();
  }
});

var _fetchBib = function(bibparams, callback){
  Bib.findOne({"bibnumber":bibparams.bibnumber, "raceId":bibparams.raceId}, function(err, bib){
    if (err) {
      callback(err);
    }
    else if (bib === null){      
      callback("Bib: " + bibparams.bibnumber + " does not exist");
    } 
    else {
      callback(null, bib);
    }
  });
};

var _validateBib = function(params, callback){
  Bib.findOne({"bibnumber":params.bibnumber, "activationCode":params.activationCode}, function(err, bib){
    if (err){
      callback(err);
    } 
    else if (bib === null){
      callback("No match found for Bib Number and Activation Code. \r\n (Activation code is case sensitive)");
    } 
    else {
      if(bib.activated){
        callback("Bib already activated");
      }
      else{
        bib.userId = params.userId;
        bib.activated = true;
        bib.save(callback);  
      }      
    }
  });
};

exports.createBib = function(bibparams, callback){
  var newBib = new Bib();
  newBib.bibnumber = bibparams.bibnumber;
  newBib.raceId = bibparams.raceId;
  newBib.save(Utilities.passErrOrResultToCallback(callback));
};

exports.fetchBibsByUserId = function(params, callback){
  Bib.find({"userId":params.userId}, function(err,bibs){
    if(err) {
      callback(err);
    }
    else if (!bibs || bibs.length === 0){
      callback(null, []);
    } 
    else{
      callback(null, bibs);
    } 
  });
};

exports.count = function(params, callback){
  var query = Bib.count(params.find);
  query.exec(callback);
};

exports.find = function(params, callback){
  var query = Bib.find(params.find);
  query = query.sort(params.sort);
  if (params.skip !== undefined){
    query = query.skip(50*params.skip);
    query = query.limit(50);     
  }
  if (params.populate){
    query = query.populate(params.populate, params.populateFields);  
  }
  
  query = query.setOptions({lean : true});
  query.exec(callback);
};

exports.fetchBib = _fetchBib;
exports.ValidateBib = _validateBib;
exports.Bib = Bib;
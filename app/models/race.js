'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var SplitSchema = new Schema({
  splitName     : String,
  splitDistance : Number,
  splitOrder    : Number
});

var WaveSchema = new Schema({    
  waveName        : String,
  waveDistance    : Number,
  waveDescription : String,
  splits          : [SplitSchema],
  waveId          : Number,
  waveStart       : String,
  runnersText     : String,
  thirdPartyWaveUrl: String
});

var RaceSchema = new Schema({
  _id             : { type : Number },
  raceId          : { type : Number },
  raceName        : { type : String, required: true },
  raceNick        : { type : String, required: true },
  raceDescription : { type : String, required: false, default: "" },
  raceDate        : { type : Date  , required: true, default: Date.now },
  raceAddress     : { type : String , required: false, default: "" },
  racePhoto       : { type : String , required: false, default: "nophoto.png" },
  owner           : { type: mongoose.Schema.ObjectId, ref: 'UserModel', required: false },
  hideRank        : { type: Boolean, default: false},  
  published       : { type : Boolean, required: true, default: false},
  upcoming        : { type: Boolean, required: true, default: false},
  raceEPCHeader   : { type : String , required: false, default: ""},
  raceMap         : { type : String , required: false, default: ""},
  instagramImages : [String],
  galleryImages   : [String],
  galleryThumbs   : [String],
  waves           : [WaveSchema],
  isThirdPartyRace: { type: Boolean, default: false }
});

RaceSchema.index({ eventId   : 1});
RaceSchema.index({ eventNick : 1});

var Race = mongoose.model('RaceModel', RaceSchema);

var getLargestRaceId = function(callback){
  Race.find().select('raceId').sort({'raceId':'descending'}).limit(1).exec(function(err, result){
    if (err){
      callback(err, null);
    } 
    else{
      if(result.length === 0){
        callback(null, 0);
      }
      else{
        callback(null, result[0].raceId);
      }
    } 
  });
};

RaceSchema.pre('save', function(next){ 
  var that = this;
  if (this.isNew){  
    getLargestRaceId(function(err, number){
      if (!err) {
        that.raceId = number+1; 
        that._id = number+1; 
        next();
      }
      else {
        next(err);
      }
    });  
  }
  else{
    next();
  } 
});



exports.fetchRace = function (params, callback) {
  if (params.find.raceId) {
    params.find.raceId = parseInt(params.find.raceId, 10);
  }
  params.lean = params.lean || false;
  Race.findOne(params.find).populate('owner').select(params.select).setOptions({lean:params.lean}).exec(callback);
};

exports.createRace = function(params, callback){
  if (params.waves){
    for (var i = 0; i < params.waves.length; i++ ){
      params.waves[i].waveId = (i+1);
    }
  }
  var newRace = new Race(params);
  newRace.save(callback);
};


exports.fetchRaces = function(params, callback){  
  if(params.pageNum){
    if(!params.sort){
      params.sort = {raceDate:-1};
    }
    Race.find(params.find).sort(params.sort).skip(9*params.pageNum).limit(9).select(params.select).setOptions({lean:true}).exec(callback);
  }
  else{
    Race.find(params.find).sort({raceDate:-1}).select(params.select).setOptions({lean:true}).exec(callback); 
  }  
};

exports.deleteRace = function(params, callback){
  Race.remove(params).exec(callback);
};

exports.Race = Race; 

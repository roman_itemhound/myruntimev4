'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  Utilities = require('../utilities'),
  TimeUtils = require('../timeComputation');


var SectorTimeSchema = new Schema({
  time          : { type : Date },
  sectorLength  : Number,
  sectorStart   : { type : Date },
  sectorEnd     : { type : Date }  
});
  
var fixGender = function(gender){
  return gender.substr(0,1).toUpperCase();
};

var ResultSchema = new Schema({
  bibnumber   : { type: String, required: true },
  firstName   : { type: String, default: "First Name",  get:Utilities.toProper},
  lastName    : { type: String, default: "Last Name",   get:Utilities.toProper},
  middleName  : { type: String, default: "M.I." ,       required: false },
  gender      : { type: String, default: "U",           required: true, get: fixGender },
  nationality : { type: String, default: "",    get:Utilities.toProper},
  birthdate   : { type : Date },
  startTime   : { type : Date,  required: true },
  endTime     : { type : Date,  required: true },
  gunTime     : { type : Number, required: true },
  chipTime    : { type : Number, required: true },
  sectorTimes : [SectorTimeSchema],
  classes     : [String],
  waveId      : { type : Number, required: true },
  raceId      : { type: Number, ref: 'RaceModel' },
  userId      : { type: String, ref: 'UserModel' },
  activated   : { type : Boolean, default:false }
});


ResultSchema.index({ bibnumber  : 1});
ResultSchema.index({ gunTime    : 1});
ResultSchema.index({ endTime    : 1});
ResultSchema.index({ waveId     : 1});
ResultSchema.index({ bibnumber  : 1, raceId: 1}, {unique: true});
ResultSchema.index({raceId: 1, waveId: 1, gunTime: 1});
ResultSchema.index({userId: 1, raceId: -1});

ResultSchema.pre('save', function(next){  
  var that = this;
  
  this.chipTime = TimeUtils.ComputeTimeDifference(that.startTime, that.endTime);

  next();
});

var Result = mongoose.model('RunnerModel', ResultSchema);

exports.Result = Result;

exports.fetchResult = function (params, callback) {
  var query = Result.findOne(params);
  query = query.populate('raceId','raceName raceNick raceDate raceAddress racePhoto raceMap waves.waveId waves.waveName');
  query = query.populate('userId', 'firstName lastName faceBookId friends birthdate');
  query.exec(callback);
};

exports.find = function(params, callback){  
  var query = Result.find(params.find);

  if(params.sort){
    query = query.sort(params.sort);
  }
  
  if (params.skip !== undefined){
    query = query.skip(50*params.skip);
    query = query.limit(50);     
  }

  query = query.populate('userId', 'firstName lastName facebookId');
  
  if (params.populate){
    query = query.populate(params.populate, params.populateFields);  
  }
    
  query = query.setOptions({lean : true});
  query.exec(callback);
};

exports.createResult = function(params, callback){
  var result = new Result(params);
  result.save(callback);
};

exports.removeResults = function(params, callback){
  Result.remove(params).exec(callback);
};

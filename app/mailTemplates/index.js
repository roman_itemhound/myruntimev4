/*jshint multistr: true */
var activateTemplateHtml = '<div class="email-background" style="background: #eee;padding: 10px;">\
  <div class="pre-header" style="background: #eee;color: #eee;font-size: 5px;">\
    Activate your MyRunTime Account!\
  </div> \
  <div class="email-container" style="max-width: 500px;background: white;font-family: sans-serif;margin: 0 auto;overflow: hidden;border-radius: 5px;text-align: center;">\
    <img src="http://b3014a5c447801daf159-aab1b1665c18d03bbbf5846be888e985.r49.cf4.rackcdn.com/images/MyRunTime_headerlogo.png" style="max-width: 100%;">\
    <h1 style="color:#666;"> Hi there, <%= name %>! </h1>\
    <h3 style="color:#666;"> It\'s the last step for activating your MyRunTime account! Just click the link below! </h3>\
    <div class="cta" style="margin: 20px;">\
      <a href="<%= activateLink%>" style="color: white;text-decoration: none;display: inline-block;background: #3d87f5;padding: 10px 20px;border-radius: 4px;">Click me to activate your account!</a>\
    </div>\
  </div>\
  <div class="email-footer" style="max-width: 500px;background: none;padding: 20px;font-size: 14px;font-family: sans-serif;margin: 0 auto;overflow: hidden;border-radius: 5px;text-align: center;">\
    <a href="http://www.itemhound.com" style="color: #3d87f5;">Powered by Itemhound</a>\
  </div>\
</div>';

var createLocalUserTemplate = '<div class="email-background" style="background: #f1f1f1;padding: 10px;">\
  <div class="pre-header" style="background: #f1f1f1;color: #f1f1f1;font-size: 5px;">\
    Thank you for pre-registering using MyRunTime. As a bonus, we have also created your own MyRunTime Account!\
  </div> \
  <div class="email-container" style="max-width: 600px;background: white;font-family: sans-serif;margin: 0 auto;overflow: hidden;border-radius: 5px;text-align: center;">\
    <img src="https://fbcdn-sphotos-f-a.akamaihd.net/hphotos-ak-ash2/v/t1.0-9/182779_512935395422636_342325240_n.jpg?oh=bc4df2a71ccbaec8f425b5c5d0b18a31&oe=54DB3EE3&__gda__=1425158402_107630d4f769e84911708769b701baa2" style="max-width: 100%;">\
    <h1 style="color:#666;"> Hi there, <%= name %>! </h1>\
    <h3 style="color:#666;"> Thank you for pre-registering using MyRunTime. As a bonus, we have also created your own MyRunTime Account! These are your account details </h3>\
    <p style="margin: 20px;font-size: 16px;color: #666;font-weight: 300;line-height: 1.4;">Username: <%= username %></p>\
    <p style="margin: 20px;font-size: 16px;color: #666;font-weight: 300;line-height: 1.4;">Temporary Password: <%= password %> </p>\
    <p style="margin: 20px;font-size: 16px;color: #666;font-weight: 300;line-height: 1.4;">Click the button below to login to your account! </p>\
    <div class="cta" style="margin: 20px;">\
      <a href="http://192.168.1.198:3000/#/login" style="color: white;text-decoration: none;display: inline-block;background: #3d87f5;padding: 10px 20px;border-radius: 4px;">Click me to login!</a>\
    </div>\
  </div>\
  <div class="email-footer" style="max-width: 500px;background: none;padding: 20px;font-size: 14px;font-family: sans-serif;margin: 0 auto;overflow: hidden;border-radius: 5px;text-align: center;">\
    <a href="http://www.itemhound.com" style="color: #3d87f5;">Powered by Itemhound</a>\
  </div>\
</div>';

exports.activateTemplateHtml = activateTemplateHtml;
exports.createLocalUserTemplate = createLocalUserTemplate;


var Controllers = require('../controllers'),
  passport = require('passport'),
  appConfiguration = require('../../config/env').config();

/* --removed caching-- problem with multiple drones  
var apiCacheObject = require('apicache').options({ debug: true });
var apiCacheMiddleware = apiCacheObject.middleware;
var cacheController = Controllers.CacheController;
cacheController.setGlobalApiCache(apiCacheObject);
*/

module.exports = function (app) {
  
  //Authentication Routes
 
  app.post('/auth/adminlogin', Controllers.AuthController.adminSignin);
  app.post('/auth/login', Controllers.AuthController.userSignin);
  app.post('/auth/register', Controllers.AuthController.registerNewUser);
  app.get('/auth/facebook', passport.authenticate('facebook', {scope: appConfiguration.facebook.scope}));
  app.get('/auth/facebook/callback', passport.authenticate('facebook',{
    successRedirect:'#/connect',
    failureRedirect:'/login'
  }));
  app.get('/auth/activate/:token', Controllers.AuthController.validateActivateToken);
  app.get('/logout', function(req, res){
    req.logout();
    res.redirect('/');
  });

  //authentication APIS?
  //app.post('/auth/api/facebook', Controllers.UserController.findOrCreateFBUser);
  //app.get('/auth/api/local/:email', Controllers.UserController.getLocalUserByEmail)
  
  app.get('/api/facebook/:facebookId', Controllers.UserController.getFBUser);  
  app.post('/api/facebook', Controllers.UserController.createOrUpdateFBUserApi);
  //app.post('/api/facebook/:facebookId', Controllers.UserController.updateFBUser);
  
  app.get('/api/local/:email', Controllers.UserController.getLocalUser);
  app.post('/api/local', Controllers.UserController.createOrUpdateLocalUserAPI);
  //app.post('/api/local/:email', Controllers.UserController.updateLocalUser);

  //Feedback Mailer
  app.post('/contact', Controllers.MailerController.feedbackMailer);

  //User Actions
  app.get('/api/user', Controllers.UserController.getSelf);
  app.get('/api/allUsers', Controllers.UserController.getAllUsers);
  //app.post('/api/registerNewUser', Controllers.UserController.registerNewUser);

  //Admin Actions
  app.get('/api/admin/race', Controllers.AdminController.OnlyAllowAdmins, Controllers.AdminController.getRaces);
  app.get('/api/admin/race/:raceId', Controllers.AdminController.OnlyAllowAdmins, Controllers.AdminController.getRace);
  app.get('/api/admin/race/:raceId/bibs', Controllers.AdminController.OnlyAllowAdmins, Controllers.AdminController.getBibs);
  app.get('/api/admin/race/:raceId/bibCount', Controllers.AdminController.OnlyAllowAdmins, Controllers.AdminController.getBibsCount);
  app.get('/api/admin/race/:raceId/activatedBibCount', Controllers.AdminController.OnlyAllowAdmins, Controllers.AdminController.getActivatedBibsCount);

  app.delete('/api/admin/race/:raceId', Controllers.AdminController.OnlyAllowAdmins, Controllers.AdminController.deleteRace);

  app.post('/api/admin/race', Controllers.AdminController.OnlyAllowAdmins, Controllers.AdminController.createRace);
  app.post('/api/admin/updateRaceDetails', Controllers.AdminController.OnlyAllowAdmins, Controllers.AdminController.updateRace);
  app.post('/api/admin/updateWaveDetails', Controllers.AdminController.OnlyAllowAdmins, Controllers.AdminController.updateWaveDetails);
  app.post('/api/admin/race/:raceId/bibs', Controllers.AdminController.OnlyAllowAdmins, Controllers.AdminController.createBibs);

  app.post('/api/admin/race/:raceId/:waveId/start', Controllers.AdminController.OnlyAllowAdmins, Controllers.AdminController.setWaveStart);
  app.post('/api/admin/race/:raceId/runners', Controllers.AdminController.OnlyAllowAdmins, Controllers.AdminController.addRunners);
  app.post('/api/admin/race/:raceId/published', Controllers.AdminController.OnlyAllowAdmins, Controllers.AdminController.togglePublished);
  app.post('/api/admin/race/:raceId/upcoming', Controllers.AdminController.OnlyAllowAdmins, Controllers.AdminController.toggleUpcoming);
  app.post('/api/admin/race/:raceId/hideRank', Controllers.AdminController.OnlyAllowAdmins, Controllers.AdminController.toggleHideRank);
  app.post('/api/admin/race/:raceId/updateRaceAddress', Controllers.AdminController.OnlyAllowAdmins, Controllers.AdminController.updateRaceAddress);
  app.post('/api/admin/race/:raceId/updateRaceDate', Controllers.AdminController.OnlyAllowAdmins, Controllers.AdminController.updateRaceDate);

  /* --removed caching-- problem with multiple drones  
  app.post('/api/admin/race/:raceId/:waveId/start', Controllers.AdminController.OnlyAllowAdmins, cacheController.clearWaveDetailsCache, Controllers.AdminController.setWaveStart);
  app.post('/api/admin/race/:raceId/runners', Controllers.AdminController.OnlyAllowAdmins, cacheController.clearRunnersAndWaveCache, Controllers.AdminController.addRunners);
  app.post('/api/admin/race/:raceId/published', Controllers.AdminController.OnlyAllowAdmins, cacheController.clearWaveDetailsCache, cacheController.clearRaceListCache, Controllers.AdminController.togglePublished);
  app.post('/api/admin/race/:raceId/upcoming', Controllers.AdminController.OnlyAllowAdmins, cacheController.clearWaveDetailsCache, cacheController.clearRaceListCache, Controllers.AdminController.toggleUpcoming);
  app.post('/api/admin/race/:raceId/hideRank', Controllers.AdminController.OnlyAllowAdmins, cacheController.clearWaveDetailsCache, Controllers.AdminController.toggleHideRank);
  app.post('/api/admin/race/:raceId/updateRaceAddress', Controllers.AdminController.OnlyAllowAdmins, cacheController.clearWaveDetailsCache, Controllers.AdminController.updateRaceAddress);
  */

  app.post('/api/admin/downloadCodes/:raceId',Controllers.AdminController.OnlyAllowAdmins, Controllers.AdminController.downloadCodes);
  app.post('/api/admin/rebuildWaveOrder/', Controllers.AdminController.OnlyAllowAdmins, Controllers.AdminController.rebuildWaveOrder);
  app.post('/api/admin/uploadRacePhoto/:raceId', Controllers.AdminController.OnlyAllowAdmins, Controllers.AdminController.uploadRacePhoto);
  app.post('/api/admin/createNewAdminUser', Controllers.AdminController.OnlyAllowAdmins, Controllers.AdminController.createNewAdminUser);
  app.post('/api/admin/uploadDetectionsForPostProcess', Controllers.AdminController.OnlyAllowAdmins, Controllers.AdminController.uploadDetectionsForPostProcess);
  app.post('/api/admin/uploadEntryListForPostProcess', Controllers.AdminController.OnlyAllowAdmins, Controllers.AdminController.uploadEntryListForPostProcess);
  
  //Public Runner Endpoints  
  app.get('/api/results/:userId', Controllers.RunnerController.getAllResultsById);
  app.get('/api/bib/:userId', Controllers.RunnerController.getActivatedBibsById);
  app.get('/api/runners/:raceId/bibnumber/:bibnumber', Controllers.RunnerController.get);
  app.post('/api/validateBib', Controllers.UserController.activateBib);
  app.post('/api/disconnectWithResult', Controllers.UserController.disconnectBibWithResult);
  app.post('/api/disconnectUpcoming', Controllers.UserController.disconnectUpcoming);

  //Public Race Endpoints
  app.get('/api/race/:raceId', Controllers.RaceController.getRace);
  app.get('/api/race/nick/:raceNick', Controllers.RaceController.getRaceByNick);
  app.get('/api/races/available/:pageNum',Controllers.RaceController.getAvailableRaces);
  app.get('/api/races/upcoming/:pageNum',Controllers.RaceController.getUpcomingRaces);
  app.get('/api/allRaces', Controllers.RaceController.getAllPublishedRaces);
  app.get('/api/combinedRaces/:pageNum', Controllers.RaceController.getRaces);

  app.get('/api/races/search', Controllers.RaceController.getFilteredRaces);
  
  /* --removed caching-- problem with multiple drones  
  app.get('/api/race/:raceId', apiCacheMiddleware('1 day'), Controllers.RaceController.getRace);
  app.get('/api/race/nick/:raceNick', apiCacheMiddleware('1 day'), Controllers.RaceController.getRaceByNick);
  app.get('/api/races/available/:pageNum', apiCacheMiddleware('1 day'), Controllers.RaceController.getAvailableRaces);
  app.get('/api/races/upcoming/:pageNum', apiCacheMiddleware('1 day'), Controllers.RaceController.getUpcomingRaces);
  app.get('/api/allRaces', apiCacheMiddleware('1 day'), Controllers.RaceController.getAllPublishedRaces);
  app.get('/api/combinedRaces/:pageNum', apiCacheMiddleware('1 day'), Controllers.RaceController.getRaces);
  */
  
  //Wave Endpoint
  app.get('/api/race/:raceId/:waveId/:pageNum', Controllers.RaceController.getWave);
  app.get('/api/raceByChip/:raceId/:waveId/:pageNum', Controllers.RaceController.getWaveByChip);
  app.get('/api/raceByBib/:raceId/:waveId', Controllers.RaceController.getWaveByBib);
  app.get('/api/race/:raceId/:waveId', Controllers.RaceController.getAllRunnersInWave);
  app.get('/api/wave/:raceId/:waveId', Controllers.RaceController.getWaveDetails);

  /* --removed caching-- problem with multiple drones  
  app.get('/api/race/:raceId/:waveId/:pageNum', apiCacheMiddleware('1 day'), Controllers.RaceController.getWave);
  app.get('/api/race/:raceId/:waveId', apiCacheMiddleware('1 day'), Controllers.RaceController.getAllRunnersInWave);
  app.get('/api/wave/:raceId/:waveId', apiCacheMiddleware('1 day'), Controllers.RaceController.getWaveDetails);
  */
  

  //Aux -- race locations
  //app.get('/api/races/locations', apiCacheMiddleware('1 day'), Controllers.RaceController.getRaceLocations);
  //app.post('/api/races/locations', Controllers.AdminController.OnlyAllowAdmins, cacheController.clearLocationsCache, Controllers.RaceController.updateRaceLocations);
  app.get('/api/races/locations', Controllers.RaceController.getRaceLocations);
  app.post('/api/races/locations', Controllers.AdminController.OnlyAllowAdmins, Controllers.RaceController.updateRaceLocations);
  

  //MyRunTime User Endpoints
  app.get('/api/user/:userId', Controllers.UserController.getByUserId);
  app.post('/api/user/getFriendsDetails', Controllers.UserController.getFriendsDetails);
  app.post('/api/submitWeight', Controllers.UserController.submitWeight);
  app.post('/api/activateUserAccount', Controllers.UserController.activateUserAccount);
  
};

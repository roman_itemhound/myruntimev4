var AdminController = require('../controllers').AdminController;

module.exports = function (app) {
  
  
  //REST ENDPOINTS
  app.get('/', function(req, res){
    //res.sendfile('./public/index.html');
    res.render('index');
    //res.render('index');
  });

  app.get('/admin', AdminController.OnlyAllowAdmins, function(req, res){
    //res.sendfile('./public/adminIndex.html');
    res.render('adminIndex');
  });

  //facebook canvas
  app.post('/', function(req, res){
    //res.sendfile('./public/index.html');
    res.render('index');
  });

  //Redirects to angular routes
  app.get('/race/:raceNick', function(req, res){
    res.redirect('#/race/'+ req.params.raceNick);
  });

  app.get('/races/:raceId/:waveId', function(req, res){
    res.redirect('#/race/' + req.params.raceId + '/' + req.params.waveId);
  });

  app.get('/connect', function(req, res){
    res.redirect('#/connect');
  });

  app.get('/me', function(req,res){
    res.redirect('#/me');
  });

  app.get('/adminlogin', function(req, res){
    res.redirect('#/adminlogin');
  });

  app.get('/login', function(req, res){
    res.redirect('#/login');
  });

  app.get('/contact', function(req, res){
    res.redirect('#/contact');
  });

  app.get('/faqs', function(req, res){
    res.redirect('#/faqs');
  });

  app.get('/races', function(req, res){
    res.redirect('#/races');
  });

  app.get('/runners/:raceId/:bibNumber', function(req, res){
    res.redirect('#/runner/' + req.params.raceId + "/" + req.params.bibNumber);
  });
};
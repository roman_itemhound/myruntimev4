/*
  Module for computing time
*/

exports.ComputeTimeDifference = function(startTime, endTime){
  return ((endTime.getTime() - startTime.getTime())/1000);
};
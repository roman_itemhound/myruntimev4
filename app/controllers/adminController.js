'use strict';

var csv = require('ya-csv');
var pkgcloud = require('pkgcloud');
var fs = require('fs');
var appConfiguration = require('../../config/env').config();
var utilityFunctions = require('../utilities');
var async = require('async');

//Initialize settings for cloud hosting
var rackspace = pkgcloud.storage.createClient({
  provider: appConfiguration.rackspace.provider,
  username: appConfiguration.rackspace.username,
  apiKey: appConfiguration.rackspace.apiKey,
  region: appConfiguration.rackspace.region
});

/*
  Computes gun and chip times for all runners;
  runners - Array of objects from csv
  wave  - wave where the runners belong  
*/
var computeGunTimes = function(params){
  var wave = params.wave;
  var WaveStart = new Date(wave.waveStart);
  var GMTOffset = WaveStart.getTimezoneOffset();          
  //var WaveStartLocal = new Date(WaveStart.getTime() + (GMTOffset* 60000));
  var PHOffset = 8*60*60*1000; //GMT +8
  var WaveStartLocal = new Date(WaveStart.getTime() + (GMTOffset * 60000) + PHOffset);  
  var WaveYearLocal = WaveStartLocal.getFullYear();
  var WaveMonthLocal = WaveStartLocal.getMonth() + 1;
  var WaveDateLocal = WaveStartLocal.getDate();
  
  var WaveYear = WaveStart.getFullYear();
  var WaveMonth = WaveStart.getMonth() + 1;
  var WaveDate = WaveStart.getDate();

  var SplitIDs = [];
  if (wave.splits.length !== 0){
    var splits = wave.splits;
    splits.sort(function(a,b){
      return (a.splitDistance - b.splitDistance);
    });
    for(var ctr = 0; ctr < splits.length; ctr++) {
      SplitIDs.push(splits[ctr].splitName);
    }
  }

  var runners = params.runners.map(function(data){
    var StartTime = new Date(WaveYear + '-' + (WaveMonth) + '-' + (WaveDate) + ' ' + data.startTime);
    var EndTime = new Date(WaveYear + '-' + (WaveMonth) + '-' + (WaveDate) + ' ' + data.endTime);

    var StartTimeLocal = new Date(WaveYearLocal + '-' + (WaveMonthLocal) + '-' + (WaveDateLocal) + ' ' + data.startTime);
    var EndTimeLocal = new Date(WaveYearLocal + '-' + (WaveMonthLocal) + '-' + (WaveDateLocal) + ' ' + data.endTime);
    //var GunTime = (EndTime.getTime() - WaveStart.getTime()) / 1000;
    var GunTime = (EndTimeLocal.getTime() - WaveStartLocal.getTime()) / 1000;
    var ChipTime = (EndTime.getTime() - StartTime.getTime()) / 1000;
    data.startTime = StartTimeLocal;
    data.endTime = EndTimeLocal;
    data.gunTime = GunTime;
    data.chipTime = ChipTime;
    data.sectorTimes = [];
    var sectorTime;
    var lastDistance;


    //Populate Splits
    for (var i = 0; i < SplitIDs.length; i++){
      if (data[SplitIDs[i]] === ""){
        continue;
      }
      sectorTime = {};
      if (data.sectorTimes.length === 0){
        sectorTime.sectorStart = StartTime;
        sectorTime.sectorLength = splits[i].splitDistance;
        lastDistance = sectorTime.sectorLength;
        sectorTime.sectorEnd = new Date(WaveYear +"/" + WaveMonth+ "/" + WaveDate + " "+ data[SplitIDs[i]]); 
      }
      else{
        sectorTime.sectorStart = data.sectorTimes[data.sectorTimes.length-1].sectorEnd;
        sectorTime.sectorLength = splits[i].splitDistance - lastDistance;
        lastDistance = splits[i].splitDistance;
        sectorTime.sectorEnd = new Date(WaveYear +"/" + WaveMonth +"/" + WaveDate + " "+ data[SplitIDs[i]]); 
      }
      data.sectorTimes.push(sectorTime);
    }

    if (data.sectorTimes.length > 0 ){              
      sectorTime = {};
      sectorTime.sectorStart = data.sectorTimes[data.sectorTimes.length-1].sectorEnd;
      sectorTime.sectorLength = wave.waveDistance  - lastDistance;
      sectorTime.sectorEnd = EndTime;
      data.sectorTimes.push(sectorTime);
    }
    return data;
  });
  return runners;
};

/*
 * Parse uploaded results
 */
var parseRunnerFile = function(params, callback){
  var ColumnNames = ['bibnumber', 'lastName', 'firstName', 'middleName', 'gender', 'nationality', 'startTime', 'endTime'];
  var SplitIDs = [];
  var race = params.race;
  var wave = null;
  
  for (var i = 0; i < race.Model.waves.length; i++){
    if (race.Model.waves[i].waveId === params.waveId){
      wave = race.Model.waves[i];
      break;
    }
  }

  if (!wave) { callback("waveId does not exist"); return; }

  //Populate column names
  if (wave.splits.length !== 0){
    var splits = wave.splits;
    splits.sort(function(a,b){
      return (a.splitDistance - b.splitDistance);
    });
    for(var ctr = 0; ctr < splits.length; ctr++) {
      ColumnNames.push(splits[ctr].splitName);
      SplitIDs.push(splits[ctr].splitName);
    }
  }

  var reader = csv.createCsvFileReader(params.path, {
      columnsFromHeader : false,
      encoding: 'utf8'
  });
  reader.setColumnNames(ColumnNames);
  
  var headerFound = false;

  reader.addListener('end', function(){
    callback(null, runners);
  });

  reader.addListener('data', function(data){
    if (!headerFound) {
      headerFound = true;
      return;
    }
    runners.push(data);
  });
  var runners = [];
};


//-----------------
// Permissions
//-----------------
exports.OnlyAllowAdmins = function (req, res, next) {  
  if (!req.user){
    console.log("no user is defnedd sa middleware");
    next("No user defined"); 
    return; 
  }
  else{
    if(!req.user.isAdmin){
      next("User is not allowed to access");
    }
    else{
      next();
    } 
  }    
};


//-----------------
// Race
//-----------------

exports.getRace = function(req, res){
  var User = req.user;
  User.GetRace({"raceId":req.params.raceId}, function(err, race){
    if (err){
      res.status(400).send(err);
    } 
    else{
      res.send(race);
    }
  });
};

exports.getRaces  = function (req, res) {
  var User = req.user;
  User.GetRaces(function(err, races){
    if (err){
      res.status(400).send(err);
    } 
    else{
      res.send(races);
    }
  });
};

exports.createRace = function(req, res){
  var race = JSON.parse(req.body.race);
  if (!req.files.racePhoto) {
    race.racePhoto = 'nophoto.png';
  }
  else {
    race.racePhoto = req.files.racePhoto.name;
  }
  var User = req.user;
  User.CreateRace(race, function(err, race){
    if (err){
      res.status(400).send(err);
    }
    else{
      res.send(race);
    }
  });
};

exports.updateRace = function(req, res){
  var User = req.user;
  var race = req.body.race;
  User.UpdateRace(race, function(err, race){
    if(err){
      res.status(400).send(err);
    } 
    else{
      res.send(race);
    }
  });
};

exports.deleteRace = function(req, res){
  var User = req.user;
  User.DeleteRace({"raceId":req.params.raceId}, function(err){
    if (err){
      res.status(400).send(err);
    } 
    else{
      res.send("OK");
    }
  });
};

exports.togglePublished = function(req, res){
  var User = req.user;
  User.TogglePublished({"raceId":req.params.raceId}, function(err,  result){
    if (err){
      res.status(400).send(err);
    }
    else{
      res.send(result);
    }
  });
};

exports.toggleUpcoming = function(req, res){
  var User = req.user;
  User.ToggleUpcoming({"raceId":req.params.raceId}, function(err, result){
    if (err){
      res.status(400).send(err);
    }
    else{
      res.send(result);   
    }
  });
};

exports.toggleHideRank = function(req, res){
  var User = req.user;
  User.ToggleHideRank({"raceId":req.params.raceId}, function(err, result){
    if (err){
      res.status(400).send(err);
    }
    else{
      res.send(result);   
    }
  });
};

exports.updateRaceAddress = function(req, res){
  var User = req.user;
  User.UpdateRaceAddress({"raceId":req.params.raceId, "raceAddress": req.body.raceAddress}, function(err, result){
    if (err){
      res.status(400).send(err);
    }
    else{
      res.send(result);   
    }
  });
};

exports.updateRaceDate = function(req, res){
  var User = req.user;
  User.UpdateRaceDate({"raceId":req.params.raceId, "raceDate": req.body.raceDate}, function(err, result){
    if (err){
      res.status(400).send(err);
    }
    else{
      res.send(result);   
    }
  });
};

exports.uploadRacePhoto = function(req, res){
  var User = req.user;
  var raceId = req.params.raceId;
  var raceName = JSON.parse(req.body.race).raceName;
  var photo = req.files.racePhoto;

  var temp_path = req.files.racePhoto.path;
  var fileName = raceName + (new Date()).getTime() + photo.name;
  var is = fs.createReadStream(temp_path);

  is.pipe(rackspace.upload({
    container: 'myruntime-cdn',
    remote: '/images/' + fileName,    
  },function(err){
    if(err) {
      res.status(500).send(err);
    }
    else{
      User.UploadRacePhoto({"raceId": raceId, "fileName":fileName}, function(err){
        if(err){
          res.status(500).send(err);
        }
        else{
          fs.unlinkSync(temp_path);
          res.send();
        }
      });
    }
  }));
};

exports.updateWaveDetails = function(req, res){
  var User = req.user;
  User.UpdateWaveDetails({"raceId": req.body.raceId, "waveDetails": req.body.waveDetails, "user": User}, function(err){
    if(err) {
      res.status(400).send(err);
    }
    else{
      res.send({"Status":"OK"});      
      User.UpdateWaveResult({"raceId":req.body.raceId, "waveId": req.body.waveDetails.waveId}, function(err){
        if(err){
          res.status(400).send(err);
        }
        else{          
          res.send({"Status":"OK"});
        }
      });
    }
  });
};

exports.setWaveStart = function(req, res){
  var User = req.user;
  User.GetRace({"raceId":req.params.raceId}, function(err){
    if (err){
      res.status(400).send(err);
    }
    else{
      var waveStart = new Date(req.body.waveStart);
      User.SetWaveStart({
        "raceId" : req.params.raceId,
        "waveId" : req.params.waveId,
        "waveStart" : waveStart
      }, function(err){
        if(err){
          res.status(400).send(err);
        }
        else{
          User.UpdateWaveResult({"raceId": req.params.raceId, "waveId": req.params.waveId}, function(err){
            if(err){
              res.status(400).send(err);
            }
            else{        
              res.send({"Status":"OK"});
            }
          });
        }        
      });
    }
  });
};

exports.rebuildWaveOrder = function(req, res){
  var User = req.user;
  var errorArray = [];

  //get All Races
  User.GetRaces(function(err, races){
    if (err){
      res.status(400).send(err);
    }
    else{

      async.eachSeries(races, function(race, callback){
        User.RebuildWaveOrder({"raceId":race.Model.raceId}, function(err){
          if(err){
            errorArray.push(err);
          }
          callback();
        });
      }, function(err){
        //should never get error here
        console.log(err);
        if(errorArray.length !== 0){
          res.status(400).send(errorArray);
        } 
        else{
          res.send("OK");
        }     
      });
    }
  });
};


//-----------------
// Bibs
//-----------------

exports.createBibs = function(req, res){
  if (!req.files.bibs) { res.status(400).send("File not received"); return; }
  
  var ColumnNames = ['bib', 'activationCode'];
  var reader = csv.createCsvFileReader(req.files.bibs.path, {
    columnsFromHeader : false,
    encoding: 'utf8'
  });
  reader.setColumnNames(ColumnNames);
  var bibs = [];
  var User = req.user;
  reader.addListener('end', function(){
    User.CreateBibs({"bibs":bibs, "raceId":req.params.raceId },function(err, newbibs){
      if (err) { res.status(400).send(err); }
      else { res.send(newbibs); }
    });
  });
  var headerFound = false;  
  reader.addListener('data', function(data){
    if (!headerFound) {
      headerFound = true;
      return;
    }
    bibs.push(data.bib);
  });
};

exports.getBibs = function(req, res){
  var User = req.user;
  User.GetBibs({"raceId":req.params.raceId},function(err, result){
    if (err){
      res.status(400).send(err);
    }
    else{
      res.send({"Result":result} ) ;
    }
  });
};

exports.getBibsCount = function(req, res){
  var User = req.user;
  User.GetBibsCount({"raceId": req.params.raceId}, function(err, count){
    if(err){
      res.status(400).send(err);
    }
    else{
      res.send({"Count": count});
    }
  });
};

exports.getActivatedBibs = function(req, res){
  var User = req.user;
  User.GetActivatedBibs({"raceId":req.params.raceId},function(err, result){
    if (err){
      res.status(400).send(err);
    }
    else{
      res.send({"Result":result}) ;
    }
  });
};

exports.getActivatedBibsCount = function(req, res){
  var User = req.user;
  User.GetBibsCount({"raceId": req.params.raceId, "activated":true}, function(err, count){
    if(err){
      res.status(400).send(err);
    }
    else{
      res.send({"Count": count});
    }
  });
};

exports.updateActivatedBibs = function(req, res){
  var User = req.user;
  var errorsList = [];
  User.GetBibs({"activated":true}, function(err, activatedBibs){
    if(err) {
      res.status(400).send(err);
    }
    else{
      activatedBibs.forEach(function(activatedBib){
        var params = {};
        params.raceId = activatedBib.raceId;
        params.bibnumber = activatedBib.bibnumber;
        params.userId = activatedBib.userId;
        User.UpdateActivatedBibs(params, function(errorsList){
          if(err){
            errorsList.push(err);
          }
        });
      });
      if(errorsList.length > 0){
        res.send(errorsList);
      }
      else{
        res.send("L");
      }
    }
  });
};

exports.createNewAdminUser = function(req, res){
  var user = req.user;
  user.createUser({"email":req.body.username, "password":req.body.password, "firstName":req.body.firstName, "lastName":req.body.lastName}, function(err){
    if(err){
      res.status(400).send(err);
    }
    else{      
      res.send({"Status":"OK"});
    }
  });
};

exports.addRunners = function(req, res){
  var user = req.user;
  if (!req.files.runners) { res.status(400).send("File not received"); return; }
  var file = req.files.runners;
  var waveId = (JSON.parse(req.body.wave)).waveId;
  var raceId = req.params.raceId;
  
  user.GetRace({"raceId":req.params.raceId}, function(err, race){
  
    if (err) { res.status(400).send(err); return; }
    parseRunnerFile({
      "path":file.path,
      "race": race,
      "waveId":waveId

    }, function(err, runners){
      var wave = [];
      wave = race.Model.waves.filter(function(wave){
        return (wave.waveId === waveId);
      });
      
      if (wave.length === 0){
        req.status(400).send("Wave Not Found");
        return;
      }

      //Populate race ID and wave ID
      runners = runners.map(function(runner){
        runner.waveId = waveId;
        runner.raceId = raceId;
        return runner;
      });

      runners = computeGunTimes({
        "runners" : runners,
        "wave":wave[0]
      });

      user.AddResults({"raceId":req.params.raceId, runners:runners, "waveId":waveId}, function(err, result){
        if (err){
          res.status(400).send(err);
        } 
        else{ 
          res.send(result);
        }
      });
    });
  }); 
};

exports.downloadCodes = function(req, res){
  var User = req.user;
  User.GetBibs({"raceId":req.params.raceId},function(err, result){
    if (err){
      res.status(400).send(err);
    }
    else{
      var rawCSV = "Bib,Code\r\n";

      for(var i = 0; i < result.length; i++){
        rawCSV+= result[i].bibnumber + "," + result[i].activationCode + "\r\n";
      }

      res.send(rawCSV);
    } 
  });
};


//-----------------
// Post Process
//-----------------

exports.uploadDetectionsForPostProcess = function(req, res){
  var detections = [];

  if (!req.files.detections) { res.status(400).send("File not received"); return; }
  
  var ColumnNames = ['EPC', 'Detection', 'Antenna'];
  var reader = csv.createCsvFileReader(req.files.detections.path, {
    columnsFromHeader : false,
    encoding: 'utf8'
  });
  
  reader.setColumnNames(ColumnNames);
  
  reader.addListener('end', function(){            
    res.send(detections);
  });

  var headerFound = false;  
  
  reader.addListener('data', function(data){
    if (!headerFound) {
      headerFound = true;
      return;
    }
    if(data.EPC.length === 8){
      detections.push({
        EPC: data.EPC,
        Detection: data.Detection
      });
    }  
  }); 
};

exports.uploadEntryListForPostProcess = function(req, res){

  var entryListData = [];

  if (!req.files.entrylist) { res.status(400).send("File not received"); return; }  
  
  var ColumnNames = ['bibnumber', 'epc', 'waveName', 'gender', 'firstName', 'lastName', 'team', 'age', 'numLaps', 'runnerClass'];  
  var reader = csv.createCsvFileReader(req.files.entrylist.path, {
    columnsFromHeader : false,
    encoding: 'utf8',
    'separator': '\t',
  });

  reader.setColumnNames(ColumnNames);

  reader.addListener('end', function(){
    res.send(entryListData);
  });

  var headerFound = false;

  reader.addListener('data', function(data){
    if (!headerFound) {
      headerFound = true;
      return;
    }    
    entryListData.push({
      bib: data.bibnumber,
      epc: data.epc,
      firstName: utilityFunctions.toProper(data.firstName),
      lastName: utilityFunctions.toProper(data.lastName),
      gender: data.gender
    });    
  });
};

'use strict';

var nodemailer = require("nodemailer");
var appConfiguration = require('../../config/env').config();

var smtpTransport = nodemailer.createTransport("SMTP",{
  service: appConfiguration.SMTP.service,
  auth: {
    user: appConfiguration.SMTP.user,
    pass: appConfiguration.SMTP.pass
  }
});

// setup e-mail data with unicode symbols
var mailOptions = {    
  to: appConfiguration.SMTP.forwarder, // list of receivers        
};

exports.feedbackMailer = function(req, res){
  mailOptions.subject = "[STRIDER FEEDBACK] "+ req.body.inquiry;
  mailOptions.text = req.body.eventName + " \r\n " + req.body.name + ' --- ' + req.body.email + " \r\n " + req.body.feedback;
  mailOptions.from = req.body.name + " ---- " + req.body.eventName + " \r\n " + req.body.email;
  smtpTransport.sendMail(mailOptions, function(error){
    if(error){
      res.send(error);
    }
    else{
      /*jshint multistr:true */
      smtpTransport.sendMail({
        from: 'Team Strider <strider@itemhound.com>',
        subject: 'Thank you for your inquiry',
        text: 'Your problem has been received. Our customer service team are now working to rectify your concerns. \r\n\r\n \
          For MISSING TIMES, it will be very helpful if you could send us an estimate of your finish time, or the bib numbers of the running buddies you finished with. \r\n\r\n \
          For INCORRECT NAMES, please ensure that you have sent us the bib numbers and the corresponding correct names. \r\n\r\n \
          For ACTIVATION PROBLEMS, please include the variant of your browser and your bibnumber in your message \r\n\r\n \
          For BOOKING INQUIRIES, our sales representative will contact you as soon as possible. \r\n\r\n \
          Thank you for running with Strider My Run Time. =)',
        to: req.body.email
      }, function(err, response){        
        res.send(response);  
      });
    }
  });
};

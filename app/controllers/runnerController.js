'use strict';

var Result = require('../Objects/Result');
var Bib = require('../Objects/Bib');
var async = require('async');

exports.get = function (req, res) {
  if ((!req.params.raceId)||(!req.params.bibnumber)) { res.status(400).send("Incomplete parameters"); return; }

  Result.fetchResult(req.params, function(err,  runner){
    if (err) { res.status(400).send(err); return; } 

    if(runner){
      if(runner.Model.activated){
        /* Mabagal na function ito!! Daming DB Calls din! Baka magkaproblem sa future*/

        var runnerFriends = runner.Model.userId.friends.map(function(friend){
          return{
            userId: friend,
            raceId: req.params.raceId
          };
        });

        var runnerFriendsResult = [];
        async.each(runnerFriends, function(runnerFriend, callback){
          Result.fetchResultByRaceAndId({"raceId": runnerFriend.raceId, "userId": runnerFriend.userId}, function(err, result){
            if(result){
              var friendResult = {
                faceBookId: result.Model.userId.faceBookId,
                firstName: result.Model.firstName,
                lastName: result.Model.lastName,
                waveId: result.Model.waveId,
                raceId: result.Model.raceId._id,
                chipTime: result.Model.chipTime,
                gunTime: result.Model.gunTime,
                bibnumber: result.Model.bibnumber
              };
              runnerFriendsResult.push(friendResult);
            }
            callback();
          });
        }, function(err){
          if(!err){
            runner.friendsResults = runnerFriendsResult;
            res.send(runner);
          }
          else{
            res.send(null);
          }            
        });
      }
      else{
        runner.friendsResults = [];
        res.send(runner);
      }      
    }
    else{
      res.send(null);
    }      
  });
};

exports.getByUserId = function(req, res){
  if (!req.params.userId) { res.status(400).send("Incomplete parameters"); return; }
  Result.fetchResultById(req.params, function(err, runner){
    if (err) { res.status(400).send(err); return; }    
    res.send(runner.Model);
  });  
};

exports.getAllResultsById = function(req, res){
  Result.fetchResultsById(req.params, function(err, results){
    if(err) { res.status(400).send(err); return; }
    res.send(results);
  });
};

exports.getActivatedBibsById = function(req, res){
  Bib.fetchUpcomingBibsByUserId(req.params, function(err, results){
    if(err) { res.status(400).send(err); return; }
    res.send(results);
  });
};

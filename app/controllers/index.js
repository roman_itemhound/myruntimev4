'use strict';

var adminController = require('./adminController');
var userController = require('./userController');
var raceController = require('./raceController');
var runnerController = require('./runnerController');
var authController = require('./authController');
var mailerController = require('./mailerController');
var cacheController = require('./cacheController');

exports.UserController = userController;
exports.AdminController = adminController;
exports.RaceController = raceController;
exports.RunnerController = runnerController;
exports.MailerController = mailerController;
exports.AuthController = authController;
exports.CacheController = cacheController;
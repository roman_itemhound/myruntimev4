'use strict';

var User = require('../Objects/User');
var passport = require('passport');

exports.adminSignin = function(req, res, next) {
  passport.authenticate('local', function(err, user){
    if(err || !user){
      console.log(err);
      res.status(401).send(err);
    }
    else{
      if(user.Model.userClass > 0){
        res.status(403).send('User not permitted to view resource');
      }
      else{
        req.login(user, function(err){
          if(err){
            res.status(400).send(err);
          }
          else{
            res.send(user);
          }
        });
      } 
    }
  })(req, res, next);
};

exports.userSignin = function(req, res, next) {
  passport.authenticate('local', function(err, user){
    if(err || !user){
      console.log(err);
      res.status(401).send(err);
    }
    else{
      if(user.Model.activated){
        req.login(user, function(err){
          if(err){
            res.status(400).send(err);
          }
          else{            
            res.send(user);
          }
        });
      }
      else{
        res.status(403).send("User account not verified. Kindly check your email to activate your account.");
      }
        
    }
  })(req, res, next);
};

exports.registerNewUser = function(req, res){
  var params = req.body;
  params.gender = params.gender.value;
  User.createNewLocalUser(params, function(err, result){
    if(err){
      res.status(400).send(err);
    }
    else{
      res.send(result);
    }
  });  
};

exports.validateActivateToken = function(req, res){
  User.fetchUserByActivationToken(req.params.token, function(err, result){
    if(err){      
      console.log(err);
      res.redirect('#/invalidAccountActivation');
    }
    else{
      if(!result){
        res.redirect('#/invalidAccountActivation');
      }
      else{
        //activate user and login
        result.ActivateUserAccount({"activated": true}, function(err, user){
          if(err){
            res.status(400).send(err);
          }
          else{
            User.fetchUser(user._id, function(err, _result){
              if(err || !_result){
                console.log(err);
                res.status(400).send(err);
              }
              else{
                req.login(_result, function(err){
                  if(err){
                    res.status(400).send(err);
                  }
                  else{
                    res.redirect('#/successAccountActivation');
                  } 
                });           
              }
            }); 
          }
        });                
      }      
    }
  });
};


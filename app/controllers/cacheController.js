'use strict';

var _apiCache;

exports.setGlobalApiCache = function(apiCache){
  _apiCache = apiCache;
};

//Manual Cache Clear Middleware
exports.clearLocationsCache = function(req, res, next){
  _apiCache.clear('raceLocations');
  next();
};

exports.clearRaceListCache = function(req, res, next){
  _apiCache.clear('raceListCache');
  next();
};

exports.clearRaceDetailsCache = function(req, res, next){
  var raceId = req.params.raceId;
  var raceCollectionName = "RaceDetails-Race" + raceId;
  _apiCache.clear(raceCollectionName);
  next();
};

exports.clearWaveDetailsCacheByBody = function(req, res, next){
  var raceId = req.body.race.raceId;
  var raceCollectionName = "RaceDetails-Race" + raceId;
  _apiCache.clear(raceCollectionName);
  next();
};

exports.clearWaveDetailsCache = function(req, res, next){
  var waveId = req.params.waveId;
  var raceId = req.params.raceId;
  var waveCollectionName = "WaveDetails-Race" + raceId + "-Wave" + waveId;
  _apiCache.clear(waveCollectionName);
  next();
};

exports.clearRunnersAndWaveCache = function(req, res, next){
  var waveId = (JSON.parse(req.body.wave)).waveId;
  var raceId = req.params.raceId;

  var runnersCollectionName = "Runners-Race" + raceId + "-Wave" + waveId;
  var waveCollectionName = "WaveDetails-Race" + raceId + "-Wave" + waveId;
  _apiCache.clear(runnersCollectionName);
  _apiCache.clear(waveCollectionName);
  next();
};

'use strict';

var Race = require('../Objects/Race');
var RaceLocation = require('../Objects/RaceLocation');
var _ = require('lodash');
//var async = require('async');

exports.getRace = function (req, res) {

  /* --removed caching-- problem with multiple drones
  var groupName = 'RaceDetails-Race' + req.params.raceId;
  req.apicacheGroup = groupName;
  */

  Race.fetchRace({"find":{"raceId":req.params.raceId}, select : "raceId raceName raceNick raceDate racePhoto raceDescription raceAddress raceMap hideRank published isThirdPartyRace waves"}, function(err, race){
    if (err) { res.status(400).send(err); return; }
    race.Model.waves = race.Model.waves.map(function(wave){
      return{
        waveName: wave.waveName,
        waveId: wave.waveId,
        thirdPartyWaveUrl: wave.thirdPartyWaveUrl
      };
    });
    res.send(race.Model);
  });
};

exports.getRaceByNick = function(req, res){
  Race.fetchRace({"find":{"raceNick":req.params.raceNick}, select : "raceId raceName raceNick raceDate racePhoto raceDescription raceAddress raceMap hideRank published isThirdPartyRace waves"}, function(err, race){
    if (err) { res.status(400).send(err); return; }
    race.Model.waves = race.Model.waves.map(function(wave){
      return{
        waveName: wave.waveName,
        waveId: wave.waveId,
        thirdPartyWaveUrl: wave.thirdPartyWaveUrl
      };
    });

    /* --removed caching-- problem with multiple drones
    var groupName = 'RaceDetails-Race' + race.Model.raceId;
    req.apicacheGroup = groupName;
    */

    res.send(race.Model);
  });
};

exports.getAvailableRaces = function(req, res){

  /* --removed caching-- problem with multiple drones
  req.apicacheGroup = 'raceListCache';
  */

  if (!req.params.pageNum) {req.params.pageNum = 0;}
  req.params["published"] = true;

  Race.fetchRaces({
    find: { "published" : true },
    pageNum : req.params.pageNum,
    select : "raceId raceName raceNick raceDate racePhoto raceDescription raceAddress"
  }, function(err, races){
    if (err) { res.status(400).send(err); return; }
    else {res.send(races);}
  });
};

exports.getAllPublishedRaces = function(req, res){

  /* --removed caching-- problem with multiple drones
  req.apicacheGroup = 'raceListCache';
  */

  Race.fetchRaces({
    find: { "published": true},
    select: "raceName"
  }, function(err, races){
    if (err) { res.status(400).send(err); return; }
    else {res.send(races);}
  });
};

exports.getUpcomingRaces = function(req, res){

  /* --removed caching-- problem with multiple drones
  req.apicacheGroup = 'raceListCache';
  */

  if (!req.params.pageNum) {req.params.pageNum = 0;}
  req.params["upcoming"] = true;
  Race.fetchRaces({
    find: { "upcoming" : true, "published":false },
    pageNum : req.params.pageNum,
    sort: {raceDate:1},
    select : "raceId raceName raceNick raceDate racePhoto raceDescription raceAddress upcoming published"
  }, function(err, races){
    if (err) { res.status(400).send(err); return; }
    else {res.send(races);}
  });
};

exports.getRaces = function(req, res){

  /* --removed caching-- problem with multiple drones
  req.apicacheGroup = 'raceListCache';
  */

  if (!req.params.pageNum) {req.params.pageNum = 0;}
  Race.fetchRaces({
    find: { $or: [{"published": true}, {"upcoming": true}]},
    pageNum: req.params.pageNum,
    sort: {raceDate:-1},
    select : "raceId raceName raceNick raceDate racePhoto raceDescription raceAddress published"
  }, function(err, races){
    if (err) { res.status(400).send(err); return; }
    else {res.send(races);}
  });
};

exports.getFilteredRaces = function(req, res){
  var filterParams = req.query;
  if(filterParams.raceName || filterParams.raceLocation || filterParams.month || filterParams.year){
    Race.fetchFilteredRaces(filterParams, function(err, races){
      if (err) { res.status(400).send(err); return; }
      else {res.send(races);}
    });    
  }
  else{
    res.status(400).send("Missing or invalid query params");
  }
};


exports.getWave = function(req, res){

  /* --removed caching-- problem with multiple drones
  //var groupName = "Runners-Race" + req.params.raceId + "-Wave" + req.params.waveId;
  //req.apicacheGroup = groupName;
  */

  req.params.pageNum = parseInt(req.params.pageNum, 10);
  Race.fetchRaceStub({"raceId":req.params.raceId}, function(err, race){
    if (err) { res.status(400).send(err); return; }

    if(req.params.pageNum === -1){
      race.GetRunnersInCategory({waveId:parseInt(req.params.waveId, 10), fetchAll:true}, function(err, runners){
        if (err) { res.status(400).send(err); return; }
        res.send(runners);
      });
    }
    else{
      race.GetRunnersInCategory({waveId:parseInt(req.params.waveId, 10), pageNum:req.params.pageNum, fetchAll:false}, function(err, runners){
        if (err) { res.status(400).send(err); return; }
        res.send(runners);
      });
    }
      
  });
};

exports.getWaveByChip = function(req, res){  

  req.params.pageNum = parseInt(req.params.pageNum, 10);
  Race.fetchRaceStub({"raceId":req.params.raceId}, function(err, race){
    if (err) { res.status(400).send(err); return; }

    if(req.params.pageNum === -1){
      race.GetRunnersInCategory({waveId:parseInt(req.params.waveId, 10), fetchAll:true, getByChipTime: true}, function(err, runners){
        if (err) { res.status(400).send(err); return; }
        res.send(runners);
      });
    }
    else{
      race.GetRunnersInCategory({waveId:parseInt(req.params.waveId, 10), pageNum:req.params.pageNum, fetchAll:false, getByChipTime: true}, function(err, runners){
        if (err) { res.status(400).send(err); return; }
        res.send(runners);
      });
    }
      
  });
};

exports.getWaveByBib = function(req, res){
  
  Race.fetchRaceStub({"raceId":req.params.raceId}, function(err, race){
    if (err) { 
      res.status(400).send(err); 
      return; 
    }
    else{
      race.GetRunnersInCategory({waveId:parseInt(req.params.waveId, 10), fetchAll:true, getByBib: true}, function(err, runners){
        if (err){ 
          res.status(400).send(err); 
          return;
        }
        else{
          res.send(runners);
        }
      });
    }
  });
};

exports.getWaveDetails = function(req, res){

  /* --removed caching-- problem with multiple drones
  var groupName = "WaveDetails-Race" + req.params.raceId + "-Wave" + req.params.waveId;
  req.apicacheGroup = groupName;
  */

  Race.fetchRace({"find":{"raceId":req.params.raceId}, select : "waves"}, function(err, race){    
    if(err){ 
      res.status(400).send(err); return;
    }
    else{      
      var matchedWave;
      race.Model.waves.forEach(function(wave){
        if(wave.waveId === parseInt(req.params.waveId,10)){
          matchedWave = wave;
        }
      });
      if(matchedWave){
        res.send(matchedWave);
      }
      else{
        res.status(400).send({"Message":"Wave not found"});
      }
    }
  });
};

exports.getAllRunnersInWave = function(req, res){

  /* --removed caching-- problem with multiple drones
  var groupName = "Runners-Race" + req.params.raceId + "-Wave" + req.params.waveId;
  req.apicacheGroup = groupName;
  */


  Race.fetchRaceStub({"raceId":req.params.raceId}, function(err, race){
    if (err) { res.status(400).send(err); return; }
    race.GetRunnersInCategory({waveId:parseInt(req.params.waveId, 10), fetchAll:true}, function(err, runners){
      if (err) { res.status(400).send(err); return; }
      res.send(runners);
    });
  });
};

exports.getRaceLocations = function(req, res){

  /* --removed caching-- problem with multiple drones
  req.apicacheGroup = 'raceLocations';
  */

  RaceLocation.fetchRaceLocations(function(err, result){
    if(err){
      res.status(400).send(err);
    }
    else if(!result){
      res.send([]);
    }
    else{
      res.send(result.locations);
    }
  });
};

exports.updateRaceLocations = function(req, res){
  var newLocation = req.body.location;
  RaceLocation.fetchRaceLocations(function(err, result){
    if(err){
      res.status(400).send(err);
    }
    else if(!result){
      res.send([]);
    }
    else{
      result.locations.push(newLocation);      
      result.locations = _.uniq(result.locations);

      result.save(function(err){
        if(err){
          res.status(400).send(err);
        }
        else{
          res.send(result.locations);
        }
      });
    }
  });
};
'use strict';

var User = require('../Objects/User');
var Bib = require('../Objects/Bib');
var Result = require('../Objects/Result');
var async = require('async');

exports.getSelf = function(req, res){
  res.send(req.user);  
};

exports.getAllUsers = function(req, res){
  //res.send(req.user);  
  User.getAllUsers(function(err, results){
    res.send(results);
  });
};


exports.activateBib = function (req, res) {
  if (!req.user){
    res.status(401).send("Not authorized to perform action");
  }
  else {
    req.user.ActivateBib(req.body, function(err){
      if (err){
        res.status(400).send(err);
      } 
      else { 
        res.send({"Message":"Ok"}); 
      }
    });
  }
};

exports.getByUserId = function(req, res){
  if (!req.params.userId) { res.status(400).send("Incomplete parameters"); return; }
  else{
    User.fetchUser(req.params.userId, function(err, user){
      if(err){
        res.status(400).send(err);
      }
      else{
        res.send(user);
      }
    });
  }  
};

exports.submitWeight = function(req, res){
  User.fetchUser(req.body.userId, function(err, user){
    user.SubmitWeight(req.body, function(err, result){
      if(err){
       res.status(400).send(err);
      }
      else{
        res.send(result);
      }
    });
  });
};

exports.activateUserAccount = function(req, res){
  User.fetchUser(req.body.userId, function(err, user){
    user.ActivateUserAccount(req.body, function(err, result){
      if(err){
        res.status(400).send(err);
      }
      else{
        res.send(result);
      }
    });
  });
};

exports.disconnectBibWithResult = function(req, res){

  Bib.fetchBib(req.body, function(err, bib){
    if(err){
      res.status(400).send(err);
    }
    else{
      if(bib){
        bib.Deactivate(function(err){
          if(err){
            res.status(400).send(err);
          }
          else{
            //fetch result            
            Result.fetchResultByRaceAndId({'bibnumber':req.body.bibnumber, 'raceId':parseInt(req.body.raceId, 10)}, function(err,result){
              if(err){
                res.status(400).send(err);
              }
              else{
                if(result){
                  result.DeactivateRunner(function(err, result){
                    if(err){
                      res.status(400).send(err);
                    }
                    else{
                      res.send(result);
                    }
                  });
                }
              }        
            });
          } 
        });            
      }
      else{
        res.status(400).send("Bib not found.");
      }
    }
  });
};

exports.disconnectUpcoming = function(req, res){
  
  Bib.fetchBib(req.body, function(err, bib){
    
    if(err){
      res.status(400).send(err);
    }
    
    else{
      if(bib){
        bib.Deactivate(function(err, result){
          if(err){
            res.status(400).send(err);
          }
          else{
            res.send(result);
          }
        });    
      }
      else{
        res.status(400).send("Bib not found!");
      }          
    }
  });
};

exports.getFriendsDetails = function(req, res){
  var friendIds = req.body.friendIds;
  var friendsDetails = [];

  async.each(friendIds, function(friendId, callback){
    User.fetchUser(friendId, function(err, result){
      if(result){
        friendsDetails.push(result.Model);
      }
      callback();
    });
  }, function(err){
    if(err){
      
      res.send(null);
    }
    else{
      res.send(friendsDetails);
    }
  });
};

/*
exports.findOrCreateFBUser = function(req, res){
  var params = {};
  params.accessToken = req.body.accessToken;
  params.fbProfile = req.body.profile._json;

  User.findOrCreateFBUser(params, function(err, result){
    if(err){
      res.status(500).send(err);
    }
    else{
      res.send(result);
    }
  });
};
*/

//MyRunTime Users API
exports.getFBUser = function(req, res){
  User.fetchUserByFb(req.params.facebookId, function(err, _user){
    if(err){
      res.status(400).send(err);
    }
    else{
      if(!_user){
        res.status(400).send("FB User not found");
      }
      else{
        res.send(_user);
      }
    }
  });
};

exports.getLocalUser = function(req, res){
  User.fetchUserByEmail(req.params.email, function(err, _user){
    if(err){
      res.status(400).send(err);
    }
    else{
      if(!_user){
        res.status(400).send("User not found");
      }
      else{
        res.send(_user);
      }
    }
  });
};

exports.createOrUpdateLocalUserAPI = function(req, res){
  //req.body.passkey = 
  if(req.body.apitoken === 'di ko pa maisip kung pano gagawing secure yung api'){
    User.createOrUpdateNewLocalUserByAPI(req.body, function(err, result){
      if(err){
        res.status(400).send(err);
      }
      else{
        res.send(result);
      }
    });        
  }
  else{
    res.status(401).send("Not allowed to edit and view resource!");
  }    
};

exports.createOrUpdateFBUserApi = function(req, res){
  if(req.body.apitoken === 'di ko pa maisip kung pano gagawing secure yung api'){
    User.findOrCreateFBUserByAPI(req.body, function(err, result){
      if(err){
        res.status(400).send(err);
      }
      else{
        res.send(result);
      }
    });
  }
  else{
    res.status(401).send("Not allowed to edit and view resource!");
  }
};
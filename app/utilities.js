exports.pipeToOutput = function(req, res){
  return function(err, result){
    if (err){ res.status(500).send(err);}
    else {res.send(result); } 
  };
};

exports.passErrOrResultToCallback = function(callback){
  return function(err, result){
    if (err) {callback(err);}
    else {
      if(callback){
        callback(null, result);
      }
    }
  };
};

exports.protectionLevels = {
  superAdmin: 1,
  regularAdmin: 2,
  storeAdmin: 20,
  publicUser:200
};

exports.redirectToHTTPS = function(req, res, next){
  if (req.secure){
     return next();
  }
  res.redirect("https://" + req.headers.host + req.url);  
};

exports.httpsProtect = function(req, res, next){
  if (!req.secure){
    res.status(400).send('Must send over https');
  } 
  else {
    next();
  }
};

var _hasOwnProperty = Object.prototype.hasOwnProperty;

exports.isEmpty = function (obj) {

    // null and undefined are "empty"
    if (obj == null) {return true;}

    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj.length && obj.length > 0) {return false;}
    if (obj.length === 0) {return true;}

    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and toValue enumeration bugs in IE < 9
    for (var key in obj) {
        if (_hasOwnProperty.call(obj, key)) {return false;}
    }

    return true;
};


exports.toUpper = function(value){
  return value.toUpperCase();
};

exports.toLower = function(value){
  return value.toLowerCase();
};

exports.toProper = function(value){
  if (!value) {return '';}

  var properCase = function(val){
    var _returnValue;
    if(val.length >= 1){
      _returnValue = val[0].toUpperCase() + val.substr(1).toLowerCase();
    }
    else{
      _returnValue = '';
    }
    return _returnValue;
  };

  var returnValue;
  if(value.length > 0){
    returnValue = value.split(' ').filter(function(a){      
      return (a.length >= 1);
    });
    returnValue = returnValue.map(properCase).join(' ');
  }
  else{
    returnValue = '';
  }
  //var returnValue = (value.length>0)?(value.split(' ').filter(function(a){return (a.length >= 1)};).map(properCase).join(' ')):('');
  return returnValue;
};


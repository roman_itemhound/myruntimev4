var testConfiguration = {
  port: process.env.PORT || 3000,
  dbConnectionString: 'mongodb://localhost:27017/strider-test',
  facebook:{
    client_id: '353126178090062', 
    client_secret: '564b4557771bea3db95624ac0c4ccd59',
    fbcallbackurl: 'http://107.170.8.212:3000/auth/facebook/callback', 
    scope: 'email, user_about_me, publish_actions'
  },
  localRegisterEndpoint: 'http://121.97.206.36:2013',
  rackspace: {
      provider: 'rackspace',
      username: 'ihcloudfiles',
      apiKey: '8546c92f5fdcf324aa11d6b55c62d317',
      region: 'SYD'
  },
  SMTP: {
    service: 'Gmail',
    forwarder: 'feedback@itemhound.com',
    user: 'strider@itemhound.com',
    pass: 'a8solutions'
  }
};

var devConfiguration = {
  port: process.env.PORT || 3000,
  dbConnectionString: 'mongodb://localhost:27017/strider',
  facebook:{
    client_id: '353126178090062',
    client_secret: '564b4557771bea3db95624ac0c4ccd59',
    fbcallbackurl: '/auth/facebook/callback',
    scope: 'email, user_about_me, publish_actions'
  },
  localRegisterEndpoint: 'http://121.97.206.36:2013',
  rackspace: {
      provider: 'rackspace',
      username: 'ihcloudfiles',
      apiKey: '8546c92f5fdcf324aa11d6b55c62d317',
      region: 'SYD'
  },
  SMTP: {
    service: 'Gmail',
    forwarder: 'feedback@itemhound.com',
    user: 'strider@itemhound.com',
    pass: 'a8solutions'
    // forwarder: 'irise@itemhound.com',
    // user: 'irise@itemhound.com',
    // pass: 'ArcusFelicis12!'
  }
};

var prodConfiguration = {
  port: process.env.PORT || 3000,
  //dbConnectionString: 'mongodb://strider_joyent_user:Aegis_Basang_basa_sa_ulan@ds029179-a0.mongolab.com:29179,ds029179-a1.mongolab.com:29179/strider_joyent_prod',
  dbConnectionString: 'mongodb://strider_aws_user:a8solutions@ds049492-a0.mongolab.com:49492,ds049492-a1.mongolab.com:49492/strider_aws_prod',
  localRegisterEndpoint: 'http://www.myrunti.me',
  facebook:{
    client_id: '353126178090062',
    client_secret: '564b4557771bea3db95624ac0c4ccd59',
    fbcallbackurl: 'http://myrunti.me/auth/facebook/callback',
    scope: 'email, user_about_me, publish_actions'
  },
  rackspace: {
    provider: 'rackspace',
    username: 'ihcloudfiles',
    apiKey: '8546c92f5fdcf324aa11d6b55c62d317',
    region: 'SYD'
  },
  SMTP: {
    service: 'Gmail',
    forwarder: 'feedback@itemhound.com',
    //user: 'strider@itemhound.com',
    //pass: 'a8solutions'
    user: 'strider2@itemhound.com',
    pass: 'a8s0lut10ns'
  },  
  assets: {
    common:{
      css: [
        'public/vendor/bootstrap/dist/css/bootstrap.min.css',
        'public/vendor/font-awesome/css/font-awesome.min.css'
      ],
      js: [
        'public/vendor/jquery/jquery.min.js',        
        'public/vendor/angular/angular.min.js',
        'public/vendor/angular-route/angular-route.min.js',
        'public/vendor/bootstrap/dist/js/bootstrap.min.js',
        'public/vendor/underscore/underscore-min.js',
        'public/vendor/moment/min/moment.min.js',
        'public/js/lib/ga.js'
      ] 
    },
    mainlib: {
      css: [
        'public/vendor/semantic/build/packaged/css/semantic.css'
      ],
      js: [
        'public/vendor/semantic/build/packaged/javascript/semantic.min.js',
        'public/vendor/highstock/js/highstock.src.js',
        'public/vendor/highcharts-ng/dist/highcharts-ng.min.js',
        'public/vendor/angular-facebook/lib/angular-facebook.js'
      ]
    },
    adminlib:{
      css: [
        'public/vendor/angular-bootstrap-datetimepicker/src/css/datetimepicker.css'
      ],
      js: [
        'public/vendor/angular-bootstrap-datetimepicker/src/js/datetimepicker.js',
        'public/vendor/FileSaver/FileSaver.min.js'
      ]
    },
    appjs: [
      'public/dist/application.min.js'
    ],
    adminjs: [
      //'public/dist/adminApplication.min.js'
      'public/adminApp/js/**/*.js'
    ]
  }
};

exports.config = function () {
  process.env.NODE_ENV = process.env.NODE_ENV || "production";
  if (process.env.NODE_ENV === "development"){
    return devConfiguration;
  }
  else if (process.env.NODE_ENV === "test"){
    return testConfiguration;
  }
  else if (process.env.NODE_ENV === "production"){
    return prodConfiguration;
  }
};

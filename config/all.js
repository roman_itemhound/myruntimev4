'use strict';

module.exports = {  
  templateEngine: 'swig', 
  assets: {
    common:{
      css: [
        'public/vendor/bootstrap/dist/css/bootstrap.css',
        'public/vendor/font-awesome/css/font-awesome.css',
        'public/vendor/semantic/build/packaged/css/semantic.css'
      ],
      js: [
        'public/vendor/jquery/jquery.js',        
        'public/vendor/angular/angular.js',
        'public/vendor/angular-route/angular-route.js',
        'public/vendor/bootstrap/dist/js/bootstrap.js',
        'public/js/lib/ga.js',        
        'public/vendor/underscore/underscore.js',
        'public/vendor/moment/moment.js'
      ] 
    },
    mainlib: {
      css: [        
      ],
      js: [
        'public/vendor/semantic/build/packaged/javascript/semantic.js',
        'public/vendor/highstock/js/highstock.src.js',
        'public/vendor/highcharts-ng/src/highcharts-ng.js',
        'public/vendor/angular-facebook/lib/angular-facebook.js'       
      ]
    },
    adminlib:{
      css: [
        'public/vendor/angular-bootstrap-datetimepicker/src/css/datetimepicker.css'
      ],
      js: [
        'public/vendor/angular-bootstrap-datetimepicker/src/js/datetimepicker.js',
        'public/vendor/FileSaver/FileSaver.js'
      ]
    },
    css: [
      'public/css/*.css'
    ],
    appjs: [
      'public/mainApp/js/**/*.js'
    ],
    adminjs: [
      'public/adminApp/js/**/*.js'
    ]
  }
};
'use strict';

/**
 * Module dependencies.
 */

var express = require('express');
var path = require('path');
var config = require('./env').config();
var assets = require('./assets');
var MongoStore = require('connect-mongo')(express);
var passport = require('passport');
var consolidate = require('consolidate');
var helmet = require('helmet');

module.exports = function(db, cdnUri){

  //initialize express app
  var app = express();  

  // Setting application local variables
  app.locals.adminVendorJSFiles = assets.getAdminVendorJSAssets();
  app.locals.adminVendorCSSFiles = assets.getAdminVendorCSSAssets();
  app.locals.adminjsFiles = assets.getAdminAppJavascriptAssets();

  app.locals.appjsFiles = assets.getMainAppJavascriptAssets();
  app.locals.appVendorJSFiles = assets.getMainVendorJSAssets();
  app.locals.appVendorCSSFiles = assets.getMainVendorCSSAssets();

  // all environments  
  app.set('port', config.port);
  app.use(express.static(path.resolve('./public')));  

  // Showing stack errors
  app.set('showStackError', true);

  // Set swig as the template engine
  app.engine('html', consolidate['swig']);
  //app.engine('html', swig.renderFile);

  // Set views path and view engine
  app.set('view engine', 'html');
  app.set('views', './app/views');  

  // Environment dependent middleware
  if (process.env.NODE_ENV === 'development') {
    // Disable views cache
    app.use(express.errorHandler());     
    app.use(express.logger('dev'));
    app.set('view cache', false);

  } else if (process.env.NODE_ENV === 'production') {
    app.locals.cache = 'memory';
  }

  app.use(express.favicon());
  app.use(express.json());
  app.use(express.bodyParser());
  app.use(express.urlencoded());
  app.use(express.methodOverride());

  //
  app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Content-Type");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    next();
  });
  //remove me?

  app.use(express.cookieParser());
  app.use(express.session({    
    secret : '_ something to get off my chesst _',
    cookie: { maxAge: 24 * 60 * 60 * 1000 },
    store: new MongoStore({ db: db.connection.db, clear_interval: 3600 })
  }));

  // use passport session
  app.use(passport.initialize());
  app.use(passport.session());

  // Use helmet to secure Express headers  
  //app.use(helmet.xframe('allow-from', 'http://localhost.myrunti.me'));
  app.use(helmet.xframe('allow-from', 'http://dev.myrunti.me'));
  app.use(helmet.xssFilter());
  app.use(helmet.nosniff());
  app.use(helmet.ienoopen());
  app.disable('x-powered-by');

  app.use(app.router);

  //rackspace middleware for intercepting images
  app.use(function(req, res, next){
    if (path.dirname(req.originalUrl).substring(0,7) === '/images'){      
      res.redirect(cdnUri + req.originalUrl);
    }      
    else {
      next();
    }
  });

  //Require routing files
  require('../app/routes/web.js')(app);
  require('../app/routes/api.js')(app);
  
  // Assume 'not found' in the error msgs is a 404. this is somewhat silly, but valid, you can do whatever you like, set properties, use instanceof etc.
  app.use(function(err, req, res, next) {
    // If the error object doesn't exists
    if (!err){
      return next();
    } 
    // Log it
    console.error(err.stack);

    // Error page
    res.status(500).render("error500", {error:err});
  });
  

  // Assume 404 since no middleware responded
  app.use(function(req, res) {
    //res.status(404).render("404");
    res.status(404);
    // respond with html page
    if (req.accepts('html')) {
      res.status(404).render('error');
      return;
    }

    // respond with json
    if (req.accepts('json')) {
      res.status(404).send({ error: 'Not found' });
      return;
    }

    // default to plain-text. send()
    res.type('txt').send('Not found');
  });

  var server = require('http').createServer(app);

  return server;
};

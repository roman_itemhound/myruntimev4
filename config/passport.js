'use strict';

var passport = require('passport'),
  LocalStrategy = require('passport-local').Strategy,
  FacebookStrategy = require('passport-facebook').Strategy,
  User = require('../app/Objects/User'),
  appConfiguration = require('./env').config();

module.exports = function() {  
  //Local Strategy
  passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password' 
  },
  function(login, password, done){
    User.validateUser({"email":login, "password":password},function(error, user){
      if(error.length){
        return done(error);
      }
      else{
        return done(null, user);
      }
    });
  }));

  //facebook
  passport.use(new FacebookStrategy({
    clientID: appConfiguration.facebook.client_id,
    clientSecret: appConfiguration.facebook.client_secret,
    callbackURL: appConfiguration.facebook.fbcallbackurl
    },
    function(accessToken, refreshToken, profile, done){      
      var params = {};
      params.accessToken = accessToken;
      params.fbProfile = profile._json;    
      User.findOrCreateFBUser(params, function(err, user){
        if(err) {
          return done(err);
        }
        else{
          return done(null, user);
        } 
      });      
    }
  ));

  //serialize sessions
  passport.serializeUser(function(userData, done){
    done(null, userData.Model._id);
  });

  passport.deserializeUser(function(userId, done){
    User.fetchUser(userId, function(err, user){    
      done(err, user);
    });
  });
};
var mongoose = require('mongoose');
var config = require('../config/env').config();

var Model = require('../app/models');
var _ = require('lodash');
var async = require('async');

var db = mongoose.connect(config.dbConnectionString);
var dbConnection = mongoose.connection;

var hideRankRaceID = [83, 96, 123, 124, 125, 127, 137, 134, 135, 144, 159, 160, 161, 162];

console.log("Connecting to MongoDB...");

dbConnection.on('error', function(){
  console.error('\x1b[31m', 'Could not connect to MongoDB!');
});

dbConnection.on('connected', function(){
  console.log("Connected to MongoDB sucesfully!");
  populateHideRanks();
});

function populateHideRanks(){
  Model.Race.Race.find({}).setOptions({lean:false}).exec(function(err, races){

    async.eachSeries(races, function(race, done){
      if( _.contains(hideRankRaceID, parseInt(race.raceId,10)) ){
        race.hideRank = true;
        race.save(done);
      }
      else{
        race.hideRank = false;
        race.save(done);
      }
    }, function(err){
      if(err){        
        console.log(err);
      }
      else{
        console.log('done!');
      }
    });
  });
};
var mongoose = require('mongoose');
var config = require('../config/env').config();

var Model = require('../app/models');
var _ = require('lodash');
var async = require('async');

var db = mongoose.connect(config.dbConnectionString);
var dbConnection = mongoose.connection;

var raceArray = [
  {
    "FIELD1":5,
    "FIELD2":"Bonifacio Global City"
  },
  {
    "FIELD1":7,
    "FIELD2":"Quezon City"},
  {
    "FIELD1":9,
    "FIELD2":"Davao City"
  },
  {
    "FIELD1":10,
    "FIELD2":"Bonifacio Global City"
  },
  {
    "FIELD1":11,
    "FIELD2":"Quezon City"
  },
  {
    "FIELD1":12,
    "FIELD2":"Quezon City"
  },
  {
    "FIELD1":13,
    "FIELD2":"Ortigas"
  },
  {
    "FIELD1":14,
    "FIELD2":"Bonifacio Global City"
  },
  {
    "FIELD1":15,
    "FIELD2":"Bonifacio Global City"
  },
  {
    "FIELD1":16,
    "FIELD2":"Bonifacio Global City"
  },
  {
    "FIELD1":18,
    "FIELD2":"Bonifacio Global City"
  },
  {
    "FIELD1":19,
    "FIELD2":"Quezon City"
  },
  {
    "FIELD1":20,
    "FIELD2":"SCTEX"
  },
  {
    "FIELD1":21,
    "FIELD2":"SCTEX"
  },
  {
    "FIELD1":22,
    "FIELD2":"SCTEX"
  },
  {
    "FIELD1":23,
    "FIELD2":"SCTEX"
  },
  {
    "FIELD1":24,
    "FIELD2":"Taguig City"
  },
  {
    "FIELD1":28,
    "FIELD2":"Naga City"
  },
  {
    "FIELD1":17,
    "FIELD2":"Quirino Grandstand, Manila"
  },
  {
    "FIELD1":25,
    "FIELD2":"Manila"
  },
  {
    "FIELD1":26,
    "FIELD2":"San Mateo, Rizal"
  },
  {
    "FIELD1":27,
    "FIELD2":"Ortigas"
  },
  {
    "FIELD1":29,
    "FIELD2":"Pasay City"
  },
  {
    "FIELD1":31,
    "FIELD2":"Taguig City"
  },
  {
    "FIELD1":32,
    "FIELD2":"Naga City"
  },
  {
    "FIELD1":33,
    "FIELD2":"Bonifacio Global City"
  },
  {
    "FIELD1":34,
    "FIELD2":"Quezon City"
  },
  {
    "FIELD1":35,
    "FIELD2":"Bohol"
  },
  {
    "FIELD1":36,
    "FIELD2":"Pasay City"
  },
  {
    "FIELD1":37,
    "FIELD2":"Quirino Grandstand, Manila"
  },
  {
    "FIELD1":38,
    "FIELD2":"Mt. Pinatubo Trail"
  },
  {
    "FIELD1":39,
    "FIELD2":"Taguig City"
  },
  {
    "FIELD1":41,
    "FIELD2":"Sta. Rosa, Laguna"
  },
  {
    "FIELD1":42,
    "FIELD2":"Bonifacio Global City"
  },
  {
    "FIELD1":43,
    "FIELD2":"Sta. Rosa, Laguna"
  },
  {
    "FIELD1":44,
    "FIELD2":"Bonifacio Global City"
  },
  {
    "FIELD1":45,
    "FIELD2":"Taguig City"
  },
  {
    "FIELD1":46,
    "FIELD2":"Sta. Rosa, Laguna"
  },
  {
    "FIELD1":47,
    "FIELD2":"Aseana City, Paranaque"
  },
  {
    "FIELD1":49,
    "FIELD2":"Bonifacio Global City"
  },
  {
    "FIELD1":50,
    "FIELD2":"Aseana City, Paranaque"
  },
  {
    "FIELD1":51,
    "FIELD2":"Quezon City"
  },
  {
    "FIELD1":52,
    "FIELD2":"Davao City"
  },
  {
    "FIELD1":53,
    "FIELD2":"Clark, Pampanga"
  },
  {
    "FIELD1":54,
    "FIELD2":"Sta. Rosa, Laguna"
  },
  {
    "FIELD1":55,
    "FIELD2":"Quirino Grandstand, Manila"
  },
  {
    "FIELD1":56,
    "FIELD2":"Bonifacio Global City"
  },
  {
    "FIELD1":57,
    "FIELD2":"Bonifacio Global City"
  },
  {
    "FIELD1":58,
    "FIELD2":"Sta. Rosa, Laguna"
  },
  {
    "FIELD1":59,
    "FIELD2":"Filinvest, Alabang"
  },
  {
    "FIELD1":60,
    "FIELD2":"Filinvest, Alabang"
  },
  {
    "FIELD1":61,
    "FIELD2":"Filinvest, Alabang"
  },
  {
    "FIELD1":62,
    "FIELD2":"Quezon City"
  },
  {
    "FIELD1":63,
    "FIELD2":"Pasay City"
  },
  {
    "FIELD1":64,
    "FIELD2":"Sta. Rosa, Laguna"
  },
  {
    "FIELD1":65,
    "FIELD2":"Ortigas"
  },
  {
    "FIELD1":66,
    "FIELD2":"San Mateo, Rizal"
  },
  {
    "FIELD1":67,
    "FIELD2":"Bonifacio Global City"
  },
  {
    "FIELD1":68,
    "FIELD2":"Ortigas"
  },
  {
    "FIELD1":69,
    "FIELD2":"Pasay City"
  },
  {
    "FIELD1":71,
    "FIELD2":"Quezon City"
  },
  {
    "FIELD1":72,
    "FIELD2":"Cebu City"
  },
  {
    "FIELD1":73,
    "FIELD2":"Cagayan de Oro"
  },
  {
    "FIELD1":74,
    "FIELD2":"Quezon"
  },
  {
    "FIELD1":75,
    "FIELD2":"Bohol"
  },
  {
    "FIELD1":76,
    "FIELD2":"Quezon City"
  },
  {
    "FIELD1":78,
    "FIELD2":"Cagayan de Oro"
  },
  {
    "FIELD1":81,
    "FIELD2":"Bonifacio Global City"
  },
  {
    "FIELD1":82,
    "FIELD2":"Taguig City"
  },
  {
    "FIELD1":83,
    "FIELD2":"Bonifacio Global City"
  },
  {
    "FIELD1":84,
    "FIELD2":"Aseana City, Paranaque"
  },
  {
    "FIELD1":85,
    "FIELD2":"Clark, Pampanga"
  },
  {
    "FIELD1":86,
    "FIELD2":"Clark, Pampanga"
  },
  {
    "FIELD1":87,
    "FIELD2":"Quezon City"
  },
  {
    "FIELD1":88,
    "FIELD2":"Bonifacio Global City"
  },
  {
    "FIELD1":89,
    "FIELD2":"SCTEX"
  },
  {
    "FIELD1":90,
    "FIELD2":"Quezon City"
  },
  {
    "FIELD1":91,
    "FIELD2":"Bonifacio Global City"
  },
  {
    "FIELD1":92,
    "FIELD2":"Davao City"
  },
  {
    "FIELD1":93,
    "FIELD2":"Quezon City"
  },
  {
    "FIELD1":94,
    "FIELD2":"Bonifacio Global City"
  },
  {
    "FIELD1":95,
    "FIELD2":"Bonifacio Global City"
  },
  {
    "FIELD1":96,
    "FIELD2":"Bonifacio Global City"
  },
  {
    "FIELD1":98,
    "FIELD2":"Quezon City"
  },
  {
    "FIELD1":99,
    "FIELD2":"San Mateo, Rizal"
  },
  {
    "FIELD1":100,
    "FIELD2":"Quirino Grandstand, Manila"
  },
  {
    "FIELD1":101,
    "FIELD2":"Bonifacio Global City"
  },
  {
    "FIELD1":102,
    "FIELD2":"Quezon City"
  },
  {
    "FIELD1":103,
    "FIELD2":"Pasay City"
  },
  {
    "FIELD1":104,
    "FIELD2":"Quezon City"
  },
  {
    "FIELD1":105,
    "FIELD2":"Quezon City"
  },
  {
    "FIELD1":106,
    "FIELD2":"Dagupan City"
  },
  {
    "FIELD1":107,
    "FIELD2":"Tarlac City"
  },
  {
    "FIELD1":108,
    "FIELD2":"Pasay City"
  },
  {
    "FIELD1":109,
    "FIELD2":"Baguio"
  },
  {
    "FIELD1":110,
    "FIELD2":"Quezon City"
  },
  {
    "FIELD1":111,
    "FIELD2":"Laguna"
  },
  {
    "FIELD1":112,
    "FIELD2":"Cagayan de Oro"
  },
  {
    "FIELD1":113,
    "FIELD2":"Quezon City"
  },
  {
    "FIELD1":114,
    "FIELD2":"Pasay City"
  },
  {
    "FIELD1":115,
    "FIELD2":"Taguig City"
  },
  {
    "FIELD1":117,
    "FIELD2":"Tayabas, Quezon"
  },
  {
    "FIELD1":118,
    "FIELD2":"Tayabas, Quezon"
  },
  {
    "FIELD1":119,
    "FIELD2":"Tayabas, Quezon"
  },
  {
    "FIELD1":120,
    "FIELD2":"Tayabas, Quezon"
  },
  {
    "FIELD1":121,
    "FIELD2":"Carmona, Cavite"
  },
  {
    "FIELD1":122,
    "FIELD2":"Bacolod"
  },
  {
    "FIELD1":123,
    "FIELD2":"Clark, Pampanga"
  },
  {
    "FIELD1":124,
    "FIELD2":"Clark, Pampanga"
  },
  {
    "FIELD1":125,
    "FIELD2":"Clark, Pampanga"
  },
  {
    "FIELD1":126,
    "FIELD2":"Cagayan de Oro"
  },
  {
    "FIELD1":127,
    "FIELD2":"Filinvest, Alabang"
  },
  {
    "FIELD1":128,
    "FIELD2":"Bonifacio Global City"
  },
  {
    "FIELD1":129,
    "FIELD2":"Bonifacio Global City"
  },
  {
    "FIELD1":130,
    "FIELD2":"Bohol"
  },
  {
    "FIELD1":131,
    "FIELD2":"General Santos City"
  },
  {
    "FIELD1":133,
    "FIELD2":"Naga City"
  }
];

console.log("Connecting to MongoDB...");

dbConnection.on('error', function(){
  console.error('\x1b[31m', 'Could not connect to MongoDB!');
});

dbConnection.on('connected', function(){
  console.log("Connected to MongoDB sucesfully!");
  populateLocations();
});

function populateLocations(){

  async.eachSeries(raceArray, function(race, done){
    Model.Race.Race.findOne({raceId: race.FIELD1}, function(err, _race){
      if(err){
        console.log("err");
        console.log(err);
        done();
      }
      else if(!_race){
        console.log("Race not found");
        done();
      }
      else{        
        _race.raceAddress = race.FIELD2;
        _race.save(done);
      }
    });
  }, function(err){
    if(err){        
      console.log(err);
    }
    else{
      console.log('done!');
    }
  });
};


var mongoose = require('mongoose');
var config = require('../config/env').config();

var Model = require('../app/models');


var db = mongoose.connect(config.dbConnectionString);
var dbConnection = mongoose.connection;

var locations = ["Bonifacio Global City", "Quezon City", "Davao City", "Ortigas", "SCTEX", "Taguig City", "Naga City", "Quirino Grandstand, Manila", "Manila", "San Mateo, Rizal", "Pasay City", "Bohol", "Mt. Pinatubo Trail", "Sta. Rosa, Laguna", "Aseana City, Paranaque", "Clark, Pampanga", "Filinvest, Alabang", "Cebu City", "Cagayan de Oro", "Quezon", "Dagupan City", "Tarlac City", "Baguio", "Laguna", "Tayabas, Quezon", "Carmona, Cavite", "Bacolod", "General Santos City"];

console.log("Connecting to MongoDB...");

dbConnection.on('error', function(){
  console.error('\x1b[31m', 'Could not connect to MongoDB!');
});

dbConnection.on('connected', function(){
  console.log("Connected to MongoDB sucesfully!");
  populateLocations();
});

function populateLocations(){
  Model.RaceLocation.RaceLocation.findOne({}, function(err, _raceLocation){
    if(err){
      console.log(err);
      process.exit(code=1);
    }
    else{
      if(!_raceLocation){
        var raceLocation = new Model.RaceLocation.RaceLocation();
        raceLocation.locations = locations;
        raceLocation.save(function(err){
          if(err){
            console.log(err);
            process.exit(code=1);
          }
          else{
            console.log('saved!');
            process.exit(code=0);    
          }
        });
      }
      else{
        _raceLocation.locations = locations;
        _raceLocation.save(function(err){
          if(err){
            console.log(err);
            process.exit(code=1);
          }
          else{
            console.log('saved!');
            process.exit(code=0);    
          }
        });        
      }
    }
  });
};

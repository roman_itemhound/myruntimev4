var mongoose = require('mongoose');
var config = require('../config/env').config();

var Model = require('../app/models');
var _ = require('lodash');
var async = require('async');

var db = mongoose.connect(config.dbConnectionString);
var dbConnection = mongoose.connection;

console.log("Connecting to MongoDB...");

dbConnection.on('error', function(){
  console.error('\x1b[31m', 'Could not connect to MongoDB!');
});

dbConnection.on('connected', function(){
  console.log("Connected to MongoDB sucesfully!");
  populateMemberSince();
});

function populateMemberSince(){
  Model.User.User.find({}, function(err, _users){

    async.eachSeries(_users, function(_user, callback){
      var objectID = _user._id.toString();
      console.log(getDateFromObjectID(objectID));
      callback(null);
    }, function(err){
      if(err){
        console.log("Done with error - " + err);
        process.exit(code=1);
      }
      else{
        console.log("Done with no errors!");
        process.exit(code=0);

      }
    });
    
  });
}

function getDateFromObjectID(objectID){
  return new Date(parseInt(objectID.substring(0,8), 16) * 1000);
}


/* mongo snippet! faster! above code causes overflow!

db.usermodels.find({}).forEach(function(user){ var x = (user._id).getTimestamp(); db.usermodels.update(user, {$set: {"dateJoined": x}}); });


*/
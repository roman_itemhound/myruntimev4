/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var config = require('./config/env').config();

//rackspace cdn container
var pkgcloud = require('pkgcloud');
var rackspace = pkgcloud.storage.createClient(config.rackspace);


// Bootstrap db connection
var db = mongoose.connect(config.dbConnectionString);
var dbConnection = mongoose.connection;

console.log("Connecting to MongoDB...");

dbConnection.on('error', function(){
  console.error('\x1b[31m', 'Could not connect to MongoDB!');
});

dbConnection.on('connected', function(){
  console.log("Connected to MongoDB sucesfully!");
  console.log("Setting rackspace cdn...");

  rackspace.getContainer('myruntime-cdn', function(err, container){
    if(err){
      console.log("there was an error retrieving container:\n");
      console.log(err);
      console.error('\x1b[31m', 'Could not connect to Rackspace');
    }
    else{
      container.enableCdn(function(error, cont){
        if(error){
          console.log('there was an error setting container as cdn:\n');
          console.log(error);
          console.error('\x1b[31m', 'Could not set Rackspace container as CDN');
        }
        else{
          console.log("Successfuly set CDN bucket");
            // Init the express application
          var app = require('./config/express')(db, cont.cdnUri);

          //Bootstrap passport config
          require('./config/passport')();

          // Start the app by listening on <port>
          app.listen(config.port, function(){  
            console.log('Running on NODE_ENV: %s', process.env.NODE_ENV);
            console.log('Express server listening on port ' + config.port);
          });

          // Expose app
          exports = module.exports = app;          
          }
      });
    }
  });
});      

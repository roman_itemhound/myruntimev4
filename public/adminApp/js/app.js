app = angular.module('myruntime-admin', ['ngRoute', 'ui.bootstrap.datetimepicker']);

app.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/admin', {
        templateUrl: '/adminApp/partials/index.html',
        controller: 'indexCtrl'
      }).
      when('/admin/race/:raceId/', {
        templateUrl: '/adminApp/partials/race.html',
        controller: 'raceCtrl'
      }).
      when('/admin/races/', {
        templateUrl: '/adminApp/partials/races.html',
        controller: 'racesCtrl'
      }).
      when('/admin/races/new', {
        templateUrl: '/adminApp/partials/newrace.html',
        controller: 'newRaceCtrl'
      }).
      when('/admin/users', {
        templateUrl: '/adminApp/partials/users.html',
        controller: 'usersCtrl'
      }).
      when('/admin/postprocess',{
        templateUrl: '/adminApp/partials/postprocess.html',
        controller: 'postProcessCtrl'
      }).
      otherwise({
        redirectTo: '/admin'
      });
}]);

app.config(function($interpolateProvider){
  $interpolateProvider.startSymbol('[[');
  $interpolateProvider.endSymbol(']]');
});


app.directive('fileUpload', function ($parse) {
  return {
    scope: true,        //create a new scope
    link: function (scope, el, attrs) {
      el.bind('change', function (event) {
        var files = event.target.files;
        //iterate files since 'multiple' may be specified on the element
        for (var i = 0;i<files.length;i++) {
          //emit event upward
          scope.$emit("fileSelected", { file: files[i], model: attrs});
        }                                       
      });
    }
  };
});


app.directive('tableData', function(){
  return {
    restrict: "A",
    scope: {
      display: '=tableData'
    },
    template:'<tr><td class="{{display.Status}}">{{display.EPC}}</td><td class={{display.Status}}>{{display.Start}}</td>' +
    '<td class={{display.Status}} ng-repeat="lap in display.Laps"> {{lap.Time}}</td>'+
    '<td class={{display.Status}}> {{display.Finish}} </td>' +
    '<td class={{display.Status}}><select ng-model="timeDetection" ng-options="detections for detections in display.AllDetections"></select></td>'+
    '<td class={{display.Status}}> <button class="btn btn-primary" ng-click="updatestart(timeDetection)">Set Start</button> &nbsp;&nbsp;' +
    '<button class="btn btn-primary" ng-click="updatefinish(timeDetection)"> Set Finish </button> &nbsp;&nbsp;' +
    '<button class="btn btn-danger" ng-click="clearstart()">Clear Start</button> &nbsp;&nbsp;' +
    '<button class="btn btn-danger" ng-click="clearfinish()"> Clear Finish </button></td></tr>',    

    link: function($scope){
      $scope.updatestart = function(startTime){
        $scope.display.Start = startTime;
        $scope.checkStatus();
      }

      $scope.updatefinish = function(finishTime){
        $scope.display.Finish = finishTime; 
        $scope.checkStatus();
      }

      $scope.clearstart = function(){
        $scope.display.Start = null;
        $scope.checkStatus();
      }

      $scope.clearfinish = function(){
        $scope.display.Finish = null;
        $scope.checkStatus();
      }

      $scope.checkStatus = function(){
        if($scope.display.Finish == null){
          $scope.display.Status = "error";
        }
        else{
          if($scope.display.Start == null){
            $scope.display.Status = "warning";   
          }
          else{
            $scope.display.Status = "success";    
          }
        }
      }
    }
  }
});

var SecondsToTimeString = function(seconds) {
  if (!seconds) return "";
  var hours = parseInt(seconds / 3600) % 24;
  var minutes = parseInt(seconds / 60) % 60;
  var seconds = seconds % 60;
  var TimeStr = (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds);
  return TimeStr;
}

'use strict';

app.controller('newRaceCtrl', function ($scope, $http, $location){
  $scope.newrace = {
    waves: []
  };
  $scope.files = [];

  $http.get('/api/races/locations').success(function(data){
    $scope.locations = data;
    $scope.newrace.raceAddress = data[0];
  });
  
  $scope.$on("fileSelected", function (event, args) {
    $scope.$apply(function () {            
      $scope.files.push(args.file);
    });
  });

  $scope.selectFromList = true;

  $scope.toggleSelectFromList = function(){
    $scope.selectFromList = !$scope.selectFromList;    
  };

  $scope.saveRace = function(){

    //Save location
    $http.post('/api/races/locations', {location: $scope.newrace.raceAddress}).success(function(){
    });

    //Save race
    $http({
      method: 'POST',
      url: "/api/admin/race",
      headers: { 'Content-Type': undefined },
      transformRequest: function (data) {
        var formData = new FormData();
        console.log(angular.toJson(data.files[0]));
        formData.append("race", angular.toJson(data.race));
        formData.append("racePhoto", data.files[0]);
        return formData;
      },
      data: { race: $scope.newrace, files: $scope.files }
    }).
    success(function (data, status, headers, config) {
      console.log(data);
      $location.path('admin/race/'+data.Model.raceId);
    }).
    error(function (data, status, headers, config) {
      alert("failed!");
    });
  }

  $scope.addwave = function(){
    $scope.newrace.waves.push({
      waveHash : Date.now(),
      waveName : "",
      waveDescription : "",
      waveDistance : 0,
      splits : [],
      addSplit : function(){
        this.splits.push({
          waveHash : this.waveHash,
          splitHash : Date.now(),
          splitDistance : 0,
          splitName : '',
          removeSplit : function(){
            for (var i = 0; i < $scope.newrace.waves.length; i++){
              if ($scope.newrace.waves[i].waveHash == this.waveHash){
                for (var j = 0; j < $scope.newrace.waves[i].splits.length; j++){
                  if ($scope.newrace.waves[i].splits[j].splitHash == this.splitHash){
                    $scope.newrace.waves[i].splits.splice(j,1);
                    break;
                  }
                }
                break;
              }
            }
          }
        });
      },
      removeWave : function(){
        for (var i = 0; i < $scope.newrace.waves.length; i++){
          if ($scope.newrace.waves[i].waveHash == this.waveHash){
            $scope.newrace.waves.splice(i,1);
            break;
          }
        }
      }
    });
  }
});

'use strict';

app.controller('usersCtrl', function ($scope, $http){
  
  $scope.data = {
    inputEmail: "",
    inputPassword: "",
    validatePassword: ""
  };

  $scope.validateAndSubmit = function(data){    
    if(data.inputEmail == null || data.inputEmail == '' || data.inputEmail == undefined){
      alert("Enter valid Email");
    }
    else{
      if(data.inputPassword == '' || data.validatePassword == '' || data.validatePassword == undefined || data.inputPassword == undefined || data.inputPassword != data.validatePassword){
        alert("Fix password!");
      }
      else{
        $http({
          method: 'POST',
          url: '/api/admin/createNewAdminUser',
          data:{ username: data.inputEmail, password: data.inputPassword, firstName: data.firstName, lastName: data.lastName }
        }).success(function (data, status, headers, config){
          alert("Created New Admin!");
          $route.reload();
        }).error(function (data, status, headers, config){
          alert("Error occurred!");
        });     
      }      
    }
  };

});
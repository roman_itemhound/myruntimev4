'use strict';

app.controller('appctrl', ['$rootScope','$route','$location', '$http', '$window', function($rootScope,$route,$location,$http, $window){  
  $http.get('/api/user/').success(function(data){  
    $rootScope.User = data;
  });

  $rootScope.$on('$viewContentLoaded', function(event) {
    $window._gaq.push(['_trackPageview', $location.path()]);
  });
}]);

'use strict';

app.controller('racesCtrl', function ($scope, $http, $rootScope){
  $http.get('/api/admin/race').success(function(data){
    $scope.races = data.map(function(race){
      race.Model.raceDate = (new Date(race.Model.raceDate)).toDateString();      
      return race;
    });
  });

  $scope.createRunnerText = function(){
    $http.post('/api/admin/rebuildWaveOrder/',{}).success(function(data){
      alert("Created Runner Text!");
    });
  };

});
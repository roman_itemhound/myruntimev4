app.controller('postProcessCtrl', function($scope,$route,$location,$http){    
  
  $scope.files = [];
  $scope.waveName = "Enter Wave Name";

  $scope.initializeProcessing = function(){
    $scope.EPCs = [];
    $scope.AllDetections = [];
    $scope.Detections = [];  
    $scope.Start = [];
    $scope.Finish = [];
    $scope.Status = [];
    $scope.Laps = [];
    $scope.forDisplay = [];    
  }

  $scope.initializeProcessing();
  

  $scope.postprocessdata = {
    waveLaps: 1,
    waveStart: "06:00:00 AM"
  };  

  $scope.entryList = [];
  $scope.showProcessing = false;
  $scope.waveDetailsReady = false;
  $scope.entryListReady = false;
  $scope.disableUpload = true;
  $scope.disableProcess = true;
  $scope.hasLaps = false;

  var _laps = [];

  $scope.downloadResults = function(){    
    var rawCSV = "Bib,LastName,FirstName,MiddleName,Gender,Nationality,StartTime,FinishTime,Comment\r\n";

    /*
    for(var i = 0; i<$scope.EPCs.length; i++){
      if($scope.hasLaps){
        var lapsString = "";
        for(var j = 0; j < _laps.length; j++){
          if($scope.forDisplay[i].Laps[j]){
            lapsString += "," + $scope.forDisplay[i].Laps[j].Time;
          } 
          else{
            lapsString += ",";
          }          
        }          
        rawCSV+= $scope.forDisplay[i].EPC + "," + $scope.forDisplay[i].Start  + "," + $scope.forDisplay[i].Finish + lapsString + "," + $scope.forDisplay[i].Status+ "\r\n"
      }
      else{
        rawCSV+= $scope.forDisplay[i].EPC + "," + $scope.forDisplay[i].Start  + "," + $scope.forDisplay[i].Finish + "," + $scope.forDisplay[i].Status + "\r\n"
      }        
    }
    */

    for(var i = 0; i<$scope.forDisplay.length; i++){

      var match = _.findWhere($scope.entryList, {epc:$scope.forDisplay[i].EPC});
      if(match){
        rawCSV+= match.bib + "," + match.lastName + "," + match.firstName + ",," + match.gender + ",," + $scope.forDisplay[i].Start  + "," + $scope.forDisplay[i].Finish + "," + $scope.forDisplay[i].Status + "\r\n" 
      }

    }    
    
    var blob = new Blob([rawCSV], {type: "text/plain;charset=utf-8"});
    saveAs(blob, $scope.waveName + ".csv");
    
    alert("Successfully downloaded results!");    
  }

  $scope.$on("fileSelected", function (event, args) {
    $scope.$apply(function () {            

      var file = args.file;      
      if (args.model.fileType == "detections"){
        $scope.files.detections = file;
      }
      if (args.model.fileType == "entrylist"){
        $scope.files.entrylist = file;        
      }  
    });      
  });

  $scope.setWaveStart = function(){
    if($scope.postprocessdata.waveLaps <= 1){
      $scope.hasLaps = false;
    }
    else{
      $scope.hasLaps = true;
      _laps = new Array(parseInt($scope.postprocessdata.waveLaps - 1));
    }    

    if(!$scope.postprocessdata.date){
      alert('Invalid Start Date!');
      $scope.waveDetailsReady = false;
      return;
    }
    else{
      if(!$scope.postprocessdata.date.toDateString()){
        alert('Invalid Start Date!');
        $scope.waveDetailsReady = false;
        return;
      } 
    }

    if(!$scope.postprocessdata.waveStart){
      alert('Invalid Start Time!');
      $scope.waveDetailsReady = false;
      return;
    } 

    $scope.waveStart = new Date($scope.postprocessdata.date.toDateString() + " " + $scope.postprocessdata.waveStart);


    if($scope.waveStart == 'Invalid Date'){
      alert('Invalid Start Date!');
      $scope.waveDetailsReady = false;
    }

    else{
      alert('Success!');      
      $scope.waveDetailsReady = true;
      $scope.disableUpload = !($scope.waveDetailsReady && $scope.entryListReady);
    }    
  }

  $scope.processDetections = function(){
    $scope.waveStart = new Date($scope.postprocessdata.date.toDateString() + " " + $scope.postprocessdata.waveStart);
    $scope.Laps = $scope.Laps.map(function(lap, index){
      var lapEntries = [];
      for(var i = 0; i < _laps.length; i++){
        var LapEntry = new Object();
        LapEntry.Lap = i;
        LapEntry.Time = null;        
        lapEntries.push(LapEntry);
      }
      return lapEntries;
    })

    var _processData = function(something, item, i, callback){
      $scope.Detections[i] = $scope.Detections[i].filter(function(detection){
        var detectionDate = new Date($scope.postprocessdata.date.toDateString() + " " + detection);
        return( detectionDate >= $scope.waveStart)
      })   

      var sorted = $scope.Detections[i].sort(function(a,b){
        return (new Date($scope.postprocessdata.date.toDateString() + " " + a) - new Date($scope.postprocessdata.date.toDateString() + " " + b));
      })

      var lastDetectionTime = null;
      var lastDetectionDate = null;
      var lapsLength = 0;
      var hasStart = false;
      var firstLap = true;

      sorted.forEach(function(detection){

        //with laps
        if($scope.hasLaps){
          if(!$scope.Finish[i]){          
            if(!$scope.Start[i]){
              $scope.Start[i] = detection;
            }
            else{
              var detectionDate = new Date($scope.postprocessdata.date.toDateString() + " " + detection);
              var startDate = new Date($scope.postprocessdata.date.toDateString() + " " + $scope.Start[i]);
              
              if( ((detectionDate.getTime() - startDate.getTime())/1000) < 420){
                $scope.Start[i] = detection;
              }
              else{
                if(firstLap){
                  $scope.Laps[i] = $scope.Laps[i].map(function(lap){
                    if(lap.Lap == 0){
                      lap.Time = detection;
                    }
                    return lap;
                  })
                  lastDetectionTime = detection;
                  lastDetectionDate = new Date($scope.postprocessdata.date.toDateString() + " " + lastDetectionTime);
                  firstLap = false;
                  lapsLength = 0;
                }
                else{
                  //second ++ instance after start is found
                  if( ((detectionDate.getTime() - lastDetectionDate.getTime())/1000) < 420){

                    //refresh lap time;
                    $scope.Laps[i] = $scope.Laps[i].map(function(lap){
                      if(lap.Lap == lapsLength){
                        lap.Time = detection;
                      }
                      return lap;
                    })
                    lastDetectionTime = detection;
                    lastDetectionDate = new Date($scope.postprocessdata.date.toDateString() + " " + lastDetectionTime);
                  }

                  else{
                    lapsLength++;
                    if(lapsLength == _laps.length){
                      console.log(((detectionDate.getTime() - lastDetectionDate.getTime())/1000));
                      $scope.Finish[i] = detection;
                      if( ( (startDate.getTime() - $scope.waveStart.getTime()) / 1000) > 600 ){
                        $scope.Status[i] = 'warning';
                      }
                      else{
                        $scope.Status[i] = 'success';  
                      }
                    }
                    else{                      
                      $scope.Laps[i] = $scope.Laps[i].map(function(lap){
                        if(lap.Lap == lapsLength){
                          lap.Time = detection;
                        }
                        return lap;
                      })
                      lastDetectionTime = detection;
                      lastDetectionDate = new Date($scope.postprocessdata.date.toDateString() + " " + detection);
                    }
                  }
                }                
              }
            }
          }
          else{
            return;
          }
        }          

        //no laps
        else{
          if(!$scope.Finish[i]){          
            if(!$scope.Start[i]){
              $scope.Start[i] = detection;
            }
            else{
              var detectionDate = new Date($scope.postprocessdata.date.toDateString() + " " + detection);
              var startDate = new Date($scope.postprocessdata.date.toDateString() + " " + $scope.Start[i]);
              
              
              //8 minutes?
              if( ((detectionDate.getTime() - startDate.getTime())/1000) < 420){
                $scope.Start[i] = detection;
              }
              else{
                $scope.Finish[i] = detection;
                if( ( (startDate.getTime() - $scope.waveStart.getTime()) / 1000) > 600 ){
                  $scope.Status[i] = 'warning';
                }
                else{
                  $scope.Status[i] = 'success';  
                }
                
              }
            }            
          }
          else{
            return;
          }          
        }   
      })    
      callback(null);
    }

    function doSynchronousLoop(initialData, data, processData, done) {
      if (data.length > 0) {
        var loop = function(initialData, data, i, processData, done) {
          processData(initialData, data[i], i, function(processResult) {
            if (++i < data.length) {
              loop(processResult, data, i, processData, done);
            }
            else {
              done(processResult);
            }
          });
        };
        loop(initialData, data, 0, processData, done);
      } 
      else {
        done(initialData);
      }
    }

    var dummyArray = new Array($scope.EPCs.length);

    doSynchronousLoop([], dummyArray, _processData, function(result){
      $scope.forDisplay = [];
      $scope.showProcessing = true;
      for(var i = 0 ; i <$scope.EPCs.length; i++){
        var displayItem = new Object({
          EPC: $scope.EPCs[i],
          Detections: $scope.Detections[i],
          Start: $scope.Start[i],
          Finish: $scope.Finish[i],
          AllDetections: _.uniq($scope.AllDetections[i]),
          Status : $scope.Status[i],
          Laps: $scope.Laps[i]
        })       
        $scope.forDisplay.push(displayItem);
      }      
    });
  }

  $scope.uploadEntryList = function(){    
    if (!$scope.files.entrylist){
      alert('No file selected');
      return;
    }
    
    $scope.entryList = [];
    var file = $scope.files.entrylist;

    $http({
      method: 'POST',
      url: "/api/admin/uploadEntryListForPostProcess",
      headers: { 'Content-Type': undefined},
      transformRequest: function (data) {
        var formData = new FormData();
        formData.append("entrylist", data.files);
        return formData;
      },
      data: { files: file }
    }).
    success(function (data, status, headers, config){
      $scope.entryList = data;
      $scope.entryListReady = true;
      $scope.disableUpload = !($scope.entryListReady && $scope.waveDetailsReady);
      alert("uploaded successfully!");
    }).
    error(function (data, status, headers, config) {
      alert("Failed! Check data.");
      $scope.entryListReady = false;
      $scope.disableUpload = true;
    });
  }

  $scope.uploadDetections = function(){
    if (!$scope.files.detections){
      alert('No file selected');
      return;
    }
    var file = $scope.files.detections;
    $scope.initializeProcessing();

    $http({
        method: 'POST',
        url: "/api/admin/uploadDetectionsForPostProcess",
        headers: { 'Content-Type': undefined },
        transformRequest: function (data) {
            var formData = new FormData();
            formData.append("detections", data.files);
            return formData;
        },
        data: { files: file }
    }).
    success(function (data, status, headers, config) {      
      data.forEach(function(entry){
        if($scope.EPCs.indexOf(entry.EPC) == -1){
          $scope.EPCs.push(entry.EPC);
          
          var detection = new Array();
          var start = null;
          var finish = null;
          var laps = new Array();

          detection.push((new Date((entry.Detection-28800)*1000)).toLocaleTimeString());          
          $scope.Detections.push(detection); 
          $scope.Start.push(start);
          $scope.Finish.push(finish);
          $scope.Laps.push(laps);          
          $scope.AllDetections.push(detection);          
          $scope.Status.push('error');
        }
        else{
          var index = $scope.EPCs.indexOf(entry.EPC);    
          $scope.Detections[index].push((new Date((entry.Detection-28800)*1000)).toLocaleTimeString());          
          $scope.AllDetections[index].push((new Date((entry.Detection-28800)*1000)).toLocaleTimeString());          
        }
      });
      $scope.forDisplay = [];
      $scope.disableProcess = false;      
      alert("Success!");
      $scope.processDetections();            
    }).
    error(function (data, status, headers, config) {
        alert("Failed! Check data.");
    });
  }
});

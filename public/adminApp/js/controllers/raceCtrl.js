'use strict';

var SecondsToTimeString = function(seconds) {
  if (!seconds) return "";
  var hours = parseInt(seconds / 3600) % 24;
  var minutes = parseInt(seconds / 60) % 60;
  var seconds = seconds % 60;
  var TimeStr = (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds);
  return TimeStr;
};

app.controller('raceCtrl', function ($scope, $http, $rootScope, $routeParams, $location, $route){
  
  var raceId = $routeParams.raceId

  $scope.editMode = false;
  $scope.editRaceDateMode = false;
  $scope.selectFromList = true;

  $scope.SecondsToTimeString = SecondsToTimeString;

  $http.get('/api/admin/race/'+ raceId + '/bibCount').success(function(data){
    $scope.uploadedBibs = data.Count;
  });

  $http.get('/api/admin/race/'+ raceId + '/activatedBibCount').success(function(data){
    $scope.activatedBibs = data.Count; 
  });


  $http.get('/api/races/locations').success(function(data){
    $scope.locations = data;
  });

  $http.get('/api/admin/race/'+raceId).success(function(data){

    (data.Model.upcoming) ?  $scope.upcomingStatus = "Hide Race" : $scope.upcomingStatus = "Show Race";
    (data.Model.published) ? $scope.publishedStatus = "Hide Results" : $scope.publishedStatus = "Show Results";
    $scope.hideRankStatus = (data.Model.hideRank) ? "Show Rank" : "Hide Rank";
    $scope.isThirdPartyRace = data.Model.isThirdPartyRace;
    
    data.Model.raceDate = (new Date(data.Model.raceDate)).toDateString();
    data.Model.raceDate_val = new Date(data.Model.raceDate);     
    data.totalRunners = 0;


    data.Model.waves = data.Model.waves.map(function(wave){
      var waveDate = new Date(wave.waveStart);
      wave.waveStartHolder = waveDate.toLocaleTimeString();
    
      if (wave.runnersText){
        var runners = JSON.parse(wave.runnersText);
        wave.totalRunners = runners.length;
        wave.runners = runners;
        data.totalRunners+=runners.length;
      }

      wave.addSplit = function(){        
        this.splits.push({           
          splitHash: Date.now(),
          splitDistance: 0,
          splitName : '',
          removeSplit : function(){           
            for (var i = 0; i < $scope.race.Model.waves.length; i++){
              if ($scope.race.Model.waves[i].waveId == wave.waveId){
                for (var j = 0; j < $scope.race.Model.waves[i].splits.length; j++){
                  //console.log($scope.race.Model.waves[i].splits[j]);
                  if ($scope.race.Model.waves[i].splits[j].splitHash == this.splitHash){
                    $scope.race.Model.waves[i].splits.splice(j,1);
                    break;
                  }
                }
                break;
              }
            }
          }
        });
      }

      wave.splits = wave.splits.map(function(split){
        split.removeSplit = function(){          
          for (var i = 0; i < $scope.race.Model.waves.length; i++){
            if ($scope.race.Model.waves[i].waveId == wave.waveId){
              for (var j = 0; j < $scope.race.Model.waves[i].splits.length; j++){
                if ($scope.race.Model.waves[i].splits[j].splitOrder == this.splitOrder){
                  $scope.race.Model.waves[i].splits.splice(j,1);
                  break;
                }
              }
              break;
            }
          }
        }
        return split;
      });


      wave.removeWave = function(){
        for (var i = 0; i < $scope.race.Model.waves.length; i++){
          if ($scope.race.Model.waves[i].waveId == this.waveId){
            $scope.race.Model.waves.splice(i,1);
            break;
          }
        }
      };

      wave.setStart = function(){
        var waveId = this.waveId;
        var start = this.waveStart;

        var tDate = new Date($scope.race.Model.raceDate + " " + this.waveStartHolder)
        if (tDate == "Invalid Date"){
          alert("Invalid date");
          return;
        }
        var raceDate = new Date($scope.race.Model.raceDate);
        var a = new Date($scope.race.Model.raceDate + " " + this.waveStartHolder);        
        this.waveStart = a;        
        $http.post('/api/admin/race/'+raceId+'/'+this.waveId+'/start', {
          waveStart: this.waveStart
        }).success(function(){
          alert('Successfully uploaded time')
          $route.reload();
        }).error(function(){
          alert('Failed to upload time');
        });
      };
    
      return wave;    
    });

    $scope.race = data;

    $scope.addwave = function(){
      $scope.race.Model.waves.push({
        waveHash : Date.now(),
        waveName : "",
        waveDescription : "",
        waveDistance : 0,
        splits       : [],
        addSplit     : function(){
          this.splits.push({
            waveHash  : this.waveHash,
            splitHash     : Date.now(),
            splitDistance : 0,
            splitName     : '',
            removeSplit : function(){
              for (var i = 0; i < $scope.race.Model.waves.length; i++){
                if ($scope.race.Model.waves[i].waveHash == this.waveHash){
                  for (var j = 0; j < $scope.race.Model.waves[i].splits.length; j++){
                    if ($scope.race.Model.waves[i].splits[j].splitHash == this.splitHash){
                      $scope.race.Model.waves[i].splits.splice(j,1);
                      break;
                    }
                  }
                  break;
                }
              }
            }
          });
        },
        removeWave : function(){
          for (var i = 0; i < $scope.race.Model.waves.length; i++){
            if ($scope.race.Model.waves[i].waveHash == this.waveHash){
              $scope.race.Model.waves.splice(i,1);
              break;
            }
          }
        }
      });
    };

    $scope.toggleWaveDetails = function(waveId){
      $('#waveDetails' + waveId).toggle();
    };
  });


  $scope.uploadRacePhoto = function(raceName){
    
    if(!$scope.files.racePhoto){
      alert("No file selected");
      return;
    }

    var file = $scope.files.racePhoto;
    
    $http({
      method: 'POST',
      url: '/api/admin/uploadRacePhoto/' + raceId,
      headers: { 'Content-Type': undefined },
      transformRequest: function (data){
        var formData = new FormData();
        formData.append("race", angular.toJson(data.race));
        formData.append("racePhoto", data.files);
        return formData;
      },
      data: {race :{"raceName": raceName}, files: file}
    }).success(function (data, status, headers, config) {        
      alert("Uploaded Photo");
      $route.reload();
    }).
    error(function (data, status, headers, config) {
        alert("Upload failed!");
    });
  };

  $scope.uploadResults = function(waveId){
    //Check if file exists
    var found = false;
    var index = -1;
    for (var i = 0; i < $scope.files.results.length; i++){
      if ($scope.files.results[i].waveId == waveId){
        found = true;
        index = i;
        break;
      }
    }
    if (!found){
      alert('No file selected');
      return;
    }

    var file = $scope.files.results[index].file;

    $http({
      method: 'POST',
      url: "/api/admin/race/"+raceId+"/runners",
      headers: { 'Content-Type': undefined },
      transformRequest: function (data) {
          var formData = new FormData();
          formData.append("wave", angular.toJson(data.race));
          formData.append("runners", data.files);
          return formData;
      },
      data: { race: {"waveId":waveId}, files: file }
    }).
    success(function (data, status, headers, config) {  
        alert("Successfully uploaded results");
        $route.reload();
    }).
    error(function (data, status, headers, config) {
        alert("failed!");
    });
  };

  $scope.deleteRace = function(){
    var confirmDelete = confirm('Are you absolutely sure you want to delete race?');
    if(confirmDelete){
      $http.delete('/api/admin/race/'+raceId).success(function(){
        $location.path('races');  
      });      
    }    
  };

  $scope.saveAllDetails = function(raceDetails){
    $http({
      method: 'POST',
      url: "/api/admin/updateRaceDetails",
      data: {race: raceDetails}
    }).success(function(data, status, headers, config){
      alert("Successfully saved race details!")
      $route.reload();
    }).error(function (data, status, headers, config){
      alert('Failed to save wave details.');
    });
  };

  $scope.saveWaveDetails = function(waveDetails, raceId){
    var detailsToUpdate = {
      raceId: raceId,
      waveDetails: waveDetails
    }
    
    $http({
      method: 'POST',
      url: "/api/admin/updateWaveDetails",
      data: detailsToUpdate
    }).success(function(data, status, headers, config){
      alert("Successfully saved wave details!")
      $route.reload();
    }).error(function (data, status, headers, config){
      alert('Failed to save wave details.');
    });
  };

  $scope.files = {
    bibs : null,
    results : []
  };

  $scope.$on("fileSelected", function (event, args) {    
    $scope.$apply(function () {            
      var file = args.file;
      if (args.model.fileType == "bibs"){
        $scope.files.bibs = file;
      }
      if (args.model.fileType == "racePhoto"){
        $scope.files.racePhoto = file;
      }
      if (args.model.fileType == "resultFile"){
        for (var i = 0; i < $scope.files.results.length; i++){
          if ($scope.files.results[i].waveId == args.model.waveAttr){
            $scope.files.results.splice(i,1);
            break;
          }
        }
        $scope.files.results.push({
          waveId : args.model.waveAttr,
          file   : file
        });
      }
    });
  });
  
  $scope.uploadBibs = function(){
    if (!$scope.files.bibs){
      alert('No file selected');
      return;
    }
    var file = $scope.files.bibs;
    $http({
        method: 'POST',
        url: "/api/admin/race/"+raceId+"/bibs",
        headers: { 'Content-Type': undefined },
        transformRequest: function (data) {
            var formData = new FormData();
            //console.log(angular.toJson(data.files[0]));
            formData.append("race", angular.toJson(data.race));
            formData.append("bibs", data.files);
            return formData;
        },
        data: { race: {}, files: file }
    }).
    success(function (data, status, headers, config) {
        //console.log(data);
        alert("Successfully uploaded bibs");
    }).
    error(function (data, status, headers, config) {
        alert("failed!");
    });
  };

  $scope.downloadCodes = function(){
    $http({
        method: 'POST',
        url: "/api/admin/downloadCodes/"+raceId,
    }).
    success(function (data, status, headers, config) {
        var blob = new Blob([data], {type: "text/plain;charset=utf-8"});
        saveAs(blob, "Codes.csv");
        alert("Successfully downloaded activation codes!");
    }).
    error(function (data, status, headers, config) {
        alert("failed!");
    });
  };
  
  $scope.togglePublished = function(){
    $http.post('/api/admin/race/'+raceId+'/published', {}).success(function(data){      
      (data.published) ? $scope.publishedStatus = "Hide Results" : $scope.publishedStatus = "Show Results";    
      alert('Successfully changed published status')
    }).error(function(){
      alert('Failed to change published status');
    });
  };

  $scope.toggleUpcoming = function(){    
    $http.post('/api/admin/race/' + raceId + '/upcoming', {}).success(function(data){
      (data.upcoming) ?  $scope.upcomingStatus = "Hide Race" : $scope.upcomingStatus = "Show Race";
      alert('Successfully changed upcoming status');
    }).error(function(){
      alert('Failed to change upcoming status');
    });
  };

  $scope.toggleHideRank = function(){
    $http.post('/api/admin/race/' + raceId + '/hideRank', {}).success(function(data){
      $scope.hideRankStatus = (data.hideRank) ? "Show Rank" : "Hide Rank";      
      alert('Successfully changed hide rank status');
    }).error(function(){
      alert('Failed to change hide rank status');
    });
  };

  $scope.updateRaceDate = function(newRaceDate){
    $http.post('/api/admin/race/' + raceId + '/updateRaceDate', {"raceDate": newRaceDate}).success(function(){
      alert('Successfully updated race date');
      $route.reload();
    }).error(function(){
      alert('Failed to change race date');
    });
  };

  $scope.updateRaceAddress = function(newAddress){
    if(newAddress === '' || newAddress === null){
      alert('Please specify race location');
    }
    else{

      //New Location Option
      if(!$scope.selectFromList){

        //add new location to db first.
        $http.post('/api/races/locations', {location: newAddress}).success(function(data){
          //then save to race
          $http.post('/api/admin/race/' + raceId + '/updateRaceAddress', {"raceAddress": newAddress}).success(function(){
            alert('Successfully updated race location');
            $route.reload();
          }).error(function(){
            alert('Failed to change race location');
          });
        }).error(function(){
          alert('Failed to change race location');
        });
      }

      //Location from dropdown
      else{
        $http.post('/api/admin/race/' + raceId + '/updateRaceAddress', {"raceAddress": newAddress}).success(function(data){
          alert('Successfully updated race location');
          $route.reload();
        }).error(function(){
          alert('Failed to change race location');
        });
      }
    }      
  };

  $scope.setEditMode = function(){
    $scope.newAddress = $scope.race.Model.raceAddress;
    $scope.editMode = true;
  };

  $scope.resetEditMode = function(){
    $scope.selectFromList = true;
    $scope.editMode = false;
  };

  $scope.setEditRaceDateMode = function(){
    $scope.newDate = $scope.race.Model.raceDate_val;
    $scope.editRaceDateMode = true;
  };

  $scope.resetEditRaceDateMode = function(){
    $scope.editRaceDateMode = false;
  }

  $scope.hideSelectList = function(){
    $scope.newAddress = '';
    $scope.selectFromList = false;
  }

  $scope.updateWaveUrl = function(raceId, waveDetails){
    if(waveDetails.thirdPartyWaveUrl === null || waveDetails.thirdPartyWaveUrl === ""){
      alert("Please indicate the url of the wave result!");
    }
    else{
      var detailsToUpdate = {
        raceId: raceId,
        waveDetails: waveDetails
      }     
      $http({
        method: 'POST',
        url: "/api/admin/updateWaveDetails",
        data: detailsToUpdate
      }).success(function(data, status, headers, config){
        alert("Successfully updated wave url!")
        $route.reload();
      }).error(function (data, status, headers, config){
        alert('Failed to update wave url.');
      });
    }
  }

});

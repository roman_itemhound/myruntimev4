app = angular.module('myruntime', ['ngRoute', 'highcharts-ng', 'facebook']);

app.config(['FacebookProvider', function(FacebookProvider){
    var myAppId = "353126178090062";
    FacebookProvider.init(myAppId);
  }
]);

app.config(function($interpolateProvider){
  $interpolateProvider.startSymbol('[[');
  $interpolateProvider.endSymbol(']]');
});

app.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/', {
        templateUrl: '/mainApp/partials/index.html',
        controller: 'indexCtrl'
      }).
      when('/runner/:raceId/:bibnumber', {
        templateUrl: '/mainApp/partials/runner.html',
        controller: 'runnerCtrl'
      }).
      when('/races', {
        templateUrl: '/mainApp/partials/races.html',
        controller: 'racesCtrl'
      }).
      when('/connect',{
        templateUrl: '/mainApp/partials/connect.html',
        controller: 'connectCtrl'
      }).
      when('/race/:raceId/:waveId', {
        templateUrl: '/mainApp/partials/wave.html',
        controller: 'waveCtrl'
      }).
      when('/race/:raceNick', {
        templateUrl: '/mainApp/partials/race.html',
        controller: 'raceCtrl'
      }).
      when('/contact', {
        templateUrl: '/mainApp/partials/contact.html',
        controller: 'contactCtrl'
      }).   
      when('/faqs', {
        templateUrl: '/mainApp/partials/faqs.html'
      }).
      when('/profile/:userId', {
        templateUrl: '/mainApp/partials/profile.html',
        controller: 'profileCtrl'
      }).
      when('/me',{
        templateUrl: '/mainApp/partials/profile.html',
        controller: 'profileCtrl'
      }).
      when('/accountSettings',{
        templateUrl: '/mainApp/partials/accountSettings.html',
        controller: 'accountSettingsCtrl'
      }).
      when('/login',{
        templateUrl: '/mainApp/partials/login.html',
        controller: 'loginCtrl'
      }).
      when('/signup',{
        templateUrl: 'mainApp/partials/register.html',
        controller: 'registerCtrl'
      }).
      when('/adminlogin',{
        templateUrl: '/mainApp/partials/adminlogin.html',
        controller: 'adminLoginCtrl'
      }).
      when('/invalidAccountActivation', {
        templateUrl: '/mainApp/partials/accountActivationInvalid.html',
      }).
      when('/successAccountActivation', {
        templateUrl: '/mainApp/partials/accountActivationSuccess.html',
      }).
      when('/pageNotFound', {
        templateUrl: '/mainApp/partials/pageNotFound.html',
        controller: 'pageNotFoundCtrl'
      }).
      otherwise({
        redirectTo: '/'
      });
}]);

app.run(function($rootScope){
  $rootScope.hideRankRaceIds = [83, 96, 123, 124, 125, 127, 137, 134, 135, 144, 159, 160, 161, 162, 180];
  $rootScope.hideBibRaceIds = [125];
  $rootScope.hideOfficialTimeRaceIds = [123, 125, 137, 134, 135, 149];
  $rootScope.hideChipTimeRaceIds = [134, 135];
  $rootScope.hideFriendsRaceIds = [134, 135];
  $rootScope.sortByChipTimeRaceIds = [149];
  $rootScope.hideNamesIds = [160];
});
   
app.filter('toProper', function(){
  return function(value){
    if (!value) return '';
    else{
      var properCase = function(val){
        return (val.length >= 1)?(val[0].toUpperCase()+ val.substr(1).toLowerCase()):('');
      }
      return (value.length>0)?(value.split(' ').filter(function(a){return (a.length >=1)}).map(properCase).join(' ')):('');
    }
  }      
});

app.filter('secondsToTimeString', function(){
  return function(seconds){
    var hours = parseInt(seconds / 3600) % 24;
    var minutes = parseInt(seconds / 60) % 60;
    var seconds = seconds % 60;
    var TimeStr = (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds);
    return TimeStr;
  }
});

app.filter('toGenderString', function(){
  return function(gender){
    return gender == 'F' ? 'Female' : 'Male';
  }
});

app.filter('offset12Hours', function(){
  return function(time){
    if(time){
      if(time.toString().indexOf('AM') != -1){
        return time.toString().replace('AM', 'PM');
      }
      else{
        return time.toString().replace('PM', 'AM');
      }
    }
    else{
      return;
    } 
  }
});

app.filter('changeTimeFormatToHHMMSS', function(){
  return function(time){
    if(time){

      //Remove AM part
      var AMindex = time.indexOf(" AM");
      var removedAM = time.substring(0, AMindex);
      var returnTime;

      if(removedAM.indexOf("12:") === 0){
        returnTime = removedAM.replace("12:", "00:");
      }
      else{
        returnTime = removedAM;
      }

      return returnTime;
      
    }
    else{
      return ;
    }
  }
});

app.filter('changeTimeFormatFromISODateToString', function(){
  return function(time){
    if(time){
      return moment(time).format('MMMM D, YYYY');
    }
    else{
      return ;
    }
  }
});

app.filter('genderToImage', function(){
  return function(gender){
    return gender == 'F' ? '/images/female.png' : '/images/male.png';
  }
});

app.directive('myfooter', function(){
  return {
    restrict: "E",
    templateUrl: '/mainApp/partials/footerTemplate.html'
  }
});

app.directive("hover", function(){
  return function(scope, element, attrs){
    element.bind("mouseenter", function(){
      scope.$apply(attrs.hover);
    })  
  }
});

app.directive("leave", function(){
  return function(scope, element, attrs){
    element.bind("mouseleave", function(){
      scope.$apply(attrs.leave);
    });
  }
});

app.directive('ngBlur', function () {
  return function (scope, elem, attrs) {
    elem.bind('blur', function () {
      scope.$apply(attrs.ngBlur);
    });
  };
});

/*
.filter('object2Array', function() {
  return function(input) {
    var out = []; 
    for(var i in input){
      out.push(input[i]);
    }
    return out;
  };
})
*/

app.directive('pwCheck', function(){
  return {
    require: 'ngModel',
    link: function(scope, elem, attrs, ctrl){
      var firstPassword = '#' + attrs.pwCheck;
      elem.append(firstPassword).on('keyup', function(){
        scope.$apply(function(){
          var v = elem.val() === $(firstPassword).val();
          ctrl.$setValidity('pwmatch', v);
        });
      });      
    }
  };
});

'use strict';

app.controller('accountSettingsCtrl', function ($scope, $http, $location){
  
  $http.get('/api/user/').success(function(data){ 
    if(!data){
      $location.path('login');
    }    
    else{
      $http.get('/api/user/' + data.userId).success(function(user){
        $scope.user = user;
      });
    }
  });

  $scope.activateAccount = function(){
     $http({
      method: 'POST',
      url: '/api/activateUserAccount',
      data:{ activated: false, userId: $scope.user.Model._id}
    }).success(function (data, status, headers, config){
      console.log(data);
    });
  };

});
'use strict';

app.controller('adminLoginCtrl', function ($scope, $http,$window){      

  $scope.signin = function(){
    $scope.loading = true;
    $scope.error = undefined;
    
    $http.post('/auth/adminlogin', $scope.credentials).success(function(response) {
      $scope.loading = false;
      $window.location.href = '/admin';
    }).error(function(response){
      $scope.loading = false;
      $scope.error = response;     
    });
  };
    
});

'use strict';

app.controller('appctrl', ['$rootScope','$route','$location', '$http', '$window', 'Facebook', function($rootScope,$route,$location,$http,$window,Facebook){     
  
  $http.get('/api/user/').success(function(data){  
    $rootScope.User = data;
    if(data){
      $rootScope.hasUser = true;
    }
    else{
     $rootScope.hasUser = false; 
    }

    $rootScope.$on('$viewContentLoaded', function(event) {
      $window._gaq.push(['_trackPageview', $location.path()]);
    });


    var getLoginStatus = function(){      
      var response = Facebook.getLoginStatus(function(response){
        return (response.status === 'connected' ? true : false);        
      });
      return response;
    };

    $rootScope.logoutFacebook = function(){      
      if(getLoginStatus() && $rootScope.User.Model.faceBookId){
        Facebook.logout(function(response) {
          $window.location.href = '/logout'  
        });
      }
      else{
        $window.location.href = '/logout'  
      }      
    };
  });

  $rootScope.openInstagramIframe = function(targetUrl){
    var dualScreenLeft = $window.screenLeft != undefined ? $window.screenLeft : screen.left;
    var dualScreenTop = $window.screenTop != undefined ? $window.screenTop : screen.top;
    var width  = 600,
        height = 600,
        left   = (($(window).width()  - width)  / 2 ) + dualScreenLeft,
        top    = (($(window).height() - height) / 2 ) + dualScreenTop,
        url    = this.href,
        opts   = 'status=1' + ',width='  + width  + ',height=' + height + ',top=' + top + ',left=' + left;

    $window.open(targetUrl, 'Instagram', opts); 
    return false;    
  };

}]);

'use strict';

app.controller('connectCtrl', function ($scope, $http, $routeParams, $route){
    
  $http.get('/api/user/').success(function(data){  
    $scope.user = data;
    if(data){
      $scope.userLoggedIn = true;
    }
    else{
     $scope.userLoggedIn = false; 
    }  
  });

  $scope.validateActivation = function(){    
    $http({
      method: 'POST',
      url: '/api/validateBib',
      data:{ bibnumber: $scope.bibNumber, activationCode: $scope.activationCode, userId: $scope.user.Model._id}
    }).success(function (data, status, headers, config){
      alert("Activated Bib!");
      $scope.bibNumber = '';
      $scope.activationCode = '';

      //

      //TODO
      //modal share on fb;
    }).error(function(data, status, headers, config){
      console.log(data);
      alert("Error in validation: \r\n" + data);
    })

  }
;});
'use strict';

app.controller('contactCtrl', function ($scope, $http, $routeParams, $route){

  $http.get('/api/allRaces').success(function(data){
    $scope.races = data.map(function(datum){ return datum.Model });
    $scope.eventName = $scope.races[0];
  });

  $scope.inquiries = [
    {name:'Missing Time', key: 0},
    {name:'Booking Inquiry', key: 1},
    {name:'Incorrect Name', key: 2},
    {name:'Activation Problems', key: 3},
    {name:'Others', key: 4}
  ];

  $scope.inquiry = $scope.inquiries[0];
  $scope.missingTimeInfo = true;
  $scope.showEvent = true;
  $scope.showBib = true;
  $scope.wrongNameInfo = false;
  $scope.activationInfo = false;
  $scope.contactForm = {};

  $scope.changeInquiry = function(){
    switch($scope.inquiry.key){
      case 0:
        //missing time
        $scope.missingTimeInfo = true;
        $scope.showEvent = true;
        $scope.showBib = true;
        $scope.wrongNameInfo = false;
        $scope.activationInfo = false;
        break;
      case 1:        
        //race organizer inquiry
        $scope.missingTimeInfo = false;
        $scope.showEvent = false;
        $scope.showBib = false;
        $scope.wrongNameInfo = false;
        $scope.activationInfo = false;
        break;
      case 2:
        //incorrect name
        $scope.missingTimeInfo = false;
        $scope.showEvent = true;
        $scope.showBib = true;
        $scope.wrongNameInfo = true;
        $scope.activationInfo = false;
        break;
      case 3:
        //activation problems
        $scope.missingTimeInfo = false;
        $scope.showEvent = true;
        $scope.showBib = true;
        $scope.wrongNameInfo = false;
        $scope.activationInfo = true;
        break;
      case 4:
        //others
        $scope.missingTimeInfo = false;
        $scope.showEvent = false;
        $scope.showBib = false;
        $scope.wrongNameInfo = false;
        $scope.activationInfo = false;
        break;
      default:
        //default
        $scope.missingTimeInfo = false;
        $scope.showEvent = false;
        $scope.showBib = false;
        $scope.wrongNameInfo = false;
        $scope.activationInfo = false;
        break;
    }
  }

  $scope.submitForm = function(){
    var emailBody = "";
    var dataToSend = {};

    if( !($scope.contactForm.name) || !($scope.contactForm.email)){
      alert("Name and Email are required fields!");
      return;
    }
    else{

      dataToSend.inquiry = $scope.inquiry.name;
      dataToSend.name = $scope.contactForm.name;
      dataToSend.email = $scope.contactForm.email;
      dataToSend.eventName = $scope.eventName.raceName;

      if($scope.inquiry.key == 0){
        //missing time
        emailBody = "Details: " + " \r\n " + "Bib Number: " + $scope.contactForm.bib + " \r\n " + "Estimated Finish: " + $scope.contactForm.estimateFinish + " \r\n " + "Finish Buddies: " + $scope.contactForm.runningBuddies + "\r\n ";
      }
      else if($scope.inquiry.key == 2){
        //incorect name
        emailBody = "Details: " + " \r\n " + "Bib Number/s: " + $scope.contactForm.bib + " \r\n " + "Correct Name: " + $scope.contactForm.correctName + "\r\n ";

      }
      else if($scope.inquiry.key == 3){
        //activation problems
        emailBody = "Details: " + " \r\n " + "Bib Number/s: " + $scope.contactForm.bib + " \r\n " + "Activation Code: " + $scope.contactForm.activationCode + "\r\n " + "Browser used: " + $scope.contactForm.browser + "\r\n " + "Error message: " + $scope.contactForm.errorMessage + " \r\n "; 
      }

      if($scope.contactForm.feedback) emailBody += $scope.contactForm.feedback;
      dataToSend.feedback = emailBody;

      $http({
        method: 'POST',
        url: '/contact',
        data: dataToSend
      }).success(function (data, status, headers, config){
        alert("Form Submitted!");
        $route.reload();
      }).error(function(data, status, headers, config){
        alert("Form Submission Failed. Try Again");
      })      
    }   
  }
});
'use strict';

app.controller('indexCtrl', function ($scope, $http){      
  //$scope.store  = storeService.store;
  $http.get('/api/races/available/0').success(function(data){    
    data =data.map(function(a){
      a.Model.raceDate = (new Date(a.Model.raceDate));
      return a;
    })
    data.splice(5);
    $scope.races = data;
  });
  $http.get('/api/races/upcoming/0').success(function(data){
    data =data.map(function(a){
      a.Model.raceDate = (new Date(a.Model.raceDate))
      return a;
    })
    data.splice(5);
    $scope.upcomingRaces = data;
  });

  $scope.segmentMarker = true;
  $scope.autoOpen = true;

  $scope.openSegment = function(){
    $('#sidelikesegment').toggle(200);
    if($scope.segmentMarker){      
      $('#floatinglikebutton').animate({"right":"+=385"}, 200); 
      $scope.segmentMarker = !$scope.segmentMarker;
    }
    else{      
      $('#floatinglikebutton').animate({"right":"-=385"}, 200);  
      $scope.segmentMarker = !$scope.segmentMarker;
    }
  }

  $scope.closeSegment = function(){
    $('#sidelikesegment').toggle(200);
    if( $scope.segmentMarker){      
        $('#floatinglikebutton').animate({"right":"+=385"}, 200); 
         $scope.segmentMarker = ! $scope.segmentMarker;
    }
    else{      
        $('#floatinglikebutton').animate({"right":"-=385"}, 200);  
         $scope.segmentMarker = !$scope.segmentMarker;
    }
  }
;});
'use strict';

app.controller('loginCtrl', function ($scope, $window, $http){      
  
  $scope.register = function(){
    $window.location.href = '#/signup';
  };

  $scope.signin = function(){
    $scope.loading = true;
    $scope.error = undefined;
    
    $http.post('/auth/login', $scope.credentials).success(function(response) {
      $scope.loading = false;
      $window.location.href = '/me';
    }).error(function(response){
      $scope.loading = false;
      $scope.error = response;     
    });
  };

});

app.controller('registerCtrl', function ($scope, $http, $window){
  $scope.genders = [
    {"name":"Male", "value":"M"},
    {"name": "Female", "value": "F"}
  ];

  $scope.loading = false;
  
  $scope.register = function(){
    var postData = $scope.data;
    $scope.loading = true;
    
    $http.post('/auth/register', postData).
    success(function(response){
      $scope.loading = false;
      $scope.showMeModal();
    }).error(function(response){
      $scope.loading = false;
      alert(response);
    });
  }

  $scope.showMeModal = function(){
    $('.basic.modal')
      .modal('setting', {
        onHide: function(){
          $window.location.href = '/';
        }
      })
      .modal('show');
  };

});

'use strict';

app.controller('pageNotFoundCtrl', function ($scope, $http){      

});

'use strict';

var isNumber = function(n){
  return !isNaN(parseFloat(n)) && isFinite(n);
};

var SecondsToTimeString = function(seconds){  
  var hours = parseInt(seconds / 3600) % 24;
  var minutes = parseInt(seconds / 60) % 60;
  var seconds = seconds % 60;
  var TimeStr = (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds);
  return TimeStr;  
};

app.controller('profileCtrl', function ($scope, $http, $routeParams, $route, $location, $window, Facebook){
  $scope.totalDistance = 0;  
  $scope.profilePic = '/images/240px-square.png';
  $scope.profileOwner = '';
  $scope.hidePublic = true;
  $scope.hideInputWeight = true;  
  $scope.caloriesBurned = "0.00 cals"
  $scope.totalMinutes = 0;  

  $scope.$watch(
    function() {
      return Facebook.isReady();
    },
    function(newVal) {
      if (newVal)
        $scope.facebookReady = true;
    }
  );

  $scope.hoverFunction = function(socialDivId){
    if($location.path() == '/me')
    { 
      $('#' + socialDivId).css('opacity',1);
    }
  }


  $scope.leaveFunction = function(socialDivId){    
    $('#' + socialDivId).css('opacity',0);
  }

  $scope.shareFB = function(result){
    var obj = {
      method: 'feed',    
      link: $window.location.href,
      picture: 'http://myrunti.me/images/240px-square.png',
      name:  result.Model.firstName + " " + result.Model.lastName + "'s Run Time via MyRunTime" ,
      caption:  result.Model.raceId.raceName ,
      description: result.Model.firstName + " "+ result.Model.lastName +" finished " + result.Model.raceId.raceName + " in " + SecondsToTimeString(result.Model.gunTime)
    };

    Facebook.ui(obj, null);
  }

  $scope.showOverlayForm = function(){
    $scope.hideInputWeight = false;
  };

  $scope.hideOverlayForm = function(){
    $scope.hideInputWeight = true;
  };

  $scope.validateInput = function(){ 
    if(!isNumber($scope.inputWeight)){
      $scope.invalidInput = true;
    }
    else{
      $http({
        method: 'POST',
        url: '/api/submitWeight',
        data:{ weight: $scope.inputWeight, userId: $scope.user.Model._id}
      }).success(function (data, status, headers, config){
        $scope.invalidInput = false;
        $scope.hideInputWeight = true;
        $scope.weightInKg = $scope.inputWeight / 2.2;
        $scope.totalCalories = ($scope.totalMinutes*7.0*$scope.weightInKg).toFixed(2);
        $scope.caloriesBurned = $scope.totalCalories + " cals";
      });
    }    
  }


  $scope.disconnectUpcoming = function(race){
    var r = confirm("This will disconnect this bib from your MyRunTime account. Do you want to continue?");
    if(r){
      $http({
        method:'POST',
        url:'/api/disconnectUpcoming',
        data: {bibnumber: race.bibDetails.Model.bibnumber, raceId: race.bibDetails.Model.raceId}
      }).success(function(data, status, headers, config){
        $route.reload();
      })
    }
  }

  
  $scope.disconnectBib = function(result){
    var r = confirm("This will disconnect this bib from your MyRunTime account. Do you want to continue?");
    if(r){
      $http({
        method:'POST',
        url:'/api/disconnectWithResult',
        data: {bibnumber: result.Model.bibnumber, raceId: result.Model.raceId._id}
      }).success(function(data, status, headers, config){
        $route.reload();
      })
    }      
  }


  if($location.path().indexOf("/me") == 0){
    $http.get('/api/user/').success(function(data){  
      if(data){
        $scope.dashboardColumnClass = "col-md-4";
        $scope.hidePublic = false;
        $scope.profileOwner = 'My';
        $http.get('/api/user/' + data.userId).success(function(user){
          $scope.user = user;
          $scope.inputWeight = user.Model.weight;
          $scope.weightInKg = user.Model.weight / 2.2;
          if($scope.user.Model.faceBookId){
            $http.get('http://graph.facebook.com/v2.3/' + $scope.user.Model.faceBookId + '/picture?redirect=0&height=240&type=normal&width=240').success(function(data){
              $scope.profilePic = data.data.url;
            });
          }
            
          $http({
            method: 'POST',
            url: '/api/user/getFriendsDetails',
            data:{ friendIds: $scope.user.Model.friends}
          }).success(function (data, status, headers, config){
            $scope.userFriends = data;
          })
          $('#friendsListModal').modal();
        });        

        $http.get('/api/results/' + data.userId).success(function(data){
          $scope.results = data;
          $scope.results.forEach(function(result){ 
            $scope.totalDistance += result.Wave.waveDistance;
            $scope.totalMinutes += (result.Model.chipTime)/3600;
          });

          $scope.totalCalories = ($scope.totalMinutes*7.0*$scope.weightInKg).toFixed(2);
          $scope.caloriesBurned = $scope.totalCalories + " cals";
        })

        $http.get('/api/bib/' + data.userId).success(function(data){
          $scope.upcomingRaces = data;    
          //console.log($scope.upcomingRaces);
        })
      }
      else{
        $location.path('login');
      }
    });
  }
  else{    
    $scope.dashboardColumnClass = "col-md-6";
    $http.get('/api/user/' + $routeParams.userId).success(function(data){  
      $scope.user = data;
      $scope.profileOwner = data.Model.firstName +"'s ";
      $http.get('http://graph.facebook.com/v2.3/' + $scope.user.Model.faceBookId + '/picture?redirect=0&height=240&type=normal&width=240').success(function(data){
        $scope.profilePic = data.data.url;
      });
      $http({
        method: 'POST',
        url: '/api/user/getFriendsDetails',
        data:{ friendIds: $scope.user.Model.friends}
      }).success(function (data, status, headers, config){
        $scope.userFriends = data;
      });

      $('#friendsListModal').modal();
    });

    $http.get('/api/results/' + $routeParams.userId).success(function(data){
      $scope.results = data;
      $scope.results.forEach(function(result){ 
        $scope.totalDistance += result.Wave.waveDistance;
      });
    })

    $http.get('/api/bib/' + $routeParams.userId).success(function(data){
      $scope.upcomingRaces = data;
      //console.log($scope.upcomingRaces);
    })
  }  
});

'use strict';

app.controller('raceCtrl', function ($scope, $http, $routeParams, $location){  
  //$scope.store  = storeService.store;

  $scope.searchBib = function(bibNumber){    
    var location = "runner/" + $scope.race._id + "/" + bibNumber;    
    $location.path(location);    
  }

  $scope.resultsOverdue = false;  

  $http.get('/api/race/nick/'+$routeParams.raceNick).success(function(data){
    
    data.raceDate = (new Date(data.raceDate));
    var dateToday = new Date();

    if(dateToday > data.raceDate){
      $scope.resultsOverdue = true;
    }

    $scope.race = data;
  }).error(function(data){
    $location.path("pageNotFound");
  });
;});
'use strict';

app.controller('racesCtrl', function ($scope, $http){  
  //$scope.store  = storeService.store;
  $scope.page = 0;
  $scope.races = [];
  var fetchData = function(){
    $http.get('/api/races/available/'+$scope.page).success(function(data){
      data = data.map(function(a){
        a.Model.raceDate = (new Date(a.Model.raceDate));
        $scope.races.push(a);
      })
      $scope.page++;
      
    });
    $http.get('/api/races/upcoming/0').success(function(data){
      data =data.map(function(a){
        a.Model.raceDate = (new Date(a.Model.raceDate));
        return a;
      })
      $scope.upcomingRaces = data;
    });
  }
  fetchData();
  $scope.loadMore = fetchData;

  $scope.segmentMarker = true;
  $scope.autoOpen = true;

  $scope.openSegment = function(){
    $('#sidelikesegment').toggle(200);
    if($scope.segmentMarker){      
      $('#floatinglikebutton').animate({"right":"+=385"}, 200); 
      $scope.segmentMarker = !$scope.segmentMarker;
    }
    else{      
      $('#floatinglikebutton').animate({"right":"-=385"}, 200);  
      $scope.segmentMarker = !$scope.segmentMarker;
    }
  }

  $scope.closeSegment = function(){
    $('#sidelikesegment').toggle(200);
    if( $scope.segmentMarker){      
        $('#floatinglikebutton').animate({"right":"+=385"}, 200); 
         $scope.segmentMarker = ! $scope.segmentMarker;
    }
    else{      
        $('#floatinglikebutton').animate({"right":"-=385"}, 200);  
         $scope.segmentMarker = !$scope.segmentMarker;
    }
  }
;});
app.controller('runnerCtrl', function ($rootScope, $scope, $http, $routeParams, $window, Facebook){
  var raceId = $routeParams.raceId;
  var bibnumber = $routeParams.bibnumber;
  var waveId;
  var race;

  $scope.disableSubmit;

  if( _.contains($rootScope.hideRankRaceIds, parseInt(raceId,10)) ){
    $scope.hideRank = true;
  }
  else{
    $scope.hideRank = false;
  }

  if( _.contains($rootScope.hideBibRaceIds, parseInt(raceId,10)) ){
    $scope.hideBib = true;
  }
  else{
    $scope.hideBib = false;
  }

  if( _.contains($rootScope.hideOfficialTimeRaceIds, parseInt(raceId,10)) ){
    $scope.hideOfficialTime = true;
  }
  else{
    $scope.hideOfficialTime = false;
  }

  if( _.contains($rootScope.hideChipTimeRaceIds, parseInt(raceId,10)) ){
    $scope.hideChipTime = true;
  }
  else{
    $scope.hideChipTime = false;
  }

  if( _.contains($rootScope.hideFriendsRaceIds, parseInt(raceId,10)) ){
    $scope.hideFriends = true;
  }
  else{
    $scope.hideFriends = false;
  }

  if( _.contains($rootScope.hideNamesIds, parseInt(raceId, 10)) ){
    $scope.hideName = true;
  }
  else{
    $scope.hideName = false;
  }



  $scope.$watch(
    function() {
      return Facebook.isReady();
    },
    function(newVal) {
      if (newVal)
        $scope.facebookReady = true;
    }
  );

  $scope.shareFB = function(runner){    
    
    var obj = {
      method: 'feed',    
      link: $window.location.href,
      picture: 'http://myrunti.me/images/240px-square.png',
      name:  runner.firstName + " " + runner.lastName + "'s Run Time via MyRunTime" ,
      caption:  runner.raceId.raceName ,
      description: runner.firstName + " "+ runner.lastName +" finished " + runner.raceId.raceName + " in " + SecondsToTimeString(runner.gunTime)
    };

    Facebook.ui(obj, null);
  }

  //$scope.store  = storeService.store;
  $scope.race = null;
  $scope.runner = null;
  $scope.Perf = {}  
  $scope.hideOverlay = true;  
  $scope.hasSplits = false;
  $scope.hasFriendsInRace = false;
  $scope.friendsInRace = [];
  
  $scope.closeOverlay = function(){
    $scope.hideOverlay = true;
    $window.history.back();
  }

  $scope.gunTimeChartSeries = []
  $scope.chipTimeChartSeries = [];
  $scope.chartCategories = [];  
  $scope.gunPlotCategories = [];
  $scope.chipPlotCategories = [];

  $scope.gunPlotSeries = [{
    name: 'Number of finishers',
    data: [],
    type: 'line',
    datalabels: { enabled: false } 
    },
    {
      tooltip:{ enabled:true },
      name: 'Number of finishers',
      data: []
    }
  ];

  $scope.chipPlotSeries = [{
    name: 'Number of finishers',
    data: [],
    type: 'line',
    datalabels: { enabled: false } 
  },
  {
    tooltip:{ enabled:true },
    name: 'Number of finishers',
    data: []
  }];

  $scope.pacePlotCategories = [];
  $scope.splitsPieCategories = [];

  $scope.splitsPieSeries = [{
    name: 'Sector Length',
    data: [],
    size: '90%',
    innerSize: '40%',
    dataLabels:{
      formatter: function() {
        return this.y > 5 ? '<b>' + this.point.name + '</b>' : null;
      },
      color: 'white',
      distance: -55
    },
  },
  {
    name: 'Sector Time',
    data: [],
    size: '90%',
    innerSize: '60%',
    dataLabels: { 
      formatter: function(){
        return  '<b>'+ this.point.name +':</b>  '+ SecondsToTimeString(this.y) + '<br/><b>Distance</b>: ' + ($scope.sectorTimes[this.point.x].sectorLength/1000).toFixed(2) + " km";
      }
    }    
  }];

  $scope.pacePlotSeries = [{
    tooltip: {enabled: true},
    name: 'Pace',
    data: []
  }];

  $scope.gunChartConfig = {
    options: {
      chart: {
          type: 'bar'
      },
      plotOptions: {
        series: {
          stacking: 'stacked'
        }        
      }
    },      
    xAxis: {          
      categories: $scope.chartCategories,
      labels:{
        enabled: true,
        formatter: function(){
          return this.value;
        }
      },
      useHTML: true
    },
    yAxis: {
      min: 0,
      title: {
          text: 'Number of runners'
      },
      labels:{
        enabled: true,
        formatter: function(){
          return this.value;
        }
      },
      useHTML: true
    },  
    legend: {
      enabled:true,
      backgroundColor: '#FFFFFF',
      reversed: true
    },    
    series: $scope.gunTimeChartSeries,  
    title: {
      text: ''
    },      
    credits: {
      enabled: false
    },
    loading: true
  }

  $scope.chipChartConfig = {
    options: {
      chart: {
          type: 'bar'
      },
      plotOptions: {
        series: {
          stacking: 'stacked'
        }        
      }
    },
    xAxis: {          
      categories: $scope.chartCategories,
      labels:{
        enabled: true,
        formatter: function(){
          return this.value;
        }
      },
      useHTML: true
    },
    yAxis: {
      min: 0,
      title: {
          text: 'Number of runners'
      },
      labels:{
        enabled: true,
        formatter: function(){
          return this.value;
        }
      },
      useHTML: true
    },  
    legend: {
      enabled:true,
      backgroundColor: '#FFFFFF',
      reversed: true
    },
    series: $scope.chipTimeChartSeries,  
    title: {
      text: ''
    },      
    credits: {
      enabled: false
    },
    loading: true
  }

  $scope.gunTimePlotConfig = {  
    options:{
      chart: {
        defaultSeriesType: 'column',
        backgroundColor : '#FFF',
      },
      legend:{
        enabled: false
      },    
      plotOptions : {
        column : {
          borderColor : "transparent"
        }
      },
      //colors : ['#3a3a3a'],
    },
    title: {
      text: '',
      style : {
        color : '#e56d1e'
      }
    },
    subtitle: {
      text: '',
      style : {
        color : '#e56d1e'
      }
    },    
    series: $scope.gunPlotSeries,
    credits: {
      enabled: false
    },
    yAxis: {
      title: {
        text: 'No. of runners',
        style : {
          color : '#e56d1e'
        }
      }
    },
    xAxis: {
      categories: $scope.gunPlotCategories,      
      title: {
        text: 'Finishing Times',
        style: {
          color: '#e56d1e'
        }
      },
      labels: {
        staggerLines: 2
      }
    },
    loading: true
  }

  $scope.chipTimePlotConfig = {  
    options:{
      chart: {
        defaultSeriesType: 'column',
        backgroundColor : '#FFF',
      },
      legend:{
        enabled: false
      },    
      plotOptions : {
        column : {
          borderColor : "transparent"
        }
      },
      //colors : ['#3a3a3a'],
    },
    title: {
      text: '',
      style : {
        color : '#e56d1e'
      }
    },
    subtitle: {
      text: '',
      style : {
        color : '#e56d1e'
      }
    },    
    series: $scope.chipPlotSeries,
    credits: {
      enabled: false
    },    
    yAxis: {
      title: {
        text: 'No. of runners',
        style : {
          color : '#e56d1e'
        }
      }
    },
    xAxis: {
      categories: $scope.chipPlotCategories,      
      title: {
        text: 'Chip Time Segments',
        style: {
          color: '#e56d1e'
        }
      },
      labels: {
        staggerLines: 2
      }
    },
    loading: true
  }

  $scope.pacePlotConfig = {
    options:{
      chart:{
        defaultSeriesType: 'column',
        backgroundColor: '#FFF'
      },
      plotOptions: {
        column:{
          borderColor: "transparent"
        }
      },
      tooltip:{
        formatter: function(){
          return '' +
           '<b> Sector ' + (this.key+1) + '</b> - ' + SecondsToHHMMSS(this.point.y) + ' per km';
        }
      },
      credits: {enabled: false},
      legend: {enabled: false}
    },
    title: {
      text: ''
    },
    yAxis: {
      title: {
        text: 'Pace (seconds/km)',
        style : {
          color : '#e56d1e'
        }
      }
    },
    xAxis: {
      title: {
        text: 'Sector',
        style : {
          color : '#e56d1e'
        }
      },
      categories: $scope.pacePlotCategories
    },
    loading: true,
    series: $scope.pacePlotSeries

  }

  $scope.splitsPieConfig = {
    options:{
      chart: {
        type: 'pie'
      },
      plotOptions: {
        pie: {shadow: false}
      },
      tooltip:{
        formatter: function() {
          if (this.series.name == "Sector Length") 
            return ''+
              '<b>' + this.key + '</b> - ' + (this.point.y/1000).toFixed(2) + 'km' ;
          else{
            return ''+
              '<b>' + this.key + '</b> - ' + SecondsToTimeString(this.point.y) ;              
          }
        }
      },
      credits: {enabled: false},
      legend: {enabled: false}
    },
    title: {
      text: 'Sector by sector performance'
    },
    loading: true,
    series: $scope.splitsPieSeries
  }


  var loadRunner = function(){
    $http.get('/api/runners/'+raceId+'/bibnumber/'+bibnumber).success(function(data){
      if(data && data.Model){
        if(data.Model.userId){
          $http.get('http://graph.facebook.com/' + data.Model.userId.faceBookId + '/picture?redirect=0&height=240&type=normal&width=240').success(function(data){
            $scope.profilePic = data.data.url;
          });
        }      
        
        data.Model.gunTimeStr = SecondsToTimeString(data.Model.gunTime);
        data.Model.chipTimeStr = SecondsToTimeString(data.Model.chipTime);
        waveId = data.Model.waveId;

        $scope.runner = data;
        $scope.runnerGender = ($scope.runner.Model.gender == 'M') ? 'Male' : 'Female';
        $scope.sectorTimes = data.Model.sectorTimes;
        
        if(data.Model.sectorTimes.length > 0){          
          $scope.hasSplits = true;  
        }

        if(data.friendsResults.length > 0){
          $scope.hasFriendsInRace = true;
          $scope.friendsInRace =  data.friendsResults;
        }
        loadRace();        
      }
      else{
        console.log('Bib number not found.');
        $scope.contactForm = {};

        //Disable Pre Emptive
        var emailBody = "";
        var dataToSend = {};

        $http.get('/api/race/'+raceId).success(function(data){
          race = data;

          $scope.contactForm.eventName = race.raceName;
          $scope.contactForm.bib = bibnumber;

          dataToSend.inquiry = "Pre-emptive Email on Missing Time";
          dataToSend.name = "NO NAME";
          dataToSend.email = "NO EMAIL";
          dataToSend.eventName = race.raceName;

          emailBody = "Details: " + " \r\n " + "Bib Number: " + $scope.contactForm.bib + " \r\n " + "Estimated Finish: " + "" + " \r\n " + "Finish Buddies: " + "" + "\r\n ";
          if($scope.contactForm.feedback) emailBody += "Feedback: " + $scope.contactForm.feedback;
          dataToSend.feedback = emailBody;
        });

          //Disable Pre Emptive
        /*
          $http({
            method: 'POST',
            url: '/contact',
            data: dataToSend
          }).success(function (data, status, headers, config){
            // alert("Form Submitted!");
            // $route.reload();
          }).error(function(data, status, headers, config){
            // alert("Form Submission Failed. Try Again");
          }) 
        });
        */

        $scope.hideOverlay = false;
      }        
    });    
  }

  $scope.submitForm = function() {
    var emailBody = "";
    var dataToSend = {};
    var estimateFinish;
    var runningBuddies;

    $scope.disableSubmit = true;

    dataToSend.inquiry = "Missing Time";
    dataToSend.name = $scope.contactForm.name;
    dataToSend.email = $scope.contactForm.email;
    dataToSend.eventName = race.raceName;

    estimateFinish = ($scope.contactForm.estimateFinish) ? $scope.contactForm.estimateFinish : "";  
    runningBuddies = ($scope.contactForm.runningBuddies) ? $scope.contactForm.runningBuddies : "";

    emailBody = "Details: " + " \r\n " + "Bib Number: " + $scope.contactForm.bib + " \r\n " + "Estimated Finish: " + estimateFinish + " \r\n " + "Finish Buddies: " + runningBuddies + "\r\n ";
    if($scope.contactForm.feedback) emailBody += "Feedback: " + $scope.contactForm.feedback;
    dataToSend.feedback = emailBody;

    $http({
      method: 'POST',
      url: '/contact',
      data: dataToSend
    }).success(function (data, status, headers, config){
      $scope.disableSubmit = false;
      alert("Thank you for your feedback! We will get back to you as soon as possible.");
      $window.history.back();
    }).error(function(data, status, headers, config){
      alert("Form Submission Failed. Try Again");
    }) 
  }

  var loadRace = function(){
    $http.get('/api/race/'+raceId).success(function(data){      
      $scope.race = data;
      $http.get('/api/wave/'+raceId + '/'+ waveId).success(function(wave){
        $scope.wave = wave;
        $scope.raceStart = (new Date(wave.waveStart)).toLocaleTimeString();
        $scope.runnerFinish = (new Date(new Date(wave.waveStart).getTime() + ($scope.runner.Model.gunTime*1000))).toLocaleTimeString();
        $scope.runnerStart = new Date((new Date(new Date(wave.waveStart).getTime() + ($scope.runner.Model.gunTime*1000))).getTime() - ($scope.runner.Model.chipTime*1000)).toLocaleTimeString();
        $scope.compet = JSON.parse(wave.runnersText);            
        $scope.minGunTime = $scope.compet[0].o;
        $scope.maxGunTime = $scope.compet[$scope.compet.length - 1].o;
        computeTimes();
      })  
    });     
  }

  var computeTimes = function(){
    if ((!$scope.race)||(!$scope.runner)) return;
    else {      

      var compet = $scope.compet;      

      var competByGender = compet.filter(function(data) { return data.g == $scope.runner.Model.gender });
      $scope.competByGender = competByGender;
      
      for (var i = 0; i < compet.length; i++){
        if ($scope.runner.Model.gunTime <= compet[i].o){
          $scope.Perf.officialRank = i+1;
          break;
        }
      }

      for (var i = 0; i< competByGender.length; i++){        
        if ($scope.runner.Model.gunTime <= competByGender[i].o){
          $scope.Perf.genderGunRank = i+1;
          break;
        }
      }

      var sortByChip = compet.sort(function(a,b){return (a.c - b.c);})
      $scope.minChipTime = sortByChip[0].c;
      $scope.maxChipTime = sortByChip[sortByChip.length - 1 ].c;
      var sortByChipGender = competByGender.sort(function(a,b){ return (a.c - b.c);})      

      for (var i = 0; i < sortByChip.length; i++){
        if ($scope.runner.Model.chipTime <= sortByChip[i].c){
          $scope.Perf.chipRank = i+1;
          break;
        }
      }

      for (var i = 0; i < sortByChipGender.length; i++){
        if ($scope.runner.Model.chipTime <= sortByChipGender[i].c){
          $scope.Perf.genderChipRank = i+1;
          break;
        }
      } 
      computeBins();
    }
  }

  var computeBins = function(){
    var binCount = 15;

    var minGunTime = $scope.minGunTime;        
    var maxGunTime = $scope.maxGunTime;

    var minChipTime = $scope.minChipTime;
    var maxChipTime = $scope.maxChipTime;

    var exactGunTimeGap = (maxGunTime - minGunTime) / binCount;    
    var gunTimeGap = parseInt(exactGunTimeGap);
    
    var exactChipTimeGap = (maxChipTime - minChipTime) / binCount;
    var chipTimeGap = parseInt(exactChipTimeGap);

    $scope.gunTimeBinBoundaries = [];
    var gunTimeStartBoundary = minGunTime;
    var gunTimeEndBoundary = gunTimeStartBoundary + gunTimeGap;

    $scope.chipTimeBinBoundaries = [];
    var chipTimeStartBoundary = minChipTime;
    var chipTimeEndBoundary = chipTimeStartBoundary + chipTimeGap;


    for(var ctr = 0; ctr < binCount; ctr++){
      $scope.gunTimeBinBoundaries.push({
        min: gunTimeStartBoundary,
        minTime: SecondsToHHMM(gunTimeStartBoundary),
        max: gunTimeEndBoundary,
        maxTime: SecondsToHHMM(gunTimeEndBoundary),
        count: 0
      });

      $scope.chipTimeBinBoundaries.push({
        min: chipTimeStartBoundary,
        minTime: SecondsToHHMM(chipTimeStartBoundary),
        max: chipTimeEndBoundary,
        maxTime: SecondsToHHMM(chipTimeEndBoundary),
        count: 0
      });

      gunTimeStartBoundary = gunTimeEndBoundary;
      gunTimeEndBoundary = gunTimeEndBoundary + gunTimeGap;
      chipTimeStartBoundary = chipTimeEndBoundary;
      chipTimeEndBoundary = chipTimeEndBoundary + chipTimeGap;
    }
    
    $scope.compet.forEach(function(detail){
      var gunTimeBinNumber = parseInt((detail.o - minGunTime) / exactGunTimeGap);
      var chipTimeBinNumber = parseInt((detail.c - minChipTime) / exactChipTimeGap);      
      if(gunTimeBinNumber == 15) gunTimeBinNumber = 14;
      if(chipTimeBinNumber == 15) chipTimeBinNumber = 14;

      $scope.gunTimeBinBoundaries[gunTimeBinNumber].count += 1; 
      $scope.chipTimeBinBoundaries[chipTimeBinNumber].count+= 1; 
    })    
    loadCharts();
    $(window).resize();    
  }

  var loadCharts = function(){
    var officialRankPercent = ($scope.compet.length - $scope.Perf.officialRank) / $scope.compet.length;
    var officialPercentile = (officialRankPercent * 100).toFixed(2);

    var chipRankPercent = ($scope.compet.length - $scope.Perf.chipRank) / $scope.compet.length;
    var chipPercentile = (chipRankPercent * 100).toFixed(2);

    var gunChartTitle = $scope.runner.Wave.waveName + " Rank: " + $scope.Perf.officialRank + " out of " + $scope.compet.length + " runners (" +  officialPercentile + "% percentile)";
    var chipChartTitle = $scope.runner.Wave.waveName + " Rank: " + $scope.Perf.chipRank + " out of " + $scope.compet.length + " runners (" +  chipPercentile + "% percentile)";

    $scope.gunChartConfig.title.text = gunChartTitle;
    $scope.chipChartConfig.title.text = chipChartTitle;

    $scope.chartCategories.push($scope.runner.Wave.waveName + " Runners");
    $scope.chartCategories.push($scope.runnerGender + " Runners");

    $scope.gunTimeChartSeries.push({
      name: 'Finished after you',
      data:[
      {
        y: $scope.compet.length - $scope.Perf.officialRank,
        datalabels:{
          enabled: true
        }
      },
      {
        y: $scope.competByGender.length -$scope.Perf.genderGunRank, 
        datalabels: {
          enabled:true,
        }  
      }]
    });

    $scope.gunTimeChartSeries.push({
      name: 'Finished before you',
      data:[
      {
        y: $scope.Perf.officialRank - 1,   
        datalabels: {
          enabled:true,
        }            
      },
      {
        y: $scope.Perf.genderGunRank - 1, 
        datalabels: {
          enabled:true,
        }  
      }]
    });

    $scope.chipTimeChartSeries.push({
      name: 'Had a slower time than you',
      data:[
      {
        y: $scope.compet.length - $scope.Perf.chipRank,
        datalabels:{
          enabled: true
        }
      },
      {
        y: $scope.competByGender.length -$scope.Perf.genderChipRank, 
        datalabels: {
          enabled:true,
        }  
      }]
    });

    $scope.chipTimeChartSeries.push({
      name: 'Had a faster time than you',
      data:[
      {
        y: $scope.Perf.chipRank - 1,   
        datalabels: {
          enabled:true,
        }            
      },
      {
        y: $scope.Perf.genderChipRank - 1, 
        datalabels: {
          enabled:true,
        }  
      }]
    });

    $scope.gunTimePlotConfig.title.text = $scope.runner.Wave.waveName + " finishers over time";
    $scope.gunTimePlotConfig.subtitle.text = "Official time: " + SecondsToTimeString($scope.runner.Model.gunTime) + " - Rank:" + $scope.Perf.officialRank + " out of " + $scope.compet.length + " runners";

    $scope.gunTimeBinBoundaries.forEach(function(gunTimeBin){
      $scope.gunPlotCategories.push(gunTimeBin.minTime + " - " + gunTimeBin.maxTime);

      if($scope.runner.Model.gunTime >= gunTimeBin.min && $scope.runner.Model.gunTime < gunTimeBin.max){
        $scope.gunPlotSeries[1].data.push({
          y: parseInt(gunTimeBin.count),
          color: '#E56E1E'
        });

        $scope.gunPlotSeries[0].data.push({
          y: parseInt(gunTimeBin.count),
          marker:{
            fillColor: '#E56E1E',
            radius: 8
          }
        })
      }
      else{
        $scope.gunPlotSeries[1].data.push({
          y: parseInt(gunTimeBin.count)
        });
        $scope.gunPlotSeries[0].data.push({
          y: parseInt(gunTimeBin.count),          
          marker:{
            enabled:false
          }
        })
      }
    })


    $scope.chipTimePlotConfig.title.text = "Plot of runner chip times";
    $scope.chipTimePlotConfig.subtitle.text = "Chip time: " + SecondsToTimeString($scope.runner.Model.chipTime) + " - Chip Rank: " + $scope.Perf.chipRank + " out of " + $scope.compet.length + " runners"; 

    $scope.chipTimeBinBoundaries.forEach(function(chipTimeBin){
      $scope.chipPlotCategories.push(chipTimeBin.minTime + " - " + chipTimeBin.maxTime);

      if($scope.runner.Model.chipTime >= chipTimeBin.min && $scope.runner.Model.chipTime < chipTimeBin.max){
        $scope.chipPlotSeries[1].data.push({
          y: parseInt(chipTimeBin.count),
          color: '#E56E1E'
        });

        $scope.chipPlotSeries[0].data.push({
          y: parseInt(chipTimeBin.count),
          marker:{
            fillColor: '#E56E1E',
            radius: 8
          }
        })
      }
      else{
        $scope.chipPlotSeries[1].data.push({
          y: parseInt(chipTimeBin.count)
        })
        $scope.chipPlotSeries[0].data.push({
          y:parseInt(chipTimeBin.count),
          marker:{
            enabled: false
          }
        })
      }
    })

    if($scope.hasSplits){
      $scope.sectorTimes.forEach(function(sectorTime, index){

        $scope.chipPlotCategories.push(index + 1);
        var _start = new Date(sectorTime.sectorStart);
        var _end = new Date(sectorTime.sectorEnd);
        var _pace = ((_end-_start)/(sectorTime.sectorLength));       

        $scope.pacePlotSeries[0].data.push({
          y: parseFloat(_pace),
          color: '#E56E1E'
        })

        $scope.splitsPieSeries[0].data.push({
          name: "Sector " + (index+1),
          y: sectorTime.sectorLength
        });

        $scope.splitsPieSeries[1].data.push({
          name: "Sector " + (index+1) + " Time",
          y: (_end-_start)/1000
        })
      })
    }

    $scope.chipChartConfig.loading = false;
    $scope.gunChartConfig.loading = false;
    $scope.gunTimePlotConfig.loading = false;
    $scope.chipTimePlotConfig.loading = false;
    $scope.pacePlotConfig.loading = false;
    $scope.splitsPieConfig.loading = false;
  }

  loadRunner();

  var SecondsToHHMM = function(seconds) {
    var hours = parseInt(seconds / 3600) % 24;
    var minutes = parseInt(seconds / 60) % 60;
    
    var TimeStr = (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes);
    return TimeStr;
  }

  var SecondsToHHMMSS = function(seconds) {
    var hours = parseInt(seconds/3600) % 24;
    var minutes = parseInt(seconds / 60) % 60;
    var seconds = (seconds % 60).toFixed(3);  
    var TimeStr;
    if(hours == 0){
      TimeStr = (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds);    
    }
    else{
      TimeStr = (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds);    
    }  
    return TimeStr;
  }

  var SecondsToTimeString = function(seconds){
    var hours = parseInt(seconds / 3600) % 24;
    var minutes = parseInt(seconds / 60) % 60;  
    var seconds = (seconds % 60).toFixed(0);
    var TimeStr = (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds);
    return TimeStr;    
  }
;});

'use strict';

app.controller('waveCtrl', function ($rootScope, $scope, $http, $routeParams, $location){
  var raceId = $routeParams.raceId;
  var waveId = $routeParams.waveId;
  $scope.shownAll = false;
  $scope.disableLoadMore = false;
  //$scope.store  = storeService.store;

  if( _.contains($rootScope.hideRankRaceIds, parseInt(raceId,10)) ){
    $scope.hideRank = true;
  }
  else{
    $scope.hideRank = false;
  }

  if( _.contains($rootScope.hideBibRaceIds, parseInt(raceId,10)) ){
    $scope.hideBib = true;
  }
  else{
    $scope.hideBib = false;
  }

  if( _.contains($rootScope.hideOfficialTimeRaceIds, parseInt(raceId,10)) ){
    $scope.hideOfficialTime = true;
  }
  else{
    $scope.hideOfficialTime = false;
  }

  if(_.contains($rootScope.sortByChipTimeRaceIds, parseInt(raceId, 10)) ){
    $scope.sortByChipTime = true;
  }
  else{
   $scope.sortByChipTime = false; 
  }

  if( _.contains($rootScope.hideNamesIds, parseInt(raceId, 10)) ){
    $scope.hideName = true;
  }
  else{
    $scope.hideName = false;
  }

  $scope.page = 0;
  $scope.runners = [];

  var loadRunners = function(){  
    if($scope.hideRank){
      $http.get('/api/race/'+raceId+'/'+waveId).success(function(data){      
        data.map(function(runner){
          runner.Model.gunTime = runner.Model.gunTime;
          runner.Model.chipTime = runner.Model.chipTime;
          runner.Model.firstName = runner.Model.firstName;
          runner.Model.lastName = runner.Model.lastName;
          runner.rank = $scope.runners.length+1;
          $scope.runners.push(runner);
        });
      });
    }

    else{
      if($scope.sortByChipTime){
        $http.get('/api/raceByChip/'+raceId+'/'+waveId+'/'+$scope.page).success(function(data){      
          //$http.get('/api/race/'+raceId+'/'+waveId).success(function(data){      
          data.map(function(runner){
            runner.Model.gunTime = runner.Model.gunTime;
            runner.Model.chipTime = runner.Model.chipTime;
            runner.Model.firstName = runner.Model.firstName;
            runner.Model.lastName = runner.Model.lastName;
            runner.rank = $scope.runners.length+1;
            $scope.runners.push(runner);
          });
          $scope.page++;
        })
      }
      else{
        $http.get('/api/race/'+raceId+'/'+waveId+'/'+$scope.page).success(function(data){      
          //$http.get('/api/race/'+raceId+'/'+waveId).success(function(data){      
          data.map(function(runner){
            runner.Model.gunTime = runner.Model.gunTime;
            runner.Model.chipTime = runner.Model.chipTime;
            runner.Model.firstName = runner.Model.firstName;
            runner.Model.lastName = runner.Model.lastName;
            runner.rank = $scope.runners.length+1;
            $scope.runners.push(runner);
          });
          if(data.length < 50){
            $scope.disableLoadMore = true;
          }
          $scope.page++;
        })
      }
    }      
  }

  loadRunners();
  
  $scope.loadMore = loadRunners;

  $scope.loadAll = function(){
    $scope.runners = [];
    $scope.shownAll = true;
    if($scope.sortByChipTime){
      $scope.page = -1;
      $http.get('/api/raceByChip/'+raceId+'/'+waveId+'/'+$scope.page).success(function(data){
        data.map(function(runner){
          runner.Model.gunTime = runner.Model.gunTime;
          runner.Model.chipTime = runner.Model.chipTime;
          runner.Model.firstName = runner.Model.firstName;
          runner.Model.lastName = runner.Model.lastName;
          runner.rank = $scope.runners.length+1;
          $scope.runners.push(runner);
        });
      })
    }
    else{      
      $scope.page = -1;
      $http.get('/api/race/'+raceId+'/'+waveId+'/'+$scope.page).success(function(data){      
        //$http.get('/api/race/'+raceId+'/'+waveId).success(function(data){      
        data.map(function(runner){
          runner.Model.gunTime = runner.Model.gunTime;
          runner.Model.chipTime = runner.Model.chipTime;
          runner.Model.firstName = runner.Model.firstName;
          runner.Model.lastName = runner.Model.lastName;
          runner.rank = $scope.runners.length+1;
          $scope.runners.push(runner);
        });        
      });
    }
  }

  $http.get('/api/race/'+raceId).success(function(data){
    data.raceDate = (new Date(data.raceDate)).toDateString();
    for (var i = 0; i < data.waves.length; i++){
      if (data.waves[i].waveId == waveId){
        $scope.wave = data.waves[i];
        break;
      }
    }

    $scope.race = data;

    if(!$scope.wave){
      $location.path("pageNotFound");
    }

  }).error(function(data){
    $location.path("pageNotFound");
  });

  $scope.sortByBib = function(runner){
    var bib = runner.Model.bibnumber;
    //var bibnumber = bib.slice(-4);
    var bibnumber = bib.replace(/\D/g,'');
    return (parseInt(bibnumber, 10));
  }
;});
app = angular.module('myruntime-admin', ['ngRoute', 'ui.bootstrap.datetimepicker']);

app.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/admin', {
        templateUrl: '/adminApp/partials/index.html',
        controller: 'indexCtrl'
      }).
      when('/admin/race/:raceId/', {
        templateUrl: '/adminApp/partials/race.html',
        controller: 'raceCtrl'
      }).
      when('/admin/races/', {
        templateUrl: '/adminApp/partials/races.html',
        controller: 'racesCtrl'
      }).
      when('/admin/races/new', {
        templateUrl: '/adminApp/partials/newrace.html',
        controller: 'newRaceCtrl'
      }).
      when('/admin/users', {
        templateUrl: '/adminApp/partials/users.html',
        controller: 'usersCtrl'
      }).
      when('/admin/postprocess',{
        templateUrl: '/adminApp/partials/postprocess.html',
        controller: 'postProcessCtrl'
      }).
      otherwise({
        redirectTo: '/admin'
      });
}]);

app.config(["$interpolateProvider", function($interpolateProvider){
  $interpolateProvider.startSymbol('[[');
  $interpolateProvider.endSymbol(']]');
}]);


app.directive('fileUpload', ["$parse", function ($parse) {
  return {
    scope: true,        //create a new scope
    link: function (scope, el, attrs) {
      el.bind('change', function (event) {
        var files = event.target.files;
        //iterate files since 'multiple' may be specified on the element
        for (var i = 0;i<files.length;i++) {
          //emit event upward
          scope.$emit("fileSelected", { file: files[i], model: attrs});
        }                                       
      });
    }
  };
}]);


app.directive('tableData', function(){
  return {
    restrict: "A",
    scope: {
      display: '=tableData'
    },
    template:'<tr><td class="{{display.Status}}">{{display.EPC}}</td><td class={{display.Status}}>{{display.Start}}</td>' +
    '<td class={{display.Status}} ng-repeat="lap in display.Laps"> {{lap.Time}}</td>'+
    '<td class={{display.Status}}> {{display.Finish}} </td>' +
    '<td class={{display.Status}}><select ng-model="timeDetection" ng-options="detections for detections in display.AllDetections"></select></td>'+
    '<td class={{display.Status}}> <button class="btn btn-primary" ng-click="updatestart(timeDetection)">Set Start</button> &nbsp;&nbsp;' +
    '<button class="btn btn-primary" ng-click="updatefinish(timeDetection)"> Set Finish </button> &nbsp;&nbsp;' +
    '<button class="btn btn-danger" ng-click="clearstart()">Clear Start</button> &nbsp;&nbsp;' +
    '<button class="btn btn-danger" ng-click="clearfinish()"> Clear Finish </button></td></tr>',    

    link: function($scope){
      $scope.updatestart = function(startTime){
        $scope.display.Start = startTime;
        $scope.checkStatus();
      }

      $scope.updatefinish = function(finishTime){
        $scope.display.Finish = finishTime; 
        $scope.checkStatus();
      }

      $scope.clearstart = function(){
        $scope.display.Start = null;
        $scope.checkStatus();
      }

      $scope.clearfinish = function(){
        $scope.display.Finish = null;
        $scope.checkStatus();
      }

      $scope.checkStatus = function(){
        if($scope.display.Finish == null){
          $scope.display.Status = "error";
        }
        else{
          if($scope.display.Start == null){
            $scope.display.Status = "warning";   
          }
          else{
            $scope.display.Status = "success";    
          }
        }
      }
    }
  }
});

var SecondsToTimeString = function(seconds) {
  if (!seconds) return "";
  var hours = parseInt(seconds / 3600) % 24;
  var minutes = parseInt(seconds / 60) % 60;
  var seconds = seconds % 60;
  var TimeStr = (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds);
  return TimeStr;
}

'use strict';

app.controller('appctrl', ['$rootScope','$route','$location', '$http', '$window', function($rootScope,$route,$location,$http, $window){  
  $http.get('/api/user/').success(function(data){  
    $rootScope.User = data;
  });

  $rootScope.$on('$viewContentLoaded', function(event) {
    $window._gaq.push(['_trackPageview', $location.path()]);
  });
}]);

'use strict';

app.controller('indexCtrl', ["$scope", "$http", function ($scope, $http){
   
}]);
'use strict';

app.controller('newRaceCtrl', ["$scope", "$http", "$location", function ($scope, $http, $location){
  $scope.newrace = {
    waves: []
  };
  $scope.files = [];

  $http.get('/api/races/locations').success(function(data){
    $scope.locations = data;
    $scope.newrace.raceAddress = data[0];
  });
  
  $scope.$on("fileSelected", function (event, args) {
    $scope.$apply(function () {            
      $scope.files.push(args.file);
    });
  });

  $scope.selectFromList = true;

  $scope.toggleSelectFromList = function(){
    $scope.selectFromList = !$scope.selectFromList;    
  };

  $scope.saveRace = function(){

    //Save location
    $http.post('/api/races/locations', {location: $scope.newrace.raceAddress}).success(function(){
    });

    //Save race
    $http({
      method: 'POST',
      url: "/api/admin/race",
      headers: { 'Content-Type': undefined },
      transformRequest: function (data) {
        var formData = new FormData();
        console.log(angular.toJson(data.files[0]));
        formData.append("race", angular.toJson(data.race));
        formData.append("racePhoto", data.files[0]);
        return formData;
      },
      data: { race: $scope.newrace, files: $scope.files }
    }).
    success(function (data, status, headers, config) {
      console.log(data);
      $location.path('admin/race/'+data.Model.raceId);
    }).
    error(function (data, status, headers, config) {
      alert("failed!");
    });
  }

  $scope.addwave = function(){
    $scope.newrace.waves.push({
      waveHash : Date.now(),
      waveName : "",
      waveDescription : "",
      waveDistance : 0,
      splits : [],
      addSplit : function(){
        this.splits.push({
          waveHash : this.waveHash,
          splitHash : Date.now(),
          splitDistance : 0,
          splitName : '',
          removeSplit : function(){
            for (var i = 0; i < $scope.newrace.waves.length; i++){
              if ($scope.newrace.waves[i].waveHash == this.waveHash){
                for (var j = 0; j < $scope.newrace.waves[i].splits.length; j++){
                  if ($scope.newrace.waves[i].splits[j].splitHash == this.splitHash){
                    $scope.newrace.waves[i].splits.splice(j,1);
                    break;
                  }
                }
                break;
              }
            }
          }
        });
      },
      removeWave : function(){
        for (var i = 0; i < $scope.newrace.waves.length; i++){
          if ($scope.newrace.waves[i].waveHash == this.waveHash){
            $scope.newrace.waves.splice(i,1);
            break;
          }
        }
      }
    });
  }
}]);

app.controller('postProcessCtrl', ["$scope", "$route", "$location", "$http", function($scope,$route,$location,$http){    
  
  $scope.files = [];
  $scope.waveName = "Enter Wave Name";

  $scope.initializeProcessing = function(){
    $scope.EPCs = [];
    $scope.AllDetections = [];
    $scope.Detections = [];  
    $scope.Start = [];
    $scope.Finish = [];
    $scope.Status = [];
    $scope.Laps = [];
    $scope.forDisplay = [];    
  }

  $scope.initializeProcessing();
  

  $scope.postprocessdata = {
    waveLaps: 1,
    waveStart: "06:00:00 AM"
  };  

  $scope.entryList = [];
  $scope.showProcessing = false;
  $scope.waveDetailsReady = false;
  $scope.entryListReady = false;
  $scope.disableUpload = true;
  $scope.disableProcess = true;
  $scope.hasLaps = false;

  var _laps = [];

  $scope.downloadResults = function(){    
    var rawCSV = "Bib,LastName,FirstName,MiddleName,Gender,Nationality,StartTime,FinishTime,Comment\r\n";

    /*
    for(var i = 0; i<$scope.EPCs.length; i++){
      if($scope.hasLaps){
        var lapsString = "";
        for(var j = 0; j < _laps.length; j++){
          if($scope.forDisplay[i].Laps[j]){
            lapsString += "," + $scope.forDisplay[i].Laps[j].Time;
          } 
          else{
            lapsString += ",";
          }          
        }          
        rawCSV+= $scope.forDisplay[i].EPC + "," + $scope.forDisplay[i].Start  + "," + $scope.forDisplay[i].Finish + lapsString + "," + $scope.forDisplay[i].Status+ "\r\n"
      }
      else{
        rawCSV+= $scope.forDisplay[i].EPC + "," + $scope.forDisplay[i].Start  + "," + $scope.forDisplay[i].Finish + "," + $scope.forDisplay[i].Status + "\r\n"
      }        
    }
    */

    for(var i = 0; i<$scope.forDisplay.length; i++){

      var match = _.findWhere($scope.entryList, {epc:$scope.forDisplay[i].EPC});
      if(match){
        rawCSV+= match.bib + "," + match.lastName + "," + match.firstName + ",," + match.gender + ",," + $scope.forDisplay[i].Start  + "," + $scope.forDisplay[i].Finish + "," + $scope.forDisplay[i].Status + "\r\n" 
      }

    }    
    
    var blob = new Blob([rawCSV], {type: "text/plain;charset=utf-8"});
    saveAs(blob, $scope.waveName + ".csv");
    
    alert("Successfully downloaded results!");    
  }

  $scope.$on("fileSelected", function (event, args) {
    $scope.$apply(function () {            

      var file = args.file;      
      if (args.model.fileType == "detections"){
        $scope.files.detections = file;
      }
      if (args.model.fileType == "entrylist"){
        $scope.files.entrylist = file;        
      }  
    });      
  });

  $scope.setWaveStart = function(){
    if($scope.postprocessdata.waveLaps <= 1){
      $scope.hasLaps = false;
    }
    else{
      $scope.hasLaps = true;
      _laps = new Array(parseInt($scope.postprocessdata.waveLaps - 1));
    }    

    if(!$scope.postprocessdata.date){
      alert('Invalid Start Date!');
      $scope.waveDetailsReady = false;
      return;
    }
    else{
      if(!$scope.postprocessdata.date.toDateString()){
        alert('Invalid Start Date!');
        $scope.waveDetailsReady = false;
        return;
      } 
    }

    if(!$scope.postprocessdata.waveStart){
      alert('Invalid Start Time!');
      $scope.waveDetailsReady = false;
      return;
    } 

    $scope.waveStart = new Date($scope.postprocessdata.date.toDateString() + " " + $scope.postprocessdata.waveStart);


    if($scope.waveStart == 'Invalid Date'){
      alert('Invalid Start Date!');
      $scope.waveDetailsReady = false;
    }

    else{
      alert('Success!');      
      $scope.waveDetailsReady = true;
      $scope.disableUpload = !($scope.waveDetailsReady && $scope.entryListReady);
    }    
  }

  $scope.processDetections = function(){
    $scope.waveStart = new Date($scope.postprocessdata.date.toDateString() + " " + $scope.postprocessdata.waveStart);
    $scope.Laps = $scope.Laps.map(function(lap, index){
      var lapEntries = [];
      for(var i = 0; i < _laps.length; i++){
        var LapEntry = new Object();
        LapEntry.Lap = i;
        LapEntry.Time = null;        
        lapEntries.push(LapEntry);
      }
      return lapEntries;
    })

    var _processData = function(something, item, i, callback){
      $scope.Detections[i] = $scope.Detections[i].filter(function(detection){
        var detectionDate = new Date($scope.postprocessdata.date.toDateString() + " " + detection);
        return( detectionDate >= $scope.waveStart)
      })   

      var sorted = $scope.Detections[i].sort(function(a,b){
        return (new Date($scope.postprocessdata.date.toDateString() + " " + a) - new Date($scope.postprocessdata.date.toDateString() + " " + b));
      })

      var lastDetectionTime = null;
      var lastDetectionDate = null;
      var lapsLength = 0;
      var hasStart = false;
      var firstLap = true;

      sorted.forEach(function(detection){

        //with laps
        if($scope.hasLaps){
          if(!$scope.Finish[i]){          
            if(!$scope.Start[i]){
              $scope.Start[i] = detection;
            }
            else{
              var detectionDate = new Date($scope.postprocessdata.date.toDateString() + " " + detection);
              var startDate = new Date($scope.postprocessdata.date.toDateString() + " " + $scope.Start[i]);
              
              if( ((detectionDate.getTime() - startDate.getTime())/1000) < 420){
                $scope.Start[i] = detection;
              }
              else{
                if(firstLap){
                  $scope.Laps[i] = $scope.Laps[i].map(function(lap){
                    if(lap.Lap == 0){
                      lap.Time = detection;
                    }
                    return lap;
                  })
                  lastDetectionTime = detection;
                  lastDetectionDate = new Date($scope.postprocessdata.date.toDateString() + " " + lastDetectionTime);
                  firstLap = false;
                  lapsLength = 0;
                }
                else{
                  //second ++ instance after start is found
                  if( ((detectionDate.getTime() - lastDetectionDate.getTime())/1000) < 420){

                    //refresh lap time;
                    $scope.Laps[i] = $scope.Laps[i].map(function(lap){
                      if(lap.Lap == lapsLength){
                        lap.Time = detection;
                      }
                      return lap;
                    })
                    lastDetectionTime = detection;
                    lastDetectionDate = new Date($scope.postprocessdata.date.toDateString() + " " + lastDetectionTime);
                  }

                  else{
                    lapsLength++;
                    if(lapsLength == _laps.length){
                      console.log(((detectionDate.getTime() - lastDetectionDate.getTime())/1000));
                      $scope.Finish[i] = detection;
                      if( ( (startDate.getTime() - $scope.waveStart.getTime()) / 1000) > 600 ){
                        $scope.Status[i] = 'warning';
                      }
                      else{
                        $scope.Status[i] = 'success';  
                      }
                    }
                    else{                      
                      $scope.Laps[i] = $scope.Laps[i].map(function(lap){
                        if(lap.Lap == lapsLength){
                          lap.Time = detection;
                        }
                        return lap;
                      })
                      lastDetectionTime = detection;
                      lastDetectionDate = new Date($scope.postprocessdata.date.toDateString() + " " + detection);
                    }
                  }
                }                
              }
            }
          }
          else{
            return;
          }
        }          

        //no laps
        else{
          if(!$scope.Finish[i]){          
            if(!$scope.Start[i]){
              $scope.Start[i] = detection;
            }
            else{
              var detectionDate = new Date($scope.postprocessdata.date.toDateString() + " " + detection);
              var startDate = new Date($scope.postprocessdata.date.toDateString() + " " + $scope.Start[i]);
              
              
              //8 minutes?
              if( ((detectionDate.getTime() - startDate.getTime())/1000) < 420){
                $scope.Start[i] = detection;
              }
              else{
                $scope.Finish[i] = detection;
                if( ( (startDate.getTime() - $scope.waveStart.getTime()) / 1000) > 600 ){
                  $scope.Status[i] = 'warning';
                }
                else{
                  $scope.Status[i] = 'success';  
                }
                
              }
            }            
          }
          else{
            return;
          }          
        }   
      })    
      callback(null);
    }

    function doSynchronousLoop(initialData, data, processData, done) {
      if (data.length > 0) {
        var loop = function(initialData, data, i, processData, done) {
          processData(initialData, data[i], i, function(processResult) {
            if (++i < data.length) {
              loop(processResult, data, i, processData, done);
            }
            else {
              done(processResult);
            }
          });
        };
        loop(initialData, data, 0, processData, done);
      } 
      else {
        done(initialData);
      }
    }

    var dummyArray = new Array($scope.EPCs.length);

    doSynchronousLoop([], dummyArray, _processData, function(result){
      $scope.forDisplay = [];
      $scope.showProcessing = true;
      for(var i = 0 ; i <$scope.EPCs.length; i++){
        var displayItem = new Object({
          EPC: $scope.EPCs[i],
          Detections: $scope.Detections[i],
          Start: $scope.Start[i],
          Finish: $scope.Finish[i],
          AllDetections: _.uniq($scope.AllDetections[i]),
          Status : $scope.Status[i],
          Laps: $scope.Laps[i]
        })       
        $scope.forDisplay.push(displayItem);
      }      
    });
  }

  $scope.uploadEntryList = function(){    
    if (!$scope.files.entrylist){
      alert('No file selected');
      return;
    }
    
    $scope.entryList = [];
    var file = $scope.files.entrylist;

    $http({
      method: 'POST',
      url: "/api/admin/uploadEntryListForPostProcess",
      headers: { 'Content-Type': undefined},
      transformRequest: function (data) {
        var formData = new FormData();
        formData.append("entrylist", data.files);
        return formData;
      },
      data: { files: file }
    }).
    success(function (data, status, headers, config){
      $scope.entryList = data;
      $scope.entryListReady = true;
      $scope.disableUpload = !($scope.entryListReady && $scope.waveDetailsReady);
      alert("uploaded successfully!");
    }).
    error(function (data, status, headers, config) {
      alert("Failed! Check data.");
      $scope.entryListReady = false;
      $scope.disableUpload = true;
    });
  }

  $scope.uploadDetections = function(){
    if (!$scope.files.detections){
      alert('No file selected');
      return;
    }
    var file = $scope.files.detections;
    $scope.initializeProcessing();

    $http({
        method: 'POST',
        url: "/api/admin/uploadDetectionsForPostProcess",
        headers: { 'Content-Type': undefined },
        transformRequest: function (data) {
            var formData = new FormData();
            formData.append("detections", data.files);
            return formData;
        },
        data: { files: file }
    }).
    success(function (data, status, headers, config) {      
      data.forEach(function(entry){
        if($scope.EPCs.indexOf(entry.EPC) == -1){
          $scope.EPCs.push(entry.EPC);
          
          var detection = new Array();
          var start = null;
          var finish = null;
          var laps = new Array();

          detection.push((new Date((entry.Detection-28800)*1000)).toLocaleTimeString());          
          $scope.Detections.push(detection); 
          $scope.Start.push(start);
          $scope.Finish.push(finish);
          $scope.Laps.push(laps);          
          $scope.AllDetections.push(detection);          
          $scope.Status.push('error');
        }
        else{
          var index = $scope.EPCs.indexOf(entry.EPC);    
          $scope.Detections[index].push((new Date((entry.Detection-28800)*1000)).toLocaleTimeString());          
          $scope.AllDetections[index].push((new Date((entry.Detection-28800)*1000)).toLocaleTimeString());          
        }
      });
      $scope.forDisplay = [];
      $scope.disableProcess = false;      
      alert("Success!");
      $scope.processDetections();            
    }).
    error(function (data, status, headers, config) {
        alert("Failed! Check data.");
    });
  }
}]);

'use strict';

var SecondsToTimeString = function(seconds) {
  if (!seconds) return "";
  var hours = parseInt(seconds / 3600) % 24;
  var minutes = parseInt(seconds / 60) % 60;
  var seconds = seconds % 60;
  var TimeStr = (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds);
  return TimeStr;
};

app.controller('raceCtrl', ["$scope", "$http", "$rootScope", "$routeParams", "$location", "$route", function ($scope, $http, $rootScope, $routeParams, $location, $route){
  
  var raceId = $routeParams.raceId

  $scope.editMode = false;
  $scope.selectFromList = true;

  $scope.SecondsToTimeString = SecondsToTimeString;

  $http.get('/api/admin/race/'+ raceId + '/bibCount').success(function(data){
    $scope.uploadedBibs = data.Count;
  });

  $http.get('/api/admin/race/'+ raceId + '/activatedBibCount').success(function(data){
    $scope.activatedBibs = data.Count; 
  });


  $http.get('/api/races/locations').success(function(data){
    $scope.locations = data;
  });

  $http.get('/api/admin/race/'+raceId).success(function(data){

    (data.Model.upcoming) ?  $scope.upcomingStatus = "Hide Race" : $scope.upcomingStatus = "Show Race";
    (data.Model.published) ? $scope.publishedStatus = "Hide Results" : $scope.publishedStatus = "Show Results";
    $scope.hideRankStatus = (data.Model.hideRank) ? "Show Rank" : "Hide Rank";
    
    data.Model.raceDate = (new Date(data.Model.raceDate)).toDateString();      
    data.totalRunners = 0;


    data.Model.waves = data.Model.waves.map(function(wave){
      var waveDate = new Date(wave.waveStart);
      wave.waveStartHolder = waveDate.toLocaleTimeString();
    
      if (wave.runnersText){
        var runners = JSON.parse(wave.runnersText);
        wave.totalRunners = runners.length;
        wave.runners = runners;
        data.totalRunners+=runners.length;
      }

      wave.addSplit = function(){        
        this.splits.push({           
          splitHash: Date.now(),
          splitDistance: 0,
          splitName : '',
          removeSplit : function(){           
            for (var i = 0; i < $scope.race.Model.waves.length; i++){
              if ($scope.race.Model.waves[i].waveId == wave.waveId){
                for (var j = 0; j < $scope.race.Model.waves[i].splits.length; j++){
                  //console.log($scope.race.Model.waves[i].splits[j]);
                  if ($scope.race.Model.waves[i].splits[j].splitHash == this.splitHash){
                    $scope.race.Model.waves[i].splits.splice(j,1);
                    break;
                  }
                }
                break;
              }
            }
          }
        });
      }

      wave.splits = wave.splits.map(function(split){
        split.removeSplit = function(){          
          for (var i = 0; i < $scope.race.Model.waves.length; i++){
            if ($scope.race.Model.waves[i].waveId == wave.waveId){
              for (var j = 0; j < $scope.race.Model.waves[i].splits.length; j++){
                if ($scope.race.Model.waves[i].splits[j].splitOrder == this.splitOrder){
                  $scope.race.Model.waves[i].splits.splice(j,1);
                  break;
                }
              }
              break;
            }
          }
        }
        return split;
      });


      wave.removeWave = function(){
        for (var i = 0; i < $scope.race.Model.waves.length; i++){
          if ($scope.race.Model.waves[i].waveId == this.waveId){
            $scope.race.Model.waves.splice(i,1);
            break;
          }
        }
      };

      wave.setStart = function(){
        var waveId = this.waveId;
        var start = this.waveStart;

        var tDate = new Date($scope.race.Model.raceDate + " " + this.waveStartHolder)
        if (tDate == "Invalid Date"){
          alert("Invalid date");
          return;
        }
        var raceDate = new Date($scope.race.Model.raceDate);
        var a = new Date($scope.race.Model.raceDate + " " + this.waveStartHolder);        
        this.waveStart = a;        
        $http.post('/api/admin/race/'+raceId+'/'+this.waveId+'/start', {
          waveStart: this.waveStart
        }).success(function(){
          alert('Successfully uploaded time')
          $route.reload();
        }).error(function(){
          alert('Failed to upload time');
        });
      };
    
      return wave;    
    });

    $scope.race = data;

    $scope.addwave = function(){
      $scope.race.Model.waves.push({
        waveHash : Date.now(),
        waveName : "",
        waveDescription : "",
        waveDistance : 0,
        splits       : [],
        addSplit     : function(){
          this.splits.push({
            waveHash  : this.waveHash,
            splitHash     : Date.now(),
            splitDistance : 0,
            splitName     : '',
            removeSplit : function(){
              for (var i = 0; i < $scope.race.Model.waves.length; i++){
                if ($scope.race.Model.waves[i].waveHash == this.waveHash){
                  for (var j = 0; j < $scope.race.Model.waves[i].splits.length; j++){
                    if ($scope.race.Model.waves[i].splits[j].splitHash == this.splitHash){
                      $scope.race.Model.waves[i].splits.splice(j,1);
                      break;
                    }
                  }
                  break;
                }
              }
            }
          });
        },
        removeWave : function(){
          for (var i = 0; i < $scope.race.Model.waves.length; i++){
            if ($scope.race.Model.waves[i].waveHash == this.waveHash){
              $scope.race.Model.waves.splice(i,1);
              break;
            }
          }
        }
      });
    };

    $scope.toggleWaveDetails = function(waveId){
      $('#waveDetails' + waveId).toggle();
    };
  });


  $scope.uploadRacePhoto = function(raceName){
    
    if(!$scope.files.racePhoto){
      alert("No file selected");
      return;
    }

    var file = $scope.files.racePhoto;
    
    $http({
      method: 'POST',
      url: '/api/admin/uploadRacePhoto/' + raceId,
      headers: { 'Content-Type': undefined },
      transformRequest: function (data){
        var formData = new FormData();
        formData.append("race", angular.toJson(data.race));
        formData.append("racePhoto", data.files);
        return formData;
      },
      data: {race :{"raceName": raceName}, files: file}
    }).success(function (data, status, headers, config) {        
      alert("Uploaded Photo");
      $route.reload();
    }).
    error(function (data, status, headers, config) {
        alert("Upload failed!");
    });
  };

  $scope.uploadResults = function(waveId){
    //Check if file exists
    var found = false;
    var index = -1;
    for (var i = 0; i < $scope.files.results.length; i++){
      if ($scope.files.results[i].waveId == waveId){
        found = true;
        index = i;
        break;
      }
    }
    if (!found){
      alert('No file selected');
      return;
    }

    var file = $scope.files.results[index].file;

    $http({
      method: 'POST',
      url: "/api/admin/race/"+raceId+"/runners",
      headers: { 'Content-Type': undefined },
      transformRequest: function (data) {
          var formData = new FormData();
          formData.append("wave", angular.toJson(data.race));
          formData.append("runners", data.files);
          return formData;
      },
      data: { race: {"waveId":waveId}, files: file }
    }).
    success(function (data, status, headers, config) {  
        alert("Successfully uploaded results");
        $route.reload();
    }).
    error(function (data, status, headers, config) {
        alert("failed!");
    });
  };

  $scope.deleteRace = function(){
    var confirmDelete = confirm('Are you absolutely sure you want to delete race?');
    if(confirmDelete){
      $http.delete('/api/admin/race/'+raceId).success(function(){
        $location.path('races');  
      });      
    }    
  };

  $scope.saveAllDetails = function(raceDetails){
    $http({
      method: 'POST',
      url: "/api/admin/updateRaceDetails",
      data: {race: raceDetails}
    }).success(function(data, status, headers, config){
      alert("Successfully saved race details!")
      $route.reload();
    }).error(function (data, status, headers, config){
      alert('Failed to save wave details.');
    });
  };

  $scope.saveWaveDetails = function(waveDetails, raceId){
    var detailsToUpdate = {
      raceId: raceId,
      waveDetails: waveDetails
    }
    
    $http({
      method: 'POST',
      url: "/api/admin/updateWaveDetails",
      data: detailsToUpdate
    }).success(function(data, status, headers, config){
      alert("Successfully saved wave details!")
      $route.reload();
    }).error(function (data, status, headers, config){
      alert('Failed to save wave details.');
    });
  };

  $scope.files = {
    bibs : null,
    results : []
  };

  $scope.$on("fileSelected", function (event, args) {    
    $scope.$apply(function () {            
      var file = args.file;
      if (args.model.fileType == "bibs"){
        $scope.files.bibs = file;
      }
      if (args.model.fileType == "racePhoto"){
        $scope.files.racePhoto = file;
      }
      if (args.model.fileType == "resultFile"){
        for (var i = 0; i < $scope.files.results.length; i++){
          if ($scope.files.results[i].waveId == args.model.waveAttr){
            $scope.files.results.splice(i,1);
            break;
          }
        }
        $scope.files.results.push({
          waveId : args.model.waveAttr,
          file   : file
        });
      }
    });
  });
  
  $scope.uploadBibs = function(){
    if (!$scope.files.bibs){
      alert('No file selected');
      return;
    }
    var file = $scope.files.bibs;
    $http({
        method: 'POST',
        url: "/api/admin/race/"+raceId+"/bibs",
        headers: { 'Content-Type': undefined },
        transformRequest: function (data) {
            var formData = new FormData();
            //console.log(angular.toJson(data.files[0]));
            formData.append("race", angular.toJson(data.race));
            formData.append("bibs", data.files);
            return formData;
        },
        data: { race: {}, files: file }
    }).
    success(function (data, status, headers, config) {
        //console.log(data);
        alert("Successfully uploaded bibs");
    }).
    error(function (data, status, headers, config) {
        alert("failed!");
    });
  };

  $scope.downloadCodes = function(){
    $http({
        method: 'POST',
        url: "/api/admin/downloadCodes/"+raceId,
    }).
    success(function (data, status, headers, config) {
        var blob = new Blob([data], {type: "text/plain;charset=utf-8"});
        saveAs(blob, "Codes.csv");
        alert("Successfully downloaded activation codes!");
    }).
    error(function (data, status, headers, config) {
        alert("failed!");
    });
  };
  
  $scope.togglePublished = function(){
    $http.post('/api/admin/race/'+raceId+'/published', {}).success(function(data){      
      (data.published) ? $scope.publishedStatus = "Hide Results" : $scope.publishedStatus = "Show Results";    
      alert('Successfully changed published status')
    }).error(function(){
      alert('Failed to change published status');
    });
  };

  $scope.toggleUpcoming = function(){    
    $http.post('/api/admin/race/' + raceId + '/upcoming', {}).success(function(data){
      (data.upcoming) ?  $scope.upcomingStatus = "Hide Race" : $scope.upcomingStatus = "Show Race";
      alert('Successfully changed upcoming status');
    }).error(function(){
      alert('Failed to change upcoming status');
    });
  };

  $scope.toggleHideRank = function(){
    $http.post('/api/admin/race/' + raceId + '/hideRank', {}).success(function(data){
      $scope.hideRankStatus = (data.hideRank) ? "Show Rank" : "Hide Rank";      
      alert('Successfully changed hide rank status');
    }).error(function(){
      alert('Failed to change hide rank status');
    });
  };

  $scope.updateRaceAddress = function(newAddress){
    if(newAddress === '' || newAddress === null){
      alert('Please specify race location');
    }
    else{

      //New Location Option
      if(!$scope.selectFromList){

        //add new location to db first.
        $http.post('/api/races/locations', {location: newAddress}).success(function(data){
          //then save to race
          $http.post('/api/admin/race/' + raceId + '/updateRaceAddress', {"raceAddress": newAddress}).success(function(){
            alert('Successfully updated race location');
            $route.reload();
          }).error(function(){
            alert('Failed to change race location');
          });
        }).error(function(){
          alert('Failed to change race location');
        });
      }

      //Location from dropdown
      else{
        $http.post('/api/admin/race/' + raceId + '/updateRaceAddress', {"raceAddress": newAddress}).success(function(data){
          alert('Successfully updated race location');
          $route.reload();
        }).error(function(){
          alert('Failed to change race location');
        });
      }
    }      
  };

  $scope.setEditMode = function(){
    $scope.newAddress = $scope.race.Model.raceAddress;
    $scope.editMode = true;
  };

  $scope.resetEditMode = function(){
    $scope.selectFromList = true;
    $scope.editMode = false;
  };

  $scope.hideSelectList = function(){
    $scope.newAddress = '';
    $scope.selectFromList = false;
  }

}]);

'use strict';

app.controller('racesCtrl', ["$scope", "$http", "$rootScope", function ($scope, $http, $rootScope){
  $http.get('/api/admin/race').success(function(data){
    $scope.races = data.map(function(race){
      race.Model.raceDate = (new Date(race.Model.raceDate)).toDateString();      
      return race;
    });
  });

  $scope.createRunnerText = function(){
    $http.post('/api/admin/rebuildWaveOrder/',{}).success(function(data){
      alert("Created Runner Text!");
    });
  };

}]);
'use strict';

app.controller('usersCtrl', ["$scope", "$http", function ($scope, $http){
  
  $scope.data = {
    inputEmail: "",
    inputPassword: "",
    validatePassword: ""
  };

  $scope.validateAndSubmit = function(data){    
    if(data.inputEmail == null || data.inputEmail == '' || data.inputEmail == undefined){
      alert("Enter valid Email");
    }
    else{
      if(data.inputPassword == '' || data.validatePassword == '' || data.validatePassword == undefined || data.inputPassword == undefined || data.inputPassword != data.validatePassword){
        alert("Fix password!");
      }
      else{
        $http({
          method: 'POST',
          url: '/api/admin/createNewAdminUser',
          data:{ username: data.inputEmail, password: data.inputPassword, firstName: data.firstName, lastName: data.lastName }
        }).success(function (data, status, headers, config){
          alert("Created New Admin!");
          $route.reload();
        }).error(function (data, status, headers, config){
          alert("Error occurred!");
        });     
      }      
    }
  };

}]);
app = angular.module('myruntime', ['ngRoute', 'highcharts-ng', 'facebook']);

app.config(['FacebookProvider', function(FacebookProvider){
    var myAppId = "353126178090062";
    FacebookProvider.init(myAppId);
  }
]);

app.config(function($interpolateProvider){
  $interpolateProvider.startSymbol('[[');
  $interpolateProvider.endSymbol(']]');
});

app.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/', {
        templateUrl: '/mainApp/partials/index.html',
        controller: 'indexCtrl'
      }).
      when('/runner/:raceId/:bibnumber', {
        templateUrl: '/mainApp/partials/runner.html',
        controller: 'runnerCtrl'
      }).
      when('/races', {
        templateUrl: '/mainApp/partials/races.html',
        controller: 'racesCtrl'
      }).
      when('/connect',{
        templateUrl: '/mainApp/partials/connect.html',
        controller: 'connectCtrl'
      }).
      when('/race/:raceId/:waveId', {
        templateUrl: '/mainApp/partials/wave.html',
        controller: 'waveCtrl'
      }).
      when('/race/:raceNick', {
        templateUrl: '/mainApp/partials/race.html',
        controller: 'raceCtrl'
      }).
      when('/contact', {
        templateUrl: '/mainApp/partials/contact.html',
        controller: 'contactCtrl'
      }).   
      when('/faqs', {
        templateUrl: '/mainApp/partials/faqs.html'
      }).
      when('/profile/:userId', {
        templateUrl: '/mainApp/partials/profile.html',
        controller: 'profileCtrl'
      }).
      when('/me',{
        templateUrl: '/mainApp/partials/profile.html',
        controller: 'profileCtrl'
      }).
      when('/accountSettings',{
        templateUrl: '/mainApp/partials/accountSettings.html',
        controller: 'accountSettingsCtrl'
      }).
      when('/login',{
        templateUrl: '/mainApp/partials/login.html',
        controller: 'loginCtrl'
      }).
      when('/signup',{
        templateUrl: 'mainApp/partials/register.html',
        controller: 'registerCtrl'
      }).
      when('/adminlogin',{
        templateUrl: '/mainApp/partials/adminlogin.html',
        controller: 'adminLoginCtrl'
      }).
      when('/invalidAccountActivation', {
        templateUrl: '/mainApp/partials/accountActivationInvalid.html',
      }).
      when('/successAccountActivation', {
        templateUrl: '/mainApp/partials/accountActivationSuccess.html',
      }).
      when('/pageNotFound', {
        templateUrl: '/mainApp/partials/pageNotFound.html',
        controller: 'pageNotFoundCtrl'
      }).
      otherwise({
        redirectTo: '/'
      });
}]);

app.run(function($rootScope){
  $rootScope.hideRankRaceIds = [83, 96, 123, 124, 125, 127, 137, 134, 135, 144, 159, 160, 161, 162, 180];
  $rootScope.hideBibRaceIds = [125];
  $rootScope.hideOfficialTimeRaceIds = [123, 125, 137, 134, 135, 149];
  $rootScope.hideChipTimeRaceIds = [134, 135];
  $rootScope.hideFriendsRaceIds = [134, 135];
  $rootScope.sortByChipTimeRaceIds = [149];
  $rootScope.hideNamesIds = [160];
});
   
app.filter('toProper', function(){
  return function(value){
    if (!value) return '';
    else{
      var properCase = function(val){
        return (val.length >= 1)?(val[0].toUpperCase()+ val.substr(1).toLowerCase()):('');
      }
      return (value.length>0)?(value.split(' ').filter(function(a){return (a.length >=1)}).map(properCase).join(' ')):('');
    }
  }      
});

app.filter('secondsToTimeString', function(){
  return function(seconds){
    var hours = parseInt(seconds / 3600) % 24;
    var minutes = parseInt(seconds / 60) % 60;
    var seconds = seconds % 60;
    var TimeStr = (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds);
    return TimeStr;
  }
});

app.filter('toGenderString', function(){
  return function(gender){
    return gender == 'F' ? 'Female' : 'Male';
  }
});

app.filter('offset12Hours', function(){
  return function(time){
    if(time){
      if(time.toString().indexOf('AM') != -1){
        return time.toString().replace('AM', 'PM');
      }
      else{
        return time.toString().replace('PM', 'AM');
      }
    }
    else{
      return;
    } 
  }
});

app.filter('changeTimeFormatToHHMMSS', function(){
  return function(time){
    if(time){

      //Remove AM part
      var AMindex = time.indexOf(" AM");
      var removedAM = time.substring(0, AMindex);
      var returnTime;

      if(removedAM.indexOf("12:") === 0){
        returnTime = removedAM.replace("12:", "00:");
      }
      else{
        returnTime = removedAM;
      }

      return returnTime;
      
    }
    else{
      return ;
    }
  }
});

app.filter('changeTimeFormatFromISODateToString', function(){
  return function(time){
    if(time){
      return moment(time).format('MMMM D, YYYY');
    }
    else{
      return ;
    }
  }
});

app.filter('genderToImage', function(){
  return function(gender){
    return gender == 'F' ? '/images/female.png' : '/images/male.png';
  }
});

app.directive('myfooter', function(){
  return {
    restrict: "E",
    templateUrl: '/mainApp/partials/footerTemplate.html'
  }
});

app.directive("hover", function(){
  return function(scope, element, attrs){
    element.bind("mouseenter", function(){
      scope.$apply(attrs.hover);
    })  
  }
});

app.directive("leave", function(){
  return function(scope, element, attrs){
    element.bind("mouseleave", function(){
      scope.$apply(attrs.leave);
    });
  }
});

app.directive('ngBlur', function () {
  return function (scope, elem, attrs) {
    elem.bind('blur', function () {
      scope.$apply(attrs.ngBlur);
    });
  };
});

/*
.filter('object2Array', function() {
  return function(input) {
    var out = []; 
    for(var i in input){
      out.push(input[i]);
    }
    return out;
  };
})
*/

app.directive('pwCheck', function(){
  return {
    require: 'ngModel',
    link: function(scope, elem, attrs, ctrl){
      var firstPassword = '#' + attrs.pwCheck;
      elem.append(firstPassword).on('keyup', function(){
        scope.$apply(function(){
          var v = elem.val() === $(firstPassword).val();
          ctrl.$setValidity('pwmatch', v);
        });
      });      
    }
  };
});

'use strict';

app.controller('waveCtrl', function ($rootScope, $scope, $http, $routeParams, $location){
  var raceId = $routeParams.raceId;
  var waveId = $routeParams.waveId;
  $scope.shownAll = false;
  $scope.disableLoadMore = false;
  //$scope.store  = storeService.store;

  if( _.contains($rootScope.hideRankRaceIds, parseInt(raceId,10)) ){
    $scope.hideRank = true;
  }
  else{
    $scope.hideRank = false;
  }

  if( _.contains($rootScope.hideBibRaceIds, parseInt(raceId,10)) ){
    $scope.hideBib = true;
  }
  else{
    $scope.hideBib = false;
  }

  if( _.contains($rootScope.hideOfficialTimeRaceIds, parseInt(raceId,10)) ){
    $scope.hideOfficialTime = true;
  }
  else{
    $scope.hideOfficialTime = false;
  }

  if(_.contains($rootScope.sortByChipTimeRaceIds, parseInt(raceId, 10)) ){
    $scope.sortByChipTime = true;
  }
  else{
   $scope.sortByChipTime = false; 
  }

  if( _.contains($rootScope.hideNamesIds, parseInt(raceId, 10)) ){
    $scope.hideName = true;
  }
  else{
    $scope.hideName = false;
  }

  $scope.page = 0;
  $scope.runners = [];

  var loadRunners = function(){  
    if($scope.hideRank){
      $http.get('/api/race/'+raceId+'/'+waveId).success(function(data){      
        data.map(function(runner){
          runner.Model.gunTime = runner.Model.gunTime;
          runner.Model.chipTime = runner.Model.chipTime;
          runner.Model.firstName = runner.Model.firstName;
          runner.Model.lastName = runner.Model.lastName;
          runner.rank = $scope.runners.length+1;
          $scope.runners.push(runner);
        });
      });
    }

    else{
      if($scope.sortByChipTime){
        $http.get('/api/raceByChip/'+raceId+'/'+waveId+'/'+$scope.page).success(function(data){      
          //$http.get('/api/race/'+raceId+'/'+waveId).success(function(data){      
          data.map(function(runner){
            runner.Model.gunTime = runner.Model.gunTime;
            runner.Model.chipTime = runner.Model.chipTime;
            runner.Model.firstName = runner.Model.firstName;
            runner.Model.lastName = runner.Model.lastName;
            runner.rank = $scope.runners.length+1;
            $scope.runners.push(runner);
          });
          $scope.page++;
        })
      }
      else{
        $http.get('/api/race/'+raceId+'/'+waveId+'/'+$scope.page).success(function(data){      
          //$http.get('/api/race/'+raceId+'/'+waveId).success(function(data){      
          data.map(function(runner){
            runner.Model.gunTime = runner.Model.gunTime;
            runner.Model.chipTime = runner.Model.chipTime;
            runner.Model.firstName = runner.Model.firstName;
            runner.Model.lastName = runner.Model.lastName;
            runner.rank = $scope.runners.length+1;
            $scope.runners.push(runner);
          });
          if(data.length < 50){
            $scope.disableLoadMore = true;
          }
          $scope.page++;
        })
      }
    }      
  }

  loadRunners();
  
  $scope.loadMore = loadRunners;

  $scope.loadAll = function(){
    $scope.runners = [];
    $scope.shownAll = true;
    if($scope.sortByChipTime){
      $scope.page = -1;
      $http.get('/api/raceByChip/'+raceId+'/'+waveId+'/'+$scope.page).success(function(data){
        data.map(function(runner){
          runner.Model.gunTime = runner.Model.gunTime;
          runner.Model.chipTime = runner.Model.chipTime;
          runner.Model.firstName = runner.Model.firstName;
          runner.Model.lastName = runner.Model.lastName;
          runner.rank = $scope.runners.length+1;
          $scope.runners.push(runner);
        });
      })
    }
    else{      
      $scope.page = -1;
      $http.get('/api/race/'+raceId+'/'+waveId+'/'+$scope.page).success(function(data){      
        //$http.get('/api/race/'+raceId+'/'+waveId).success(function(data){      
        data.map(function(runner){
          runner.Model.gunTime = runner.Model.gunTime;
          runner.Model.chipTime = runner.Model.chipTime;
          runner.Model.firstName = runner.Model.firstName;
          runner.Model.lastName = runner.Model.lastName;
          runner.rank = $scope.runners.length+1;
          $scope.runners.push(runner);
        });        
      });
    }
  }

  $http.get('/api/race/'+raceId).success(function(data){
    data.raceDate = (new Date(data.raceDate)).toDateString();
    for (var i = 0; i < data.waves.length; i++){
      if (data.waves[i].waveId == waveId){
        $scope.wave = data.waves[i];
        break;
      }
    }

    $scope.race = data;

    if(!$scope.wave){
      $location.path("pageNotFound");
    }

  }).error(function(data){
    $location.path("pageNotFound");
  });

  $scope.sortByBib = function(runner){
    var bib = runner.Model.bibnumber;
    //var bibnumber = bib.slice(-4);
    var bibnumber = bib.replace(/\D/g,'');
    return (parseInt(bibnumber, 10));
  }
;});
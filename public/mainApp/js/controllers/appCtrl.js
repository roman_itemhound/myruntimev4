'use strict';

app.controller('appctrl', ['$rootScope','$route','$location', '$http', '$window', 'Facebook', function($rootScope,$route,$location,$http,$window,Facebook){     
  
  $http.get('/api/user/').success(function(data){  
    $rootScope.User = data;
    if(data){
      $rootScope.hasUser = true;
    }
    else{
     $rootScope.hasUser = false; 
    }

    $rootScope.$on('$viewContentLoaded', function(event) {
      $window._gaq.push(['_trackPageview', $location.path()]);
    });


    var getLoginStatus = function(){      
      var response = Facebook.getLoginStatus(function(response){
        return (response.status === 'connected' ? true : false);        
      });
      return response;
    };

    $rootScope.logoutFacebook = function(){      
      if(getLoginStatus() && $rootScope.User.Model.faceBookId){
        Facebook.logout(function(response) {
          $window.location.href = '/logout'  
        });
      }
      else{
        $window.location.href = '/logout'  
      }      
    };
  });

  $rootScope.openInstagramIframe = function(targetUrl){
    var dualScreenLeft = $window.screenLeft != undefined ? $window.screenLeft : screen.left;
    var dualScreenTop = $window.screenTop != undefined ? $window.screenTop : screen.top;
    var width  = 600,
        height = 600,
        left   = (($(window).width()  - width)  / 2 ) + dualScreenLeft,
        top    = (($(window).height() - height) / 2 ) + dualScreenTop,
        url    = this.href,
        opts   = 'status=1' + ',width='  + width  + ',height=' + height + ',top=' + top + ',left=' + left;

    $window.open(targetUrl, 'Instagram', opts); 
    return false;    
  };

}]);

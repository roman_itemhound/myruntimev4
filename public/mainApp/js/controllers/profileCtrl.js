'use strict';

var isNumber = function(n){
  return !isNaN(parseFloat(n)) && isFinite(n);
};

var SecondsToTimeString = function(seconds){  
  var hours = parseInt(seconds / 3600) % 24;
  var minutes = parseInt(seconds / 60) % 60;
  var seconds = seconds % 60;
  var TimeStr = (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds);
  return TimeStr;  
};

app.controller('profileCtrl', function ($scope, $http, $routeParams, $route, $location, $window, Facebook){
  $scope.totalDistance = 0;  
  $scope.profilePic = '/images/240px-square.png';
  $scope.profileOwner = '';
  $scope.hidePublic = true;
  $scope.hideInputWeight = true;  
  $scope.caloriesBurned = "0.00 cals"
  $scope.totalMinutes = 0;  

  $scope.$watch(
    function() {
      return Facebook.isReady();
    },
    function(newVal) {
      if (newVal)
        $scope.facebookReady = true;
    }
  );

  $scope.hoverFunction = function(socialDivId){
    if($location.path() == '/me')
    { 
      $('#' + socialDivId).css('opacity',1);
    }
  }


  $scope.leaveFunction = function(socialDivId){    
    $('#' + socialDivId).css('opacity',0);
  }

  $scope.shareFB = function(result){
    var obj = {
      method: 'feed',    
      link: $window.location.href,
      picture: 'http://myrunti.me/images/240px-square.png',
      name:  result.Model.firstName + " " + result.Model.lastName + "'s Run Time via MyRunTime" ,
      caption:  result.Model.raceId.raceName ,
      description: result.Model.firstName + " "+ result.Model.lastName +" finished " + result.Model.raceId.raceName + " in " + SecondsToTimeString(result.Model.gunTime)
    };

    Facebook.ui(obj, null);
  }

  $scope.showOverlayForm = function(){
    $scope.hideInputWeight = false;
  };

  $scope.hideOverlayForm = function(){
    $scope.hideInputWeight = true;
  };

  $scope.validateInput = function(){ 
    if(!isNumber($scope.inputWeight)){
      $scope.invalidInput = true;
    }
    else{
      $http({
        method: 'POST',
        url: '/api/submitWeight',
        data:{ weight: $scope.inputWeight, userId: $scope.user.Model._id}
      }).success(function (data, status, headers, config){
        $scope.invalidInput = false;
        $scope.hideInputWeight = true;
        $scope.weightInKg = $scope.inputWeight / 2.2;
        $scope.totalCalories = ($scope.totalMinutes*7.0*$scope.weightInKg).toFixed(2);
        $scope.caloriesBurned = $scope.totalCalories + " cals";
      });
    }    
  }


  $scope.disconnectUpcoming = function(race){
    var r = confirm("This will disconnect this bib from your MyRunTime account. Do you want to continue?");
    if(r){
      $http({
        method:'POST',
        url:'/api/disconnectUpcoming',
        data: {bibnumber: race.bibDetails.Model.bibnumber, raceId: race.bibDetails.Model.raceId}
      }).success(function(data, status, headers, config){
        $route.reload();
      })
    }
  }

  
  $scope.disconnectBib = function(result){
    var r = confirm("This will disconnect this bib from your MyRunTime account. Do you want to continue?");
    if(r){
      $http({
        method:'POST',
        url:'/api/disconnectWithResult',
        data: {bibnumber: result.Model.bibnumber, raceId: result.Model.raceId._id}
      }).success(function(data, status, headers, config){
        $route.reload();
      })
    }      
  }


  if($location.path().indexOf("/me") == 0){
    $http.get('/api/user/').success(function(data){  
      if(data){
        $scope.dashboardColumnClass = "col-md-4";
        $scope.hidePublic = false;
        $scope.profileOwner = 'My';
        $http.get('/api/user/' + data.userId).success(function(user){
          $scope.user = user;
          $scope.inputWeight = user.Model.weight;
          $scope.weightInKg = user.Model.weight / 2.2;
          if($scope.user.Model.faceBookId){
            $http.get('http://graph.facebook.com/v2.3/' + $scope.user.Model.faceBookId + '/picture?redirect=0&height=240&type=normal&width=240').success(function(data){
              $scope.profilePic = data.data.url;
            });
          }
            
          $http({
            method: 'POST',
            url: '/api/user/getFriendsDetails',
            data:{ friendIds: $scope.user.Model.friends}
          }).success(function (data, status, headers, config){
            $scope.userFriends = data;
          })
          $('#friendsListModal').modal();
        });        

        $http.get('/api/results/' + data.userId).success(function(data){
          $scope.results = data;
          $scope.results.forEach(function(result){ 
            $scope.totalDistance += result.Wave.waveDistance;
            $scope.totalMinutes += (result.Model.chipTime)/3600;
          });

          $scope.totalCalories = ($scope.totalMinutes*7.0*$scope.weightInKg).toFixed(2);
          $scope.caloriesBurned = $scope.totalCalories + " cals";
        })

        $http.get('/api/bib/' + data.userId).success(function(data){
          $scope.upcomingRaces = data;    
          //console.log($scope.upcomingRaces);
        })
      }
      else{
        $location.path('login');
      }
    });
  }
  else{    
    $scope.dashboardColumnClass = "col-md-6";
    $http.get('/api/user/' + $routeParams.userId).success(function(data){  
      $scope.user = data;
      $scope.profileOwner = data.Model.firstName +"'s ";
      $http.get('http://graph.facebook.com/v2.3/' + $scope.user.Model.faceBookId + '/picture?redirect=0&height=240&type=normal&width=240').success(function(data){
        $scope.profilePic = data.data.url;
      });
      $http({
        method: 'POST',
        url: '/api/user/getFriendsDetails',
        data:{ friendIds: $scope.user.Model.friends}
      }).success(function (data, status, headers, config){
        $scope.userFriends = data;
      });

      $('#friendsListModal').modal();
    });

    $http.get('/api/results/' + $routeParams.userId).success(function(data){
      $scope.results = data;
      $scope.results.forEach(function(result){ 
        $scope.totalDistance += result.Wave.waveDistance;
      });
    })

    $http.get('/api/bib/' + $routeParams.userId).success(function(data){
      $scope.upcomingRaces = data;
      //console.log($scope.upcomingRaces);
    })
  }  
});

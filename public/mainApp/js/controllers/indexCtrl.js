'use strict';

app.controller('indexCtrl', function ($scope, $http){      
  //$scope.store  = storeService.store;
  $http.get('/api/races/available/0').success(function(data){    
    data =data.map(function(a){
      a.Model.raceDate = (new Date(a.Model.raceDate));
      return a;
    })
    data.splice(5);
    $scope.races = data;
  });
  $http.get('/api/races/upcoming/0').success(function(data){
    data =data.map(function(a){
      a.Model.raceDate = (new Date(a.Model.raceDate))
      return a;
    })
    data.splice(5);
    $scope.upcomingRaces = data;
  });

  $scope.segmentMarker = true;
  $scope.autoOpen = true;

  $scope.openSegment = function(){
    $('#sidelikesegment').toggle(200);
    if($scope.segmentMarker){      
      $('#floatinglikebutton').animate({"right":"+=385"}, 200); 
      $scope.segmentMarker = !$scope.segmentMarker;
    }
    else{      
      $('#floatinglikebutton').animate({"right":"-=385"}, 200);  
      $scope.segmentMarker = !$scope.segmentMarker;
    }
  }

  $scope.closeSegment = function(){
    $('#sidelikesegment').toggle(200);
    if( $scope.segmentMarker){      
        $('#floatinglikebutton').animate({"right":"+=385"}, 200); 
         $scope.segmentMarker = ! $scope.segmentMarker;
    }
    else{      
        $('#floatinglikebutton').animate({"right":"-=385"}, 200);  
         $scope.segmentMarker = !$scope.segmentMarker;
    }
  }
;});
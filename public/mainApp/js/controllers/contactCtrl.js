'use strict';

app.controller('contactCtrl', function ($scope, $http, $routeParams, $route){

  $http.get('/api/allRaces').success(function(data){
    $scope.races = data.map(function(datum){ return datum.Model });
    $scope.eventName = $scope.races[0];
  });

  $scope.inquiries = [
    {name:'Missing Time', key: 0},
    {name:'Booking Inquiry', key: 1},
    {name:'Incorrect Name', key: 2},
    {name:'Activation Problems', key: 3},
    {name:'Others', key: 4}
  ];

  $scope.inquiry = $scope.inquiries[0];
  $scope.missingTimeInfo = true;
  $scope.showEvent = true;
  $scope.showBib = true;
  $scope.wrongNameInfo = false;
  $scope.activationInfo = false;
  $scope.contactForm = {};

  $scope.changeInquiry = function(){
    switch($scope.inquiry.key){
      case 0:
        //missing time
        $scope.missingTimeInfo = true;
        $scope.showEvent = true;
        $scope.showBib = true;
        $scope.wrongNameInfo = false;
        $scope.activationInfo = false;
        break;
      case 1:        
        //race organizer inquiry
        $scope.missingTimeInfo = false;
        $scope.showEvent = false;
        $scope.showBib = false;
        $scope.wrongNameInfo = false;
        $scope.activationInfo = false;
        break;
      case 2:
        //incorrect name
        $scope.missingTimeInfo = false;
        $scope.showEvent = true;
        $scope.showBib = true;
        $scope.wrongNameInfo = true;
        $scope.activationInfo = false;
        break;
      case 3:
        //activation problems
        $scope.missingTimeInfo = false;
        $scope.showEvent = true;
        $scope.showBib = true;
        $scope.wrongNameInfo = false;
        $scope.activationInfo = true;
        break;
      case 4:
        //others
        $scope.missingTimeInfo = false;
        $scope.showEvent = false;
        $scope.showBib = false;
        $scope.wrongNameInfo = false;
        $scope.activationInfo = false;
        break;
      default:
        //default
        $scope.missingTimeInfo = false;
        $scope.showEvent = false;
        $scope.showBib = false;
        $scope.wrongNameInfo = false;
        $scope.activationInfo = false;
        break;
    }
  }

  $scope.submitForm = function(){
    var emailBody = "";
    var dataToSend = {};

    if( !($scope.contactForm.name) || !($scope.contactForm.email)){
      alert("Name and Email are required fields!");
      return;
    }
    else{

      dataToSend.inquiry = $scope.inquiry.name;
      dataToSend.name = $scope.contactForm.name;
      dataToSend.email = $scope.contactForm.email;
      dataToSend.eventName = $scope.eventName.raceName;

      if($scope.inquiry.key == 0){
        //missing time
        emailBody = "Details: " + " \r\n " + "Bib Number: " + $scope.contactForm.bib + " \r\n " + "Estimated Finish: " + $scope.contactForm.estimateFinish + " \r\n " + "Finish Buddies: " + $scope.contactForm.runningBuddies + "\r\n ";
      }
      else if($scope.inquiry.key == 2){
        //incorect name
        emailBody = "Details: " + " \r\n " + "Bib Number/s: " + $scope.contactForm.bib + " \r\n " + "Correct Name: " + $scope.contactForm.correctName + "\r\n ";

      }
      else if($scope.inquiry.key == 3){
        //activation problems
        emailBody = "Details: " + " \r\n " + "Bib Number/s: " + $scope.contactForm.bib + " \r\n " + "Activation Code: " + $scope.contactForm.activationCode + "\r\n " + "Browser used: " + $scope.contactForm.browser + "\r\n " + "Error message: " + $scope.contactForm.errorMessage + " \r\n "; 
      }

      if($scope.contactForm.feedback) emailBody += $scope.contactForm.feedback;
      dataToSend.feedback = emailBody;

      $http({
        method: 'POST',
        url: '/contact',
        data: dataToSend
      }).success(function (data, status, headers, config){
        alert("Form Submitted!");
        $route.reload();
      }).error(function(data, status, headers, config){
        alert("Form Submission Failed. Try Again");
      })      
    }   
  }
});
app.controller('runnerCtrl', function ($rootScope, $scope, $http, $routeParams, $window, Facebook){
  var raceId = $routeParams.raceId;
  var bibnumber = $routeParams.bibnumber;
  var waveId;
  var race;

  $scope.disableSubmit;

  if( _.contains($rootScope.hideRankRaceIds, parseInt(raceId,10)) ){
    $scope.hideRank = true;
  }
  else{
    $scope.hideRank = false;
  }

  if( _.contains($rootScope.hideBibRaceIds, parseInt(raceId,10)) ){
    $scope.hideBib = true;
  }
  else{
    $scope.hideBib = false;
  }

  if( _.contains($rootScope.hideOfficialTimeRaceIds, parseInt(raceId,10)) ){
    $scope.hideOfficialTime = true;
  }
  else{
    $scope.hideOfficialTime = false;
  }

  if( _.contains($rootScope.hideChipTimeRaceIds, parseInt(raceId,10)) ){
    $scope.hideChipTime = true;
  }
  else{
    $scope.hideChipTime = false;
  }

  if( _.contains($rootScope.hideFriendsRaceIds, parseInt(raceId,10)) ){
    $scope.hideFriends = true;
  }
  else{
    $scope.hideFriends = false;
  }

  if( _.contains($rootScope.hideNamesIds, parseInt(raceId, 10)) ){
    $scope.hideName = true;
  }
  else{
    $scope.hideName = false;
  }



  $scope.$watch(
    function() {
      return Facebook.isReady();
    },
    function(newVal) {
      if (newVal)
        $scope.facebookReady = true;
    }
  );

  $scope.shareFB = function(runner){    
    
    var obj = {
      method: 'feed',    
      link: $window.location.href,
      picture: 'http://myrunti.me/images/240px-square.png',
      name:  runner.firstName + " " + runner.lastName + "'s Run Time via MyRunTime" ,
      caption:  runner.raceId.raceName ,
      description: runner.firstName + " "+ runner.lastName +" finished " + runner.raceId.raceName + " in " + SecondsToTimeString(runner.gunTime)
    };

    Facebook.ui(obj, null);
  }

  //$scope.store  = storeService.store;
  $scope.race = null;
  $scope.runner = null;
  $scope.Perf = {}  
  $scope.hideOverlay = true;  
  $scope.hasSplits = false;
  $scope.hasFriendsInRace = false;
  $scope.friendsInRace = [];
  
  $scope.closeOverlay = function(){
    $scope.hideOverlay = true;
    $window.history.back();
  }

  $scope.gunTimeChartSeries = []
  $scope.chipTimeChartSeries = [];
  $scope.chartCategories = [];  
  $scope.gunPlotCategories = [];
  $scope.chipPlotCategories = [];

  $scope.gunPlotSeries = [{
    name: 'Number of finishers',
    data: [],
    type: 'line',
    datalabels: { enabled: false } 
    },
    {
      tooltip:{ enabled:true },
      name: 'Number of finishers',
      data: []
    }
  ];

  $scope.chipPlotSeries = [{
    name: 'Number of finishers',
    data: [],
    type: 'line',
    datalabels: { enabled: false } 
  },
  {
    tooltip:{ enabled:true },
    name: 'Number of finishers',
    data: []
  }];

  $scope.pacePlotCategories = [];
  $scope.splitsPieCategories = [];

  $scope.splitsPieSeries = [{
    name: 'Sector Length',
    data: [],
    size: '90%',
    innerSize: '40%',
    dataLabels:{
      formatter: function() {
        return this.y > 5 ? '<b>' + this.point.name + '</b>' : null;
      },
      color: 'white',
      distance: -55
    },
  },
  {
    name: 'Sector Time',
    data: [],
    size: '90%',
    innerSize: '60%',
    dataLabels: { 
      formatter: function(){
        return  '<b>'+ this.point.name +':</b>  '+ SecondsToTimeString(this.y) + '<br/><b>Distance</b>: ' + ($scope.sectorTimes[this.point.x].sectorLength/1000).toFixed(2) + " km";
      }
    }    
  }];

  $scope.pacePlotSeries = [{
    tooltip: {enabled: true},
    name: 'Pace',
    data: []
  }];

  $scope.gunChartConfig = {
    options: {
      chart: {
          type: 'bar'
      },
      plotOptions: {
        series: {
          stacking: 'stacked'
        }        
      }
    },      
    xAxis: {          
      categories: $scope.chartCategories,
      labels:{
        enabled: true,
        formatter: function(){
          return this.value;
        }
      },
      useHTML: true
    },
    yAxis: {
      min: 0,
      title: {
          text: 'Number of runners'
      },
      labels:{
        enabled: true,
        formatter: function(){
          return this.value;
        }
      },
      useHTML: true
    },  
    legend: {
      enabled:true,
      backgroundColor: '#FFFFFF',
      reversed: true
    },    
    series: $scope.gunTimeChartSeries,  
    title: {
      text: ''
    },      
    credits: {
      enabled: false
    },
    loading: true
  }

  $scope.chipChartConfig = {
    options: {
      chart: {
          type: 'bar'
      },
      plotOptions: {
        series: {
          stacking: 'stacked'
        }        
      }
    },
    xAxis: {          
      categories: $scope.chartCategories,
      labels:{
        enabled: true,
        formatter: function(){
          return this.value;
        }
      },
      useHTML: true
    },
    yAxis: {
      min: 0,
      title: {
          text: 'Number of runners'
      },
      labels:{
        enabled: true,
        formatter: function(){
          return this.value;
        }
      },
      useHTML: true
    },  
    legend: {
      enabled:true,
      backgroundColor: '#FFFFFF',
      reversed: true
    },
    series: $scope.chipTimeChartSeries,  
    title: {
      text: ''
    },      
    credits: {
      enabled: false
    },
    loading: true
  }

  $scope.gunTimePlotConfig = {  
    options:{
      chart: {
        defaultSeriesType: 'column',
        backgroundColor : '#FFF',
      },
      legend:{
        enabled: false
      },    
      plotOptions : {
        column : {
          borderColor : "transparent"
        }
      },
      //colors : ['#3a3a3a'],
    },
    title: {
      text: '',
      style : {
        color : '#e56d1e'
      }
    },
    subtitle: {
      text: '',
      style : {
        color : '#e56d1e'
      }
    },    
    series: $scope.gunPlotSeries,
    credits: {
      enabled: false
    },
    yAxis: {
      title: {
        text: 'No. of runners',
        style : {
          color : '#e56d1e'
        }
      }
    },
    xAxis: {
      categories: $scope.gunPlotCategories,      
      title: {
        text: 'Finishing Times',
        style: {
          color: '#e56d1e'
        }
      },
      labels: {
        staggerLines: 2
      }
    },
    loading: true
  }

  $scope.chipTimePlotConfig = {  
    options:{
      chart: {
        defaultSeriesType: 'column',
        backgroundColor : '#FFF',
      },
      legend:{
        enabled: false
      },    
      plotOptions : {
        column : {
          borderColor : "transparent"
        }
      },
      //colors : ['#3a3a3a'],
    },
    title: {
      text: '',
      style : {
        color : '#e56d1e'
      }
    },
    subtitle: {
      text: '',
      style : {
        color : '#e56d1e'
      }
    },    
    series: $scope.chipPlotSeries,
    credits: {
      enabled: false
    },    
    yAxis: {
      title: {
        text: 'No. of runners',
        style : {
          color : '#e56d1e'
        }
      }
    },
    xAxis: {
      categories: $scope.chipPlotCategories,      
      title: {
        text: 'Chip Time Segments',
        style: {
          color: '#e56d1e'
        }
      },
      labels: {
        staggerLines: 2
      }
    },
    loading: true
  }

  $scope.pacePlotConfig = {
    options:{
      chart:{
        defaultSeriesType: 'column',
        backgroundColor: '#FFF'
      },
      plotOptions: {
        column:{
          borderColor: "transparent"
        }
      },
      tooltip:{
        formatter: function(){
          return '' +
           '<b> Sector ' + (this.key+1) + '</b> - ' + SecondsToHHMMSS(this.point.y) + ' per km';
        }
      },
      credits: {enabled: false},
      legend: {enabled: false}
    },
    title: {
      text: ''
    },
    yAxis: {
      title: {
        text: 'Pace (seconds/km)',
        style : {
          color : '#e56d1e'
        }
      }
    },
    xAxis: {
      title: {
        text: 'Sector',
        style : {
          color : '#e56d1e'
        }
      },
      categories: $scope.pacePlotCategories
    },
    loading: true,
    series: $scope.pacePlotSeries

  }

  $scope.splitsPieConfig = {
    options:{
      chart: {
        type: 'pie'
      },
      plotOptions: {
        pie: {shadow: false}
      },
      tooltip:{
        formatter: function() {
          if (this.series.name == "Sector Length") 
            return ''+
              '<b>' + this.key + '</b> - ' + (this.point.y/1000).toFixed(2) + 'km' ;
          else{
            return ''+
              '<b>' + this.key + '</b> - ' + SecondsToTimeString(this.point.y) ;              
          }
        }
      },
      credits: {enabled: false},
      legend: {enabled: false}
    },
    title: {
      text: 'Sector by sector performance'
    },
    loading: true,
    series: $scope.splitsPieSeries
  }


  var loadRunner = function(){
    $http.get('/api/runners/'+raceId+'/bibnumber/'+bibnumber).success(function(data){
      if(data && data.Model){
        if(data.Model.userId){
          $http.get('http://graph.facebook.com/' + data.Model.userId.faceBookId + '/picture?redirect=0&height=240&type=normal&width=240').success(function(data){
            $scope.profilePic = data.data.url;
          });
        }      
        
        data.Model.gunTimeStr = SecondsToTimeString(data.Model.gunTime);
        data.Model.chipTimeStr = SecondsToTimeString(data.Model.chipTime);
        waveId = data.Model.waveId;

        $scope.runner = data;
        $scope.runnerGender = ($scope.runner.Model.gender == 'M') ? 'Male' : 'Female';
        $scope.sectorTimes = data.Model.sectorTimes;
        
        if(data.Model.sectorTimes.length > 0){          
          $scope.hasSplits = true;  
        }

        if(data.friendsResults.length > 0){
          $scope.hasFriendsInRace = true;
          $scope.friendsInRace =  data.friendsResults;
        }
        loadRace();        
      }
      else{
        console.log('Bib number not found.');
        $scope.contactForm = {};

        //Disable Pre Emptive
        var emailBody = "";
        var dataToSend = {};

        $http.get('/api/race/'+raceId).success(function(data){
          race = data;

          $scope.contactForm.eventName = race.raceName;
          $scope.contactForm.bib = bibnumber;

          dataToSend.inquiry = "Pre-emptive Email on Missing Time";
          dataToSend.name = "NO NAME";
          dataToSend.email = "NO EMAIL";
          dataToSend.eventName = race.raceName;

          emailBody = "Details: " + " \r\n " + "Bib Number: " + $scope.contactForm.bib + " \r\n " + "Estimated Finish: " + "" + " \r\n " + "Finish Buddies: " + "" + "\r\n ";
          if($scope.contactForm.feedback) emailBody += "Feedback: " + $scope.contactForm.feedback;
          dataToSend.feedback = emailBody;
        });

          //Disable Pre Emptive
        /*
          $http({
            method: 'POST',
            url: '/contact',
            data: dataToSend
          }).success(function (data, status, headers, config){
            // alert("Form Submitted!");
            // $route.reload();
          }).error(function(data, status, headers, config){
            // alert("Form Submission Failed. Try Again");
          }) 
        });
        */

        $scope.hideOverlay = false;
      }        
    });    
  }

  $scope.submitForm = function() {
    var emailBody = "";
    var dataToSend = {};
    var estimateFinish;
    var runningBuddies;

    $scope.disableSubmit = true;

    dataToSend.inquiry = "Missing Time";
    dataToSend.name = $scope.contactForm.name;
    dataToSend.email = $scope.contactForm.email;
    dataToSend.eventName = race.raceName;

    estimateFinish = ($scope.contactForm.estimateFinish) ? $scope.contactForm.estimateFinish : "";  
    runningBuddies = ($scope.contactForm.runningBuddies) ? $scope.contactForm.runningBuddies : "";

    emailBody = "Details: " + " \r\n " + "Bib Number: " + $scope.contactForm.bib + " \r\n " + "Estimated Finish: " + estimateFinish + " \r\n " + "Finish Buddies: " + runningBuddies + "\r\n ";
    if($scope.contactForm.feedback) emailBody += "Feedback: " + $scope.contactForm.feedback;
    dataToSend.feedback = emailBody;

    $http({
      method: 'POST',
      url: '/contact',
      data: dataToSend
    }).success(function (data, status, headers, config){
      $scope.disableSubmit = false;
      alert("Thank you for your feedback! We will get back to you as soon as possible.");
      $window.history.back();
    }).error(function(data, status, headers, config){
      alert("Form Submission Failed. Try Again");
    }) 
  }

  var loadRace = function(){
    $http.get('/api/race/'+raceId).success(function(data){      
      $scope.race = data;
      $http.get('/api/wave/'+raceId + '/'+ waveId).success(function(wave){
        $scope.wave = wave;
        $scope.raceStart = (new Date(wave.waveStart)).toLocaleTimeString();
        $scope.runnerFinish = (new Date(new Date(wave.waveStart).getTime() + ($scope.runner.Model.gunTime*1000))).toLocaleTimeString();
        $scope.runnerStart = new Date((new Date(new Date(wave.waveStart).getTime() + ($scope.runner.Model.gunTime*1000))).getTime() - ($scope.runner.Model.chipTime*1000)).toLocaleTimeString();
        $scope.compet = JSON.parse(wave.runnersText);            
        $scope.minGunTime = $scope.compet[0].o;
        $scope.maxGunTime = $scope.compet[$scope.compet.length - 1].o;
        computeTimes();
      })  
    });     
  }

  var computeTimes = function(){
    if ((!$scope.race)||(!$scope.runner)) return;
    else {      

      var compet = $scope.compet;      

      var competByGender = compet.filter(function(data) { return data.g == $scope.runner.Model.gender });
      $scope.competByGender = competByGender;
      
      for (var i = 0; i < compet.length; i++){
        if ($scope.runner.Model.gunTime <= compet[i].o){
          $scope.Perf.officialRank = i+1;
          break;
        }
      }

      for (var i = 0; i< competByGender.length; i++){        
        if ($scope.runner.Model.gunTime <= competByGender[i].o){
          $scope.Perf.genderGunRank = i+1;
          break;
        }
      }

      var sortByChip = compet.sort(function(a,b){return (a.c - b.c);})
      $scope.minChipTime = sortByChip[0].c;
      $scope.maxChipTime = sortByChip[sortByChip.length - 1 ].c;
      var sortByChipGender = competByGender.sort(function(a,b){ return (a.c - b.c);})      

      for (var i = 0; i < sortByChip.length; i++){
        if ($scope.runner.Model.chipTime <= sortByChip[i].c){
          $scope.Perf.chipRank = i+1;
          break;
        }
      }

      for (var i = 0; i < sortByChipGender.length; i++){
        if ($scope.runner.Model.chipTime <= sortByChipGender[i].c){
          $scope.Perf.genderChipRank = i+1;
          break;
        }
      } 
      computeBins();
    }
  }

  var computeBins = function(){
    var binCount = 15;

    var minGunTime = $scope.minGunTime;        
    var maxGunTime = $scope.maxGunTime;

    var minChipTime = $scope.minChipTime;
    var maxChipTime = $scope.maxChipTime;

    var exactGunTimeGap = (maxGunTime - minGunTime) / binCount;    
    var gunTimeGap = parseInt(exactGunTimeGap);
    
    var exactChipTimeGap = (maxChipTime - minChipTime) / binCount;
    var chipTimeGap = parseInt(exactChipTimeGap);

    $scope.gunTimeBinBoundaries = [];
    var gunTimeStartBoundary = minGunTime;
    var gunTimeEndBoundary = gunTimeStartBoundary + gunTimeGap;

    $scope.chipTimeBinBoundaries = [];
    var chipTimeStartBoundary = minChipTime;
    var chipTimeEndBoundary = chipTimeStartBoundary + chipTimeGap;


    for(var ctr = 0; ctr < binCount; ctr++){
      $scope.gunTimeBinBoundaries.push({
        min: gunTimeStartBoundary,
        minTime: SecondsToHHMM(gunTimeStartBoundary),
        max: gunTimeEndBoundary,
        maxTime: SecondsToHHMM(gunTimeEndBoundary),
        count: 0
      });

      $scope.chipTimeBinBoundaries.push({
        min: chipTimeStartBoundary,
        minTime: SecondsToHHMM(chipTimeStartBoundary),
        max: chipTimeEndBoundary,
        maxTime: SecondsToHHMM(chipTimeEndBoundary),
        count: 0
      });

      gunTimeStartBoundary = gunTimeEndBoundary;
      gunTimeEndBoundary = gunTimeEndBoundary + gunTimeGap;
      chipTimeStartBoundary = chipTimeEndBoundary;
      chipTimeEndBoundary = chipTimeEndBoundary + chipTimeGap;
    }
    
    $scope.compet.forEach(function(detail){
      var gunTimeBinNumber = parseInt((detail.o - minGunTime) / exactGunTimeGap);
      var chipTimeBinNumber = parseInt((detail.c - minChipTime) / exactChipTimeGap);      
      if(gunTimeBinNumber == 15) gunTimeBinNumber = 14;
      if(chipTimeBinNumber == 15) chipTimeBinNumber = 14;

      $scope.gunTimeBinBoundaries[gunTimeBinNumber].count += 1; 
      $scope.chipTimeBinBoundaries[chipTimeBinNumber].count+= 1; 
    })    
    loadCharts();
    $(window).resize();    
  }

  var loadCharts = function(){
    var officialRankPercent = ($scope.compet.length - $scope.Perf.officialRank) / $scope.compet.length;
    var officialPercentile = (officialRankPercent * 100).toFixed(2);

    var chipRankPercent = ($scope.compet.length - $scope.Perf.chipRank) / $scope.compet.length;
    var chipPercentile = (chipRankPercent * 100).toFixed(2);

    var gunChartTitle = $scope.runner.Wave.waveName + " Rank: " + $scope.Perf.officialRank + " out of " + $scope.compet.length + " runners (" +  officialPercentile + "% percentile)";
    var chipChartTitle = $scope.runner.Wave.waveName + " Rank: " + $scope.Perf.chipRank + " out of " + $scope.compet.length + " runners (" +  chipPercentile + "% percentile)";

    $scope.gunChartConfig.title.text = gunChartTitle;
    $scope.chipChartConfig.title.text = chipChartTitle;

    $scope.chartCategories.push($scope.runner.Wave.waveName + " Runners");
    $scope.chartCategories.push($scope.runnerGender + " Runners");

    $scope.gunTimeChartSeries.push({
      name: 'Finished after you',
      data:[
      {
        y: $scope.compet.length - $scope.Perf.officialRank,
        datalabels:{
          enabled: true
        }
      },
      {
        y: $scope.competByGender.length -$scope.Perf.genderGunRank, 
        datalabels: {
          enabled:true,
        }  
      }]
    });

    $scope.gunTimeChartSeries.push({
      name: 'Finished before you',
      data:[
      {
        y: $scope.Perf.officialRank - 1,   
        datalabels: {
          enabled:true,
        }            
      },
      {
        y: $scope.Perf.genderGunRank - 1, 
        datalabels: {
          enabled:true,
        }  
      }]
    });

    $scope.chipTimeChartSeries.push({
      name: 'Had a slower time than you',
      data:[
      {
        y: $scope.compet.length - $scope.Perf.chipRank,
        datalabels:{
          enabled: true
        }
      },
      {
        y: $scope.competByGender.length -$scope.Perf.genderChipRank, 
        datalabels: {
          enabled:true,
        }  
      }]
    });

    $scope.chipTimeChartSeries.push({
      name: 'Had a faster time than you',
      data:[
      {
        y: $scope.Perf.chipRank - 1,   
        datalabels: {
          enabled:true,
        }            
      },
      {
        y: $scope.Perf.genderChipRank - 1, 
        datalabels: {
          enabled:true,
        }  
      }]
    });

    $scope.gunTimePlotConfig.title.text = $scope.runner.Wave.waveName + " finishers over time";
    $scope.gunTimePlotConfig.subtitle.text = "Official time: " + SecondsToTimeString($scope.runner.Model.gunTime) + " - Rank:" + $scope.Perf.officialRank + " out of " + $scope.compet.length + " runners";

    $scope.gunTimeBinBoundaries.forEach(function(gunTimeBin){
      $scope.gunPlotCategories.push(gunTimeBin.minTime + " - " + gunTimeBin.maxTime);

      if($scope.runner.Model.gunTime >= gunTimeBin.min && $scope.runner.Model.gunTime < gunTimeBin.max){
        $scope.gunPlotSeries[1].data.push({
          y: parseInt(gunTimeBin.count),
          color: '#E56E1E'
        });

        $scope.gunPlotSeries[0].data.push({
          y: parseInt(gunTimeBin.count),
          marker:{
            fillColor: '#E56E1E',
            radius: 8
          }
        })
      }
      else{
        $scope.gunPlotSeries[1].data.push({
          y: parseInt(gunTimeBin.count)
        });
        $scope.gunPlotSeries[0].data.push({
          y: parseInt(gunTimeBin.count),          
          marker:{
            enabled:false
          }
        })
      }
    })


    $scope.chipTimePlotConfig.title.text = "Plot of runner chip times";
    $scope.chipTimePlotConfig.subtitle.text = "Chip time: " + SecondsToTimeString($scope.runner.Model.chipTime) + " - Chip Rank: " + $scope.Perf.chipRank + " out of " + $scope.compet.length + " runners"; 

    $scope.chipTimeBinBoundaries.forEach(function(chipTimeBin){
      $scope.chipPlotCategories.push(chipTimeBin.minTime + " - " + chipTimeBin.maxTime);

      if($scope.runner.Model.chipTime >= chipTimeBin.min && $scope.runner.Model.chipTime < chipTimeBin.max){
        $scope.chipPlotSeries[1].data.push({
          y: parseInt(chipTimeBin.count),
          color: '#E56E1E'
        });

        $scope.chipPlotSeries[0].data.push({
          y: parseInt(chipTimeBin.count),
          marker:{
            fillColor: '#E56E1E',
            radius: 8
          }
        })
      }
      else{
        $scope.chipPlotSeries[1].data.push({
          y: parseInt(chipTimeBin.count)
        })
        $scope.chipPlotSeries[0].data.push({
          y:parseInt(chipTimeBin.count),
          marker:{
            enabled: false
          }
        })
      }
    })

    if($scope.hasSplits){
      $scope.sectorTimes.forEach(function(sectorTime, index){

        $scope.chipPlotCategories.push(index + 1);
        var _start = new Date(sectorTime.sectorStart);
        var _end = new Date(sectorTime.sectorEnd);
        var _pace = ((_end-_start)/(sectorTime.sectorLength));       

        $scope.pacePlotSeries[0].data.push({
          y: parseFloat(_pace),
          color: '#E56E1E'
        })

        $scope.splitsPieSeries[0].data.push({
          name: "Sector " + (index+1),
          y: sectorTime.sectorLength
        });

        $scope.splitsPieSeries[1].data.push({
          name: "Sector " + (index+1) + " Time",
          y: (_end-_start)/1000
        })
      })
    }

    $scope.chipChartConfig.loading = false;
    $scope.gunChartConfig.loading = false;
    $scope.gunTimePlotConfig.loading = false;
    $scope.chipTimePlotConfig.loading = false;
    $scope.pacePlotConfig.loading = false;
    $scope.splitsPieConfig.loading = false;
  }

  loadRunner();

  var SecondsToHHMM = function(seconds) {
    var hours = parseInt(seconds / 3600) % 24;
    var minutes = parseInt(seconds / 60) % 60;
    
    var TimeStr = (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes);
    return TimeStr;
  }

  var SecondsToHHMMSS = function(seconds) {
    var hours = parseInt(seconds/3600) % 24;
    var minutes = parseInt(seconds / 60) % 60;
    var seconds = (seconds % 60).toFixed(3);  
    var TimeStr;
    if(hours == 0){
      TimeStr = (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds);    
    }
    else{
      TimeStr = (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds);    
    }  
    return TimeStr;
  }

  var SecondsToTimeString = function(seconds){
    var hours = parseInt(seconds / 3600) % 24;
    var minutes = parseInt(seconds / 60) % 60;  
    var seconds = (seconds % 60).toFixed(0);
    var TimeStr = (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds);
    return TimeStr;    
  }
;});

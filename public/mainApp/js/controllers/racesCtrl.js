'use strict';

app.controller('racesCtrl', function ($scope, $http){  
  //$scope.store  = storeService.store;
  $scope.page = 0;
  $scope.races = [];
  var fetchData = function(){
    $http.get('/api/races/available/'+$scope.page).success(function(data){
      data = data.map(function(a){
        a.Model.raceDate = (new Date(a.Model.raceDate));
        $scope.races.push(a);
      })
      $scope.page++;
      
    });
    $http.get('/api/races/upcoming/0').success(function(data){
      data =data.map(function(a){
        a.Model.raceDate = (new Date(a.Model.raceDate));
        return a;
      })
      $scope.upcomingRaces = data;
    });
  }
  fetchData();
  $scope.loadMore = fetchData;

  $scope.segmentMarker = true;
  $scope.autoOpen = true;

  $scope.openSegment = function(){
    $('#sidelikesegment').toggle(200);
    if($scope.segmentMarker){      
      $('#floatinglikebutton').animate({"right":"+=385"}, 200); 
      $scope.segmentMarker = !$scope.segmentMarker;
    }
    else{      
      $('#floatinglikebutton').animate({"right":"-=385"}, 200);  
      $scope.segmentMarker = !$scope.segmentMarker;
    }
  }

  $scope.closeSegment = function(){
    $('#sidelikesegment').toggle(200);
    if( $scope.segmentMarker){      
        $('#floatinglikebutton').animate({"right":"+=385"}, 200); 
         $scope.segmentMarker = ! $scope.segmentMarker;
    }
    else{      
        $('#floatinglikebutton').animate({"right":"-=385"}, 200);  
         $scope.segmentMarker = !$scope.segmentMarker;
    }
  }
;});